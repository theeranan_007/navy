<div class="content-wrapper">
<style>

</style>
<section class="content">

  <div class="row">
    <div class="col-md-8"  id="fadetab1" style="display:none;">

      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active" id="tab1"><a href="#tab_1-1" data-toggle="tab">กรอกข้อมูล</a></li>
          <li id="tab2"><a href="#tab_1-2" data-toggle="tab">รายงาน</a></li>
          <li id="tab3"><a href="#tab_1-3" data-toggle="tab">ตรวจสอบ</a></li>
          <li class="pull-right header"> ร้องขอ</li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1-1">
            <div class="row">
              <div class="col-md-12">
                <h4><b>ค้นหา</b></h4>
                <div class="row" id="search">
                  <div class="col-md-4 border">
                    <label>ชื่อ</label>
                    <input class="form-control" id="search_name" placeholder="ชื่อ" list="datalist_name"/>
                    <datalist id="datalist_name"></datalist>
                  </div>
                  <div class="col-md-4">
                    <label>สกุล</label>
                    <input class="form-control" id="search_sname" placeholder="สกุล" list="datalist_sname"/>
                    <datalist id="datalist_sname"></datalist>
                  </div>
                  <div class="col-md-4">
                    <label>ทะเบียน</label>
                    <input class="form-control" id="search_id8" placeholder="ทะเบียน" list="datalist_id8"/>
                    <datalist id="datalist_id8"></datalist>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <button type="button" onclick="search();" id="search_submit" class="btn btn-primary pull-right"><i class="fa fa-search"></i> ค้นหา</button>
                  </div>
                </div>
                <hr/>
                <h4><b>ผลลัพท์</b></h4>
                <div class="row">
                  <div class="col-md-12">
                    <table id="table_search" class="table table-hover">
                      <thead>
                        <tr>
                          <th>ชื่อ</th>
                          <th>สกุล</th>
                          <th>ทะเบียน</th>
                          <th>สังกัด</th>
                          <th>หมายเหตุ</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="modal fade" id="modal-default" tabindex=-1>
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">รายละเอียด</h4>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="row">
                                <div class="col-md-5"><b>ชื่อ</b></div>
                                <div class="col-md-7" id="detail_name">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>ผลัดที่ฝึก</b></div>
                                <div class="col-md-7" id="detail_yearin">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>ทะเบียน</b></div>
                                <div class="col-md-7" id="detail_id8">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>สังกัด</b></div>
                                <div class="col-md-7" id="detail_belong_to">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>สถานะภาพ</b></div>
                                <div class="col-md-7" id="detail_statustab">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>ยาเสพติด</b></div>
                                <div class="col-md-7" id="patient_status">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>ศาสนา</b></div>
                                <div class="col-md-7" id="detail_religion">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>สูง</b></div>
                                <div class="col-md-7" id="detail_height">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>ร้องขอ</b></div>
                                <div class="col-md-7" id="detail_is_request">-</div>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-md-5"><b>วุฒิการศึกษา</b></div>
                                <div class="col-md-7" id="detail_eductab">-</div>
                              </div>

                              <div class="row">
                                <div class="col-md-5"><b>ความรู้พิเศษ</b></div>
                                <div class="col-md-7" id="detail_skill">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>คะแนน</b></div>
                                <div class="col-md-7" id="detail_percent">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>คัดเลือก</b></div>
                                <div class="col-md-7" id="detail_uint4">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>สมัครใจ1</b></div>
                                <div class="col-md-7"id="detail_uint1">-</div>
                              </div>
                              <div class="row">
                                <div class="col-md-5"><b>สมัครใจ2</b></div>
                                <div class="col-md-7"id="detail_uint2">-</div>
                              </div>
                            </div>
                            <div class="col-md-8">
                              <div class="row">
                                <div class="col-md-3">
                                  <label for="input_unitname" >หน่วยที่เลือก</label>
                                </div>
                                <div class="col-md-9">
                                  <select id="input_unitname" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
                                  </select>
                                </div>

                              </div>
                              <div class="row">
                                <div class="col-md-3">
                                  <label for="input_askby" >ผู้ขอ</label>
                                </div>
                                <div class="col-md-3">
                                  <select id="input_askby" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" disabled=true>
                                    <?php
                                    for($num=1;$num<=20;$num++){
                                      if($num<10){
                                        echo '<option value="0'.$num.'">0'.$num.'</option>';
                                      }
                                      else{
                                        echo '<option value="'.$num.'">'.$num.'</option>';
                                      }
                                    }
                                    ?>
                                  </select>

                                </div>
                                <div class="col-md-6">
                                  <input type="text" class="form-control" id="input_askby2"  disabled=true>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-3">
                                  <label for="input_num" class="control-label">ลำดับ</label>
                                </div>
                                <div class="col-md-9">
                                  <input id="input_num" class="form-control" type="number" min=1 disabled=true>
                                  <div id="alert_num"></div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-3">
                                  <label for="input_remark" class="control-label">หมายเหตุ</label>
                                </div>
                                <div class="col-md-9">
                                  <input type="text" class="form-control EntTab" id="input_remark"  disabled=true>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-3">
                                  <label for="input_remark2" class="control-label">ขอต่อ</label>
                                </div>
                                <div class="col-md-9">
                                  <input type="text" class="form-control EntTab" id="input_remark2"  disabled=true>
                                </div>
                              </div>
                              <br />
                              <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                  <div class="form-group">
                                    <button type="button" id="request_clear" class="btn btn-default btn-flat"> ล้างค่า</button>
                                    <button type="button" id="request_add" class="btn btn-success btn-flat pull-right" disabled=true><i class="fa fa-plus-square"> เพื่ม </i></button>
                                  </div>
                                </div>
                              </div>
                              <div class='row'>
                                <div class="col-md-12">


                                  <!--<p><b>ร้องขอ</b></p>
                                  <hr>-->
                                  <div style="height: 245px;overflow-y: auto;">
                                  <style>
                                  .table-bordered>thead>tr>th,.table-bordered>tbody>tr>td{
                                    border: 1px solid black;
                                  }
                                  </style>
                                  <table id="request" class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th  class="text-center">#</th>
                                        <th  class="text-center">หน่วยที่เลือก</th>
                                        <th  class="text-center">ผู้ขอ</th>
                                        <th  class="text-center">ลำดับ</th>
                                        <th  class="text-center">หมายเหตุ</th>
                                        <th  class="text-center">ขอต่อ</th>
                                        <th  class="text-center">เลือก</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td>5</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                      <div id="loading_Modal"></div>
                      <!-- ทำโหลดหน้า modal -->
                      <!-- /.modal-content -->
                      <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>

              </div>
            </div>
          </div></div>
        <div id="loading_Search"></div>
        <div class="tab-pane" id="tab_1-2">
          <div class="row">
            <div class="col-md-offset-3 col-md-6">
              <label>ตรวจสอบตาม</label>
              <select id="select_order" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
                <option value="1">
                  หน่วยที่ขอ
                </option>
                <option value="2">
                  หน่วยที่ตก
                </option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-offset-3 col-md-6" id="sh_unitname">
              <label>ระบุหน่วย</label>
              <select id="qunitname" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
              </select>
            </div>

          </div>
          <div class="row">
            <div class="col-md-offset-3 col-md-6" id="sh_Askbyform">
              <label>รหัสผู้ขอ</label>
              <select id="qAskbyform" class="form-control selectpicker" data-live-search="true" data-show-subtext="true">
                <?php
                for($num=1;$num<=20;$num++){
                  if($num<10){
                    echo '<option value="0'.$num.'">0'.$num.'</option>';
                  }
                  else{
                    echo '<option value="'.$num.'">'.$num.'</option>';
                  }
                }
                ?>
              </select>

            </div>
          </div>
          <div class="row">
            <div class="col-md-offset-3 col-md-6" id="sh_Askbyto" >
              <label>ถึง</label></label>
              <select id="qAskbyto" class="form-control selectpicker" data-live-search="true" data-show-subtext="true">
                <?php
                for($num=1;$num<=20;$num++){
                  if($num<10&& $num!=20){
                    echo '<option value="0'.$num.'">0'.$num.'</option>';
                  }
                  else{
                    echo '<option value="'.$num.'">'.$num.'</option>';
                  }
                  if($num==20){
                    echo '<option selected value="'.$num.'">'.$num.'</option>';
                  }
                }
                ?>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-offset-3 col-md-6" >
              <label>ตรวจสอบข้อมูลซ้ำ</label>
              <select id="cAskby" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
                <option value="1">
                  ทั้งหมด
                </option>
                <option value="2">
                  ไม่ซ้ำ
                </option>
                <option value="3">
                  ผู้ขอซ้ำ
                </option>
                <option value="4">
                  ขอซ้ำกับคัดเลือกหน่วย
                </option>
                <option value="5">
                  ขอซ้ำกับมีเหตุ
                </option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-offset-3 col-md-6" >
              <label>เรียงตาม</label>
              <select id="oSortby" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
                <option value="2">
                  ผู้ขอ
                </option>
                <option value="1">
                  หน่วย
                </option>

              </select>
            </div>
          </div>

          <div class="row">
            <div class="col-md-offset-3 col-md-6" >
              <br />
              <button type="button" class="btn btn-primary btn-block" onclick="print();"><i class="fa fa-print"></i> ออกรายงาน</button>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab_1-3">

            <h4><b>หน่วย</b id="unit"> กร.</h4>
          <div class="row">
            <div class="col-md-12">
              <table id="table_request_check" class="table">
                <thead>
                  <tr>
                    <th>ชื่อ-สกุล</th>
                    <th>ทะเบียน</th>
                    <th>สังกัด</th>
                    <th>หน่วยตก</th>
                    <th>ร้องขอ</th>
                    <th>คัดเลือก</th>
                    <th>สถานะ</th>
                    <th>สมัครใจ1</th>
                    <th>สมัครใจ2</th>
                    <th>คะแนนสอบ</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4" id="fadetab2" style="display:none;">
    <div class="box box-info">
      <div class="box-body">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab2default" data-toggle="tab">ยอดร้องขอ</a></li>
          <li><a href="#tab1default" data-toggle="tab">ยอดรวม</a></li>
          <li><a href="#tab3default" data-toggle="tab">ตั้งค่า</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="panel with-nav-tabs skin-blue">
            <div class="panel-body">
              <div class="tab-content">
                <div class="tab-pane fade" id="tab1default">
                  <div class="row" id="countall">
                  </div>
                </div>
                <div class="tab-pane fade in active" id="tab2default">
                <style>
                </style>
                <div class="row" id="count">
                </div>
              </div>
              <div class="tab-pane fade" id="tab3default">
                <button type="button" onclick="saveamount();" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                <div class="form-group" id="amount">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<br/>
</section>
</div>
