<script  type="text/javascript" >
var navyid;
var askcode;
var yearin;
var num;
var name;
var sname;
var obj;
var selectid;
var tselectid = [5];
var trefnum = [5];
var tyearin = [5];
var str;
var tunit3 = [5];
var taskcode = [5];
var tasktext = [5];
var tunitname = [5];
var tnum = [5];
var tremark = [5];
var tremark2 = [5];
var editcheck = 0;
var tmp = 0;
var limit = 10, rowtable = 0;
var all;
var d;
var table=false;
var row
$body = $("body");
////////////////////////////////////// define table search to datatable/////////////////////////////////
var datatable = $('#table_search').DataTable({
  searching:false,
  ordering: false,
  bLengthChange: false,
  fnCreatedRow: function (nRow, aData, iDataIndex) {
    $(nRow).attr('id', aData["navyid"]);
  },
  columns: [
    {"data": "name"},
    {"data": "sname"},
    {"data": "belong"},
    {"data": "id8"},
    {"data": "oldyearin"}
  ]
});

$(document).ready(function () {


////////////////////////////////////// switch togle รายงาน

$('#qAskbyform').on('change',function(){
  console.log($('#qAskbyform').selectpicker('val'));
  $('#qAskbyto').selectpicker('val',$('#qAskbyform').selectpicker('val')).selectpicker('refresh');
})
$('#qAskbyto').on('change',function(){
  if(parseInt($('#qAskbyform').selectpicker('val'))>parseInt($('#qAskbyto').selectpicker('val'))){
    $.alertable.alert("ต้องมีค่ามากกว่า หรือ เท่ากับตามผู้ขอจาก").then(function(){
  $('#qAskbyto').selectpicker('val',$('#qAskbyform').selectpicker('val')).selectpicker('refresh');
    })
  }
})


  $('#table_search').on('keydown',function(e){
    console.log(e)
    // var next = row.next();
    // var prev = row.prev();
  })
  $('#search_name').focus();
  $('#hide').focus(function(){
    $('#input_unitname').selectpicker('toggle');
  })
  count1();//////////////////////ยอด แท็บร้องขอ////////////////////////////////
  countall();/////////////////////ยอดใหญื///////////////////////////////////
  unittabseting();///////////////////// ปรับยอดต้องการ "ตั้งค่า"///////////////////////
  $('#input_unitname').on('change',function(){//////////////////////////////// ดัก ถ้ายังไม่กรอกหน่วยไม่ให้ไปต่อ //////////////////////////

    console.log($("#input_unitname").selectpicker('val'));
    if(editcheck==0){
    if ($("#input_unitname").selectpicker('val') != 0) {

      $('#input_askby').prop('disabled',false).selectpicker('refresh');
      $('#input_askby2').prop('disabled',false);
      $('#input_num').prop('disabled',false);
      $('#input_remark').prop('disabled',false);
      $('#input_remark2').prop('disabled',false);
      $('#request_add').prop('disabled',false);
      $('#input_askby').selectpicker('toggle');
      if(editcheck==0)
      num = 0;
      setnum();

    }else{
      $('#input_unitname').selectpicker('toggle');
      $('#input_askby').prop('disabled',true);
      $('#input_askby2').prop('disabled',true);
      $('#input_num').prop('disabled',true);
      $('#input_remark').prop('disabled',true);
      $('#input_remark2').prop('disabled',true);
      $('#request_add').prop('disabled',true);
      $('#input_askby2').val("");
      $('#input_num').val("");
      $('#input_remark').val("");
      $('#input_remark2').val("");
    }
  }
  });

  $('#table_search tbody').on('dblclick', 'tr', function () {///////////////////////////// ดับเบิลคลิก เลือกรายชื่อ table search ///////////////////////
    $('#modal-default').modal('show');//////////////////////////// เปิด modal/////////////////
    navyid = $(this).attr('id');//////////////////// get navyid จาก this tr
    detail();/////////////////////// ดีงรายละเอียด
    request_table(); ////////////// ดึงข้อมูลผู้ขอ
  });

  // $('#select_refnum tbody').on('click', 'tr', function () {
  //   table=true;
  //   if ($(this).hasClass('selected')) {
  //     $(this).removeClass('selected');
  //   } else {
  //     $('#select_refnum tbody tr.selected').removeClass('selected');
  //     $(this).addClass('selected');
  //   }
  //   navyid = $(this).attr('navyid');
  //   console.log(navyid);
  //   detail_manual();
  // });
  $('#request tbody').on('dblclick', 'tr', function () {/////////////คลิกเผื่อเปลี่ยนผู้ขอ

    selectid = $(this).attr('id');///////////////// get selectid from request tbody tr
    if(selectid!=null){
      loadingStart('loading_Modal');/////////////////////////////ทำโหลดให้ modal
    $.alertable.confirm('ยืนยันการเปลี่ยนผู้ขอ').then(function() {

      select_request();/////////////////////////////เลือกผู้ขอใหม่
      request_table();/////////////////////// โหลด request table ใหม่
      count1();//////////////////////////โหลด ยอดใหม่
      loadingStop('loading_Modal');
    }, function() {
      loadingStop('loading_Modal');
    });
    }
  });
  //requset_table_print();

  $('#search_id8').keyup (function(event){////////////////////////////// replace ทะเบียน "-.-.0000"
    console.log(event);
    if(event.which!=8){
      var str=$('#search_id8').val().split("");
      if(str[0]!=null && str[1]!='.')
      $('#search_id8').val(str[0]+'.');

      if(str[2]=="."){
        $('#search_id8').val(str[0]+'.');
      }
      else if(str[2]!=null && str[3]!='.'){
        $('#search_id8').val(str[0]+str[1]+str[2]+'.0000');
      }
      if(str[8]!=null){
        str[4]=str[5];
        str[5]=str[6];
        str[6]=str[7];
        str[7]=str[8];
        $('#search_id8').val(str[0]+str[1]+str[2]+str[3]+str[4]+str[5]+str[6]+str[7]);
      }
    }
  });

  $('#search_name').keypress(function(event){//////////////////////////// ทำ event ให้กับ ฟอร์ม input search
    if(event.which==13){////////////////////////เมื่อกด enter
    $('#search_sname').focus();
  }
  });
  $('#search_sname').keypress(function(event){//////////////////////////// ทำ event ให้กับ ฟอร์ม input search
    if(event.which==13){////////////////////////เมื่อกด enter
    $('#search_id8').focus();
    }
  });
  $('#search_id8').keypress(function(event){//////////////////////////// ทำ event ให้กับ ฟอร์ม input search
    if(event.which==13){////////////////////////เมื่อกด enter
    $('#search_submit').focus();
  }
  });
  $('#modal-default').on('shown.bs.modal', function () {
      $('#input_unitname').selectpicker('toggle').selectpicker('toggle');
  });
  $('#input_askby').on('change',function(event){
      askcode = $('#input_askby').selectpicker('val');
      if(editcheck==0)
      num = 0;
      setnum();
      $('#input_askby2').focus();
  })
  $('#input_askby2').keypress(function(event){
    if(event.which == 13){
      $('#input_num').focus();
    }
  })
  $('#input_num').keypress(function(event){
    if(event.which == 13){
    $('#alert_num').empty();
    if(editcheck==0){
    $.ajax({////////////////////////////// ส่ง ajax ไป ค้นหา
          url: "<?php echo site_url('request/checknum')?>",
          type: 'GET',
          dataType: 'JSON',
          data: {askby: $('#input_askby').selectpicker('val'),
          num: $('#input_num').val()},
        })
    .done(function (result) {
          console.log(result);
          if(result[0]!=null){
            $('#alert_num').attr('style','background:#f39b12;').append("***ลำดับถูกใช้กับ พลฯ "+result[0]['name']+" "+result[0]['sname']+"***");
            $('#input_num').focus();
          }
          else{
            $('#input_remark').focus();
          }

        })
        .fail(function () {
          console.log("error");
        })
        .always(function () {
          console.log("complete");
          loadingStop('loading_Search');
        });
    }
      else{
        $('#input_remark').focus();
      }
    }
  })
  $('#input_remark').keypress(function(event){
    if(event.which == 13){
      $('#input_remark2').focus();
    }
  })
    $('#input_remark2').keypress(function(event){
    if(event.which == 13){
      $('#request_add').focus();
    }
  })
  $('#modal-default').on('hidden.bs.modal', function () {/////////////////เมื่อ modal close
  navyid=null;
  $("#detail_name").empty();
  $("#detail_name").empty();
  $("#detail_sname").empty();
  $("#detail_height").empty();
  $("#patient_status").css("background-color", "").empty();
  $("#detail_id8").empty();
  $("#detail_statustab").css("background-color", "").empty();
  $("#detail_yearin").empty();
  $("#detail_religion").empty();
  $("#detail_belong_to").empty();
  $("#detail_eductab").empty();
  $("#detail_is_request").css("background-color", "").empty();
  $("#detail_percent").empty();
  $("#detail_uint3").empty();
  $("#detail_uint4").empty();
  $("#detail_uint1").empty();
  $("#detail_uint2").empty();
  $("#detail_skill").empty();
  confirmcodeunit = null;
  editcheck = 0;
  $('#input_askby').prop('disabled',true);
  $('#input_askby2').prop('disabled',true);
  $('#input_num').prop('disabled',true);
  $('#input_remark').prop('disabled',true);
  $('#input_remark2').prop('disabled',true);
  $('#input_unitname').selectpicker('val',0);
  $('#input_askby').selectpicker('val','01');
  $('#input_askby2').val("");
  $('#input_num').val("");
  $('#input_remark').val("");
  $('#input_remark2').val("");
  if($('#request_add').hasClass('btn-warning')){
    $('#request_add').empty();
    $('#request_add').removeClass('btn-warning').addClass('btn-success').append('<i class="fa fa-plus-square"> เพิ่ม </i>');
  }
  $('#search input').val("");
  $('#search_name').focus();
})
  // $('#input_unitname4').on('keypress', function (event) {
  //   table=false;
  //   if(event.which==13){

  //     var unit3,txt=$('#input_unitname4').val();
  //     txt = txt.split(" ");
  //     if(txt[0]!=null){
  //       unit3=txt[0];
  //     }
  //     else{
  //       unit3=0;
  //     }
  //     $.ajax({
  //       url: 'request/update_unit3',
  //       type: 'GET',
  //       dataType: 'JSON',
  //       data: {unit3: unit3,
  //         navyid: navyid},
  //       })
  //       .done(function() {
  //         console.log("success");
  //       })
  //       .fail(function() {
  //         console.log("error");
  //       })
  //       .always(function() {
  //         console.log("complete");
  //         table=true;
  //         table_tab2();
  //         count1();
  //         countall();
  //       });

  //     }
  //   });
    $.ajax({////////////////////////////////////////////////// ดึงหน่วยลง
      url: 'request/unittab_request',
      type: 'GET',
      dataType: 'JSON',
    })
    .done(function (e) {
      console.log(e.length);
      $('#datalist_unitname').empty();
      $('#qunitname').empty();

      $.each(e, function (index, item) {
        $('#input_unitname').append(
          $("<option></option>").attr("value", item.refnum).text( item.name)
          );
          if(item.refnum==0)
          $('#qunitname').append(
            $("<option></option>").attr("value", 0).text("ทั้งหมด")
            );
            else
          $('#qunitname').append(
            $("<option></option>").attr("value", item.refnum).text( item.name)
            );

      });
      $('#qunitname').selectpicker('refresh');
      $('#input_unitname').selectpicker('refresh');

    })
    .fail(function () {
      console.log("error");
    })
    .always(function () {
      console.log("complete");
    });

  //   $('#input_unitname2').on('input', function () {
  //     var refnum = $('#input_unitname2').val();
  //     refnum = refnum.split(" ");
  //     if (refnum[0] != null) {
  //       d = refnum[0];
  //     } else {
  //       d = "";
  //     }
  //     table_tab2();

  //   });
  //   //$('#input_unitname1').easyAutocomplete(unittab);
  //   $('#search_person').on('input', function (e) {
  //     var id = $('#search_person').val().split(" ");
  //     if (e.which == 13 || e.which == 0 || id[5] != null) {

  //       //alert(id);
  //       navyid = id[5];
  //       detail();
  //       request_table();
  //     }
  //   });
  //   var askby = {
  //     data: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"],
  //     list: {
  //       match: {
  //         enabled: true
  //       }
  //     },

  //     theme: "plate-light"
  //   };


    $('#request_add').on('click',function(){
      if (editcheck == 0) {
        add_request();
      } else {
        update_request();
        editcheck == 0;
      }
      request_table();
      count1();
      countall();
    });
    $('#request_clear').on('click',function(){
      confirmcodeunit = null;
      editcheck = 0;
      $('#input_askby').prop('disabled',true);
      $('#input_askby2').prop('disabled',true);
      $('#input_num').prop('disabled',true);
      $('#input_remark').prop('disabled',true);
      $('#input_remark2').prop('disabled',true);
      $('#input_unitname').val(0).trigger('change');
      $('#input_askby').selectpicker('val','01');
      $('#input_askby2').val("");
      $('#input_num').val("");
      $('#input_remark').val("");
      $('#input_remark2').val("");
      if($('#request_add').hasClass('btn-warning')){
        $('#request_add').empty();
        $('#request_add').removeClass('btn-warning').addClass('btn-success').append('<i class="fa fa-plus-square"> เพิ่ม </i>');
      }
    });

    $('#fadetab1').fadeIn(500);
    $('#fadetab2').fadeIn(500);
    $('#search_name').focus();
  });
  function setnum() { ////////////////////////////////////// ฟอร์มร้องขอ
    $('#input_num').empty();
    console.log('setnum');
    $.ajax({
      url: "request/from_request",
      data: "yearin=" + yearin + "&input_askby=" + askcode,
      type: 'GET',
      async: false,
      dataType: 'JSON',
      cache: false,
      success: function (result) {
        if (num != 0) {
        $('#input_num').val(num);
      }
      else{
          $('#input_num').val(parseInt(result['num'][0]['maxNUM'])+1);
      }
        // if (num != 0) {
        //   for (i = 1; i <= parseInt(result['num'][0]['maxNUM']); i++) {//////////////////////////////  ลำดับ(num)
        //     $('#input_num')
        //     .append($("<option></option>")
        //     .attr("value", i).text(i));
        //   }
        //   $('#input_num').val(num).trigger('change');
        // } else {
        //   for (i = 1; i <= parseInt(result['num'][0]['maxNUM']) + 1; i++) {//////////////////////////////  ลำดับ(num)
        //     $('#input_num')
        //     .append($("<option></option>")
        //     .attr("value", i).text(i));
        //   }
        //   $('#input_num').val(parseInt(result['num'][0]['maxNUM']) + 1).trigger('change');
        // }
      }
    });
  }
  function detail_manual() {   ///////////////////////////////////รายละเอียดข้อมูล

    $("#detail_name_manual").empty();
    $("#detail_sname_manual").empty();
    $("#detail_height_manual").empty();
    $("#patient_status_manual").css("background-color", "").empty();
    $("#detail_id8_manual").empty();
    $("#detail_statustab_manual").css("background-color", "").empty();
    $("#detail_yearin_manual").empty();
    $("#detail_religion_manual").empty();
    $("#detail_belong_to_manual").empty();
    $("#detail_eductab_manual").empty();
    $("#detail_is_request_manual").css("background-color", "").empty();
    $("#detail_percent_manual").empty();
    $("#detail_uint3_manual").empty();
    $("#detail_uint4_manual").empty();
    $("#detail_uint1_manual").empty();
    $("#detail_uint2_manual").empty();
    $("#detail_skill_manual").empty();
    $('#input_unitname4').val('');
    $.ajax({
      url: "request/query_detail",
      data: "navyid=" + navyid,
      type: 'GET',
      dataType: 'JSON',
      cache: false,
      success: function (result) {
        obj = result[0];
        yearin = obj.yearin;
        id8 = obj.id8;
        name = obj.name;
        sname = obj.sname;
        cunit3 = obj.refnum3;
        $("#detail_name_manual").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
        $("#detail_height_manual").append(obj.height);
        if (obj.patient_status != null) {
          if (obj.patient_status != "")
          $('#patient_status_manual').append(obj.patient_status).css("background-color", "#ff4000    ");
        }
        $('#detail_religion_manual').append(obj.religion);
        $("#detail_id8_manual").append(obj.id8);
        if (obj.statuscode != "AA") {
          $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")").css("background-color", "yellow");
        } else {
          $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")");
        }
        if (obj.oldyerin != null) {
          $("#detail_yearin_manual").append(obj.yearin + "(" + obj.oldyerin + ")");
        } else {
          $("#detail_yearin_manual").append(obj.yearin);
        }
        if (obj.pseq < 10) {
          $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + "0" + obj.pseq + ")");
        } else {
          $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + obj.pseq + ")");
        }
        $("#detail_eductab_manual").append(obj.educname);
        if (obj.is_request != "000") {
          $("#detail_is_request_manual").append("ร้องขอ").css("background-color", "#40ff00");
        }
        $("#detail_percent_manual").append(obj.percent);
        $("#detail_uint3_manual").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
        if ((obj.postcode == null) && (obj.item == null))
        $("#detail_uint4_manual").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
        else
        $("#detail_uint4_manual").append(obj.refnum4 + " <b>หน่วย</b> " + obj.postname + " <b>ลำดับ</b> " + obj.item);
        $("#detail_uint1_manual").append(obj.refnum1);
        $("#detail_uint2_manual").append(obj.refnum2);
        $("#detail_skill_manual").append(obj.skill);
        $('#input_unitname4').val(obj.unit3+' '+obj.refnum3);
      },
      error: function (err) {
        alert(err);
      }
    });
    $.ajax({
      url: 'request/request_table_new',
      type: 'GET',
      dataType: 'JSON',
      data: 'sql=person.navyid=' + navyid,
    })
    .done(function (e) {
      var txt='';
      $("#detail_request").empty();
      $.each(e['select'], function (index, val) {
        txt += '<div class="row">'
        +'<div class="col-xs-5"><b>ขอลงหน่วย</b></div>'
        +'<div class="col-xs-7" id="detail_unit_manual">'+val.refnum2+'</div>'
        +'</div>'
        +'<div class="row">'
        +'<div class="col-xs-5"><b>ผู้ขอ</b></div>'
        +'<div class="col-xs-7" id="detail_askby_manual">'+val.askby+'</div>'
        +'</div>'
        +'<div class="row">'
        +'<div class="col-xs-5"><b>หมายเหตุ</b></div>'
        +'<div class="col-xs-7" id="detail_remark_manual">'+val.remark+'</div>'
        +'</div>'
        +'<div class="row">'
        +'<div class="col-xs-5"><b>ขอต่อ</b></div>'
        +'<div class="col-xs-7" id="detail_remark2_manual">'+val.remark2+'</div>'
        +'</div>'
        +'<br>';
      });
      $("#detail_request").append(txt);
    })
    .fail(function () {
      console.log("error");
    })
    .always(function () {
      console.log("complete");
    });

  }
  function detail() {   ///////////////////////////////////รายละเอียดข้อมูล

    $("#detail_name").empty();
    $("#detail_name").empty();
    $("#detail_sname").empty();
    $("#detail_height").empty();
    $("#patient_status").css("background-color", "").empty();
    $("#detail_id8").empty();
    $("#detail_statustab").css("background-color", "").empty();
    $("#detail_yearin").empty();
    $("#detail_religion").empty();
    $("#detail_belong_to").empty();
    $("#detail_eductab").empty();
    $("#detail_is_request").css("background-color", "").empty();
    $("#detail_percent").empty();
    $("#detail_uint3").empty();
    $("#detail_uint4").empty();
    $("#detail_uint1").empty();
    $("#detail_uint2").empty();
    $("#detail_skill").empty();
    $.ajax({
      url: "request/query_detail",
      data: "navyid=" + navyid,
      type: 'GET',
      dataType: 'JSON',
      cache: false,
      success: function (result) {
        obj = result[0];
        yearin = obj.yearin;
        id8 = obj.id8;
        name = obj.name;
        sname = obj.sname;
        cunit3 = obj.refnum3;
        $("#detail_name").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
        $("#detail_height").append(obj.height);
        if (obj.patient_status != null) {
          if (obj.patient_status != "")
          $('#patient_status').append(obj.patient_status).css("background-color", "#ff4000    ");
        }
        $('#detail_religion').append(obj.religion);
        $("#detail_id8").append(obj.id8);
        if (obj.statuscode != "AA") {
          $("#detail_statustab").append(obj.title + "(" + obj.statuscode + ")").css("background-color", "yellow");
        } else {
          $("#detail_statustab").append(obj.title + "(" + obj.statuscode + ")");
        }
        if (obj.oldyerin != null) {
          $("#detail_yearin").append(obj.yearin + "(" + obj.oldyerin + ")");
        } else {
          $("#detail_yearin").append(obj.yearin);
        }
        if (obj.pseq < 10) {
          $("#detail_belong_to").append(obj.company + "/" + obj.batt + "(" + obj.platoon + "0" + obj.pseq + ")");
        } else {
          $("#detail_belong_to").append(obj.company + "/" + obj.batt + "(" + obj.platoon + obj.pseq + ")");
        }
        $("#detail_eductab").append(obj.educname);
        if (obj.is_request != "000") {
          $("#detail_is_request").append("ร้องขอ").css("background-color", "#40ff00");
        }
        $("#detail_percent").append(parseFloat(obj.percent).toFixed(2));
        $("#detail_uint3").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
        if ((obj.postcode == null) && (obj.item == null))
        $("#detail_uint4").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
        else
        $("#detail_uint4").append(obj.refnum4 + " <b>หน่วย</b> " + obj.postname + " <b>ลำดับ</b> " + obj.item);
        $("#detail_uint1").append(obj.refnum1);
        $("#detail_uint2").append(obj.refnum2);
        $("#detail_skill").append(obj.skill);

      },
      error: function (err) {
        alert(err);
      }
    });
  }
  function add_request() {
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = d.getFullYear() + 543 + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;
    var dt = new Date();
    var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    $.ajax({
      url: "<?php echo site_url('request/add_request')?>",
      data: "yearin=" + yearin +
      "&navyid=" + navyid +
      "&id8=" + id8 +
      "&name=" + name +
      "&sname=" + sname +
      "&unit=" + $('#input_unitname').val() +
      "&askby=" + $('#input_askby').val() + " " + $('#input_askby2').val() +
      "&num=" + $('#input_num').val() +
      "&remark=" + $('#input_remark').val() +
      "&remark2=" + $('#input_remark2').val() +
      "&update=" + output +
      "&updtime=" + time,
      async: false,
      success: function (key) {
        if (key == '0') {
          alert("จำนวนผู้ขอเกิน 5 คน");
        }
      }
    });
    $('#input_askby').prop('disabled',true);
    $('#input_askby2').prop('disabled',true);
    $('#input_num').prop('disabled',true);
    $('#input_remark').prop('disabled',true);
    $('#input_remark2').prop('disabled',true);
    $('#input_unitname').selectpicker('val',0);
    $('#input_askby').selectpicker('val','01');
    $('#input_askby2').val("");
    $('#input_num').val("");
    $('#input_remark').val("");
    $('#input_remark2').val("");
    $('#close').focus();
  }
  function update_request() {
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = d.getFullYear() + 543 + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;
    var dt = new Date();
    var time = (dt.getHours()-543) + ":" + dt.getMinutes() + ":" + dt.getSeconds();

    $.ajax({
      url: "<?php echo site_url('request/edit_request')?>",
      data: "navyid=" + navyid +
      "&unit=" + $('#input_unitname').val() +
      "&askby=" + $('#input_askby').val() + " " + $('#input_askby2').val() +
      "&num=" + $('#input_num').val() +
      "&remark=" + $('#input_remark').val() +
      "&remark2=" + $('#input_remark2').val() +
      "&update=" + output +
      "&updtime=" + time +
      "&selectid=" + selectid,
      async: false,
    });
    $('#input_unitname').selectpicker('val',0);
    $('#input_askby').selectpicker('val','01');
    $('#input_askby2').val("");
    $('#input_num').val("");
    $('#input_remark').val("");
    $('#input_remark2').val("");

    if($('#request_add').hasClass('btn-warning')){
      $('#request_add').empty();
      $('#request_add').removeClass('btn-warning').addClass('btn-success').append('<i class="fa fa-plus-square"> เพิ่ม </i>');
    }
  }
  function request_table() {        //////////////////////////////////// ใส่ข้อมูลลงtable
    confirmcodeunit = null;
    editcheck = 0;
    var count;
    $.ajax({
      url: "<?php echo site_url('request/query_request')?>",
      data: "navyid=" + navyid,
      type: 'GET',
      dataType: 'JSON',
      cache: false,
      success: function (result) {
        var txthtml = "";
        count = 1;
        $("#request tbody").empty();
        $.each(result, function (i, item) {
          if (item.selectcode == 1) {
            txthtml += '<tr class="tsuccess" id=' + item.selectid + ' refnum = ' + item.refnum + '>';
            txthtml += "<td>" + count + "</td>";
            txthtml += "<td id='table_unitname'>" + item.unitname + "</td>";
            txthtml += "<td id='table_askby'>" + item.askby + "</td>";
            txthtml += "<td id='table_num'>" + item.num + "</td>";
            txthtml += "<td id='table_remark'>" + item.remark + "</td>";
            txthtml += "<td id='table_remark2'>" + item.remark2 + "</td>";
            txthtml += '<td><a class="fa fa-hand-o-left"></a>';
            txthtml += '<div class="pull-right" ><button type="button" onclick="edit(' + (count - 1) + ');" id="request_edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></button>';
            txthtml += '<button type="button" id="request_remove" onclick="remove_request(' + item.selectid + ');" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></td></div>';
            txthtml += "</tr>";
            tselectid[count - 1] = item.selectid;
            trefnum[count - 1] = item.refnum;
            str = item.askby;
            tunit3[count - 1] = item.unitname;
            taskcode[count - 1] = str.substring(0, 2);
            tasktext[count - 1] = str.substring(3);
            tunitname[count - 1] = item.unitname;
            tnum[count - 1] = parseInt(item.num);
            tremark[count - 1] = item.remark;
            tremark2[count - 1] = item.remark2;
            //set_val_input();

          } else {
            txthtml += '<tr class="" id=' + item.selectid + ' refnum = ' + item.refnum + '>';
            txthtml += "<td>" + count + "</td>";
            txthtml += "<td id='table_unitname'>" + item.unitname + "</td>";
            txthtml += "<td id='table_askby'>" + item.askby + "</td>";
            txthtml += "<td id='table_num'>" + item.num + "</td>";
            txthtml += "<td id='table_remark'>" + item.remark + "</td>";
            txthtml += "<td id='table_remark2'>" + item.remark2 + "</td>";
            txthtml += "<td>";
            txthtml += '<div class="pull-right" ><button type="button" onclick="edit(' + (count - 1) + ');" id="request_edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></button>';
            txthtml += '<button type="button" id="request_remove" onclick="remove_request(' + item.selectid + ');" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></td></div>';
            txthtml += "</tr>";
            tselectid[count - 1] = item.selectid;
            tyearin[count - 1] = item.yearin;
            trefnum[count - 1] = item.refnum;
            str = item.askby;
            tunit3[count - 1] = item.unitname;
            taskcode[count - 1] = str.substring(0, 2);
            tasktext[count - 1] = str.substring(3);
            tunitname[count - 1] = item.unitname;
            tnum[count - 1] = parseInt(item.num);
            tremark[count - 1] = item.remark;
            tremark2[count - 1] = item.remark2;
          }
          count++;
        });
        if (count <= 5) {
          for (i = count; i <= 5; i++) {
            txthtml += '<tr class="" >';
            txthtml += "<td>" + i + "</td>";
            txthtml += "<td></td>";
            txthtml += "<td></td>";
            txthtml += "<td></td>";
            txthtml += "<td></td>";
            txthtml += "<td></td>";
            txthtml += "<td></td>";
            txthtml += "</tr>";
          }
        }

        $("#request tbody").append(txthtml);
      },
      error: function (err) {
        alert(err);
      }
    });
  }
  function edit(count) {
    editcheck = 1;
    selectid = tselectid[count];
    yearin = tyearin[count];
    $('#input_askby').prop('disabled',false);
    $('#input_askby2').prop('disabled',false);
    $('#input_num').prop('disabled',false);
    $('#input_remark').prop('disabled',false);
    $('#input_remark2').prop('disabled',false);
    if($('#request_add').hasClass('btn-success')){
      $('#request_add').empty();
      $('#request_add').removeClass('btn-success').addClass('btn-warning').append('<i class="fa fa-edit"> แก้ไข </i>');
    }
    $('#request_add').prop('disabled',false);
    $('#input_unitname').val(trefnum[count]).trigger('change');
    askcode = taskcode[count];
    num = tnum[count];
    $('#input_askby').val(taskcode[count]).trigger('change');
    $('#input_askby2').val(tasktext[count]);
    $('#input_remark').val(tremark[count]);
    $('#input_remark2').val(tremark2[count]);

  }
  function remove_request(id) {      ////////////////////////////////////// เลือกผู้ขอ
    $.ajax({
      url: "request/delete_request",
      data: "navyid=" + navyid + "&selectid=" + id,
      async: false,
    });
    request_table();
    count1();
  }
  function remove_request_print(nid, id) {      ////////////////////////////////////// เลือกผู้ขอ
    $.ajax({
      url: "request/delete_request",
      data: "navyid=" + nid + "&selectid=" + id,
      async: false,
    });
    requset_table_print();
    count1();
  }
  function requset_table_print() {
    txthtml = "";
    var oldnavyid = "";
    //$('#tbodyid tr').removeAttr('selected');
    $("#tbodyid").empty();
    count = 0;
    //$("#result_incident").append("<tbody id='tbodyid'></tbody>");
    $("#num_row").empty();
    $.ajax({
      url: "request/request_table",
      data: "limit=" + limit + "&row=" + rowtable,
      type: 'GET',
      async: false,
      dataType: 'JSON',
      cache: false,
      success: function (result) {
        if ([result['select']]) {
          var btn;
          $.each(result['select'], function (i, item) {
            if (item.navyid == oldnavyid) {
              item.name = "";
              item.sname = "";
              btn = "";
            } else {
              btn = '<div class="pull-right" ><button type="button" onclick="edit_print(' + item.navyid + ',' + item.selectid + ');" id="request_edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></button>';
              btn += '<button type="button" id="request_remove" onclick="remove_request_print(' + item.navyid + ',' + item.selectid + ');" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';

            }
            if (item.up == 1 && item.selectcode == 1) {
              txthtml += '<tr class="" style="background-color: #5cb85c;" navyid=' + item.navyid + ' refnum = ' + item.refnum + ' unitname=' + item.unitname + ' id=' + count + '>';
            } else {
              txthtml += '<tr class="" navyid=' + item.navyid + ' refnum = ' + item.refnum + ' unitname=' + item.unitname + ' id=' + count + '>';
            }
            if (item.selectcode == 1) {
              txthtml += "<td><div  class='fa fa-check'> </div></td>";
            } else {
              txthtml += "<td></td>";
            }

            txthtml += "<td>" + item.name + "</td>";
            txthtml += "<td>" + item.sname + "</td>";
            txthtml += "<td>" + item.refnum3 + "</td>";
            txthtml += "<td>" + item.askby + "</td>";
            txthtml += "<td>" + item.num + "</td>";
            txthtml += "<td>" + item.remark2 + "</td>";
            txthtml += "<td>" + item.remark + "</td>";
            if (item.title == null)
            item.title = "";
            txthtml += "<td>" + item.title + "</td>";
            if (item.schoolname == null)
            item.schoolname = "";
            txthtml += "<td>" + item.schoolname + "</td>";
            if (item.refnum4 == null)
            item.refnum4 = "";
            if (item.postname == null)
            item.postname = "";
            txthtml += "<td>" + item.postname + ' ' + item.refnum4 + "</td>";
            txthtml += "<td>" + btn + "</td>";
            txthtml += '</tr>';
            oldnavyid = item.navyid;
            count++;
          });
          if (count <= 9) {
            $("#incident_next").prop('disabled', true);
          } else {
            $("#incident_next").prop('disabled', false);
          }

          $("#tbodyid").append(txthtml);
          $.each(result['count'], function (i, item) {
            all = parseInt(item.count);
            $("#num_row").append(rowtable + 1 + "-" + (rowtable + 10) + " (" + all + ")&nbsp;&nbsp;&nbsp;&nbsp;");
          });
        } else {

        }
      },
      error: function (err) {
        alert(err);
      }
    });
  }
  function select_request() {
    $.ajax({
      url: "<?php echo site_url('request/update_request')?>",
      data: "navyid=" + navyid + "&selectid=" + selectid,
      async: false,
    });
  }
  function request_print() {
    txthtml = "";
    var oldnavyid = "";

    $.ajax({
      url: "request/request_print",
      type: 'GET',
      async: false,
      dataType: 'JSON',
      cache: false,
      success: function (result) {
        if ([result['select']]) {
          txthtml += '<table id="request_table" class="table table-condensed table-bordered" >';
          txthtml += '</thead>';
          txthtml += '<tr>'
          txthtml += '<th  class="text-center">เลือก</th>';
          txthtml += ' <th  class="text-center">ชื่อ</th>';
          txthtml += '<th  class="text-center">นามสกุล</th>';
          txthtml += '<th  class="text-center">หน่วยที่ขอ</th>';
          txthtml += '<th  class="text-center">ผู้ขอ</th>';
          txthtml += '<th  class="text-center">ลำดับ</th>';
          txthtml += ' <th  class="text-center">ขอต่อ</th> ';
          txthtml += '<th  class="text-center">หมายเหตุ</th>';
          txthtml += '<th  class="text-center">สถานภาพ</th>';
          txthtml += ' <th  class="text-center">ความรู้</th> ';
          txthtml += '<th  class="text-center">คัดเลือกหน่วย</th>';
          txthtml += '<tr>';
          txthtml += '</thead>';
          $.each(result['select'], function (i, item) {
            if (item.navyid == oldnavyid) {
              item.name = "";
              item.sname = "";
            }
            if (item.up == 1 && item.selectcode == 1) {
              txthtml += '<tr class="tsuccess" navyid=' + item.navyid + ' refnum = ' + item.refnum + ' unitname=' + item.unitname + ' id=' + count + '>';
            } else {
              txthtml += '<tr class="" navyid=' + item.navyid + ' refnum = ' + item.refnum + ' unitname=' + item.unitname + ' id=' + count + '>';
            }
            if (item.selectcode == 1) {
              txthtml += "<td><div  class='fa fa-check'> </div></td>";
            } else {
              txthtml += "<td></td>";
            }

            txthtml += "<td>" + item.name + "</td>";
            txthtml += "<td>" + item.sname + "</td>";
            txthtml += "<td>" + item.refnum3 + "</td>";
            txthtml += "<td>" + item.askby + "</td>";
            txthtml += "<td>" + item.num + "</td>";
            txthtml += "<td>" + item.remark2 + "</td>";
            txthtml += "<td>" + item.remark + "</td>";
            txthtml += "</tr>";
            oldnavyid = item.navyid;
          });
          txthtml += '</table>';
        }
      }
    });
  }
  function printWindow()
  {
    request_print();
    var shtml = '<HTML>\n<HEAD>\n';
    if (document.getElementsByTagName != null)
    {
      var sheadTags = document.getElementsByTagName("head");
      if (sheadTags.length > 0)
      shtml += sheadTags[0].innerHTML;

    }

    shtml += '</HEAD>\n<font size="2"><div onload="window.print();">\n';
    if (txthtml != null)
    {
      shtml += '';//
      shtml += txthtml;
    }
    shtml += '\n</div></font>\n</HTML>';

    var printWin1 = window.open();
    printWin1.document.open();
    printWin1.document.write(shtml);
    printWin1.document.close();
  }
  function print() {
    var unit = "";
    var u = $('#input_unitname2').val();
    if (u != "") {
      u = u.split(" ");
      if (u[0] == null) {
        unit = "";
      } else
      unit = u[0];
    } else {
      unit = "";
    }

    window.open('request/pdf?unit=' + unit + '&askby=' + $('#input_askby1').val() + '&orderby=');
  }
  function saveamount() {
    data = "";
    for (var i = 1; i <= 30; i++) {
      data += '&' + i + "=" + $('#input' + i).val();
    }
    $.ajax({
      url: 'request/saveamount?' + data,
      type: 'GET',
      dataType: 'JSON',
    })
    .done(function () {
      console.log("success");
      count1();
      countall();
    })
    .fail(function () {
      console.log("error");
    })
    .always(function () {
      console.log("complete");
    });


  }
  function count1() {
        $.ajax({
            url: '<?php echo site_url('request/countunittab')?>',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("Query countall unittab >>> success");
                    console.log(result);
                    $('#count').empty();
                    var txt = '<div class="col-xs-5" id="label"><b>หน่วย</b></div>'
                            + '<div class="col-xs-3" id="labelall"><b>ยอดต้องการ</b></div>'
                            + '<div class="col-xs-3" id="labelunit1"><b>ร้องขอ</b></div>'
                    $.each(result['query'], function (index, val) {
                        txt += '<div class="col-xs-5" id="label' + val.refnum + '"><b>' + val.unitname + '</b></div>'
                                + '<div class="col-xs-3" id="countall_all' + val.refnum + '">0</div>'
                                + '<div class="col-xs-2" id="countall_request' + val.refnum + '" onclick="active('+val.refnum +')">0</div>'
                    });
                    $('#count').append(txt);
                })
                .fail(function () {
                    console.log("Query countall unittab >>> error");
                })
                .always(function () {
                    console.log("Query countall unittab >>> complete");


                    $.ajax({
                        url: '<?php echo site_url('request/countamount')?>',
                        type: 'GET',
                        dataType: 'JSON',
                    })
                            .done(function (result) {
                                console.log("Query countamount countall >>> success");
                                console.log(result);
                                $.each(result['query'], function (index, val) {
                                    $('#countall_all' + val.unit + '').empty();
                                    $('#countall_all' + val.unit + '').append(val.count);
                                });
                            })
                            .fail(function () {
                                console.log("Query countamount countall >>> error");
                            })
                            .always(function () {
                                console.log("Query countamount countall >>> complete");

                                $.ajax({
                                    url: '<?php echo site_url('request/countrequest')?>',
                                    type: 'GET',
                                    dataType: 'JSON',
                                })
                                        .done(function (result) {
                                            console.log("Query countall countunit3 >>> success");
                                            console.log(result);
                                            $.each(result['query'], function (index, val) {

                                                $('#countall_request' + val.unit + '').empty();
                                                $('#countall_request' + val.unit + '').append(val.count);
                                                console.log(parseInt($('#countall_all' + val.unit + '').text())+" "+parseInt($('#countall_request' + val.unit + '').text()));
                                                if(parseInt($('#countall_all' + val.unit + '').text()) == parseInt($('#countall_request' + val.unit + '').text())) {
                                                  $('#label' + val.unit + '').addClass('tsuccess');
                                                  $('#countall_all' + val.unit + '').addClass('tsuccess');
                                                  $('#countall_request' + val.unit+ '').addClass('tsuccess');
                                                }
                                                else if(parseInt($('#countall_all' + val.unit + '').text()) < parseInt($('#countall_request' + val.unit + '').text())) {
                                                  $('#label' + val.unit + '').addClass('twarning');
                                                  $('#countall_all' + val.unit + '').addClass('twarning');
                                                  $('#countall_request' + val.unit + '').addClass('twarning');
                                                }
                                            });
                                        })
                                        .fail(function () {
                                            console.log("Query countall countunit3 >>> error");
                                        })
                                        .always(function () {
                                            console.log("Query countall countunit3 >>> complete");
                                        });
                            });
                });

    }
  function countall() {
    $.ajax({
      url: 'request/countall',
      type: 'GET',
      dataType: 'JSON',
    })
    .done(function (result) {
      console.log("success");
      $('#countall').empty();
      var txt = '<div class="col-xs-5" id="label"><b>หน่วย</b></div>'

      + '<div class="col-xs-3" id="count_all"><b>ยอดต้องการ</b></div>'
      + '<div class="col-xs-3"id="count_requestall"><b>ยอดปัจจุบัน</b></div>';
      $.each(result, function (index, val) {
        if (val.unit != 'ไม่ได้ระบุ') {
          if (parseInt(val.count) == parseInt(val.all)) {
            console.log(val.unit + " " + val.count + " = " + val.all);
            txt += '<div class="col-xs-5 tsuccess"  id="label"><b>' + val.unit + '</b></div>'

            + '<div class="col-xs-3 tsuccess" id="count_all">' + val.all + '</div>'
            + '<div class="col-xs-3 tsuccess" id="count_requestall">' + val.count + '</div>';

          } else if (parseInt(val.count) > parseInt(val.all))
          {
            console.log(val.unit + " " + val.count + " > " + val.all);
            txt += '<div class="col-xs-5 twarning" id="label"><b>' + val.unit + '</b></div>'

            + '<div class="col-xs-3 twarning" id="count_all">' + val.all + '</div>'
            + '<div class="col-xs-3 twarning" id="count_requestall">' + val.count + '</div>';
          } else
          {
            console.log(val.unit + " " + val.count + " < " + val.all);
            txt += '<div class="col-xs-5 " id="label"><b>' + val.unit + '</b></div>'

            + '<div class="col-xs-3" id="count_all">' + val.all + '</div>'
            + '<div class="col-xs-3" id="count_requestall">' + val.count + '</div>';
          }
        }
      });
      $('#countall').append(txt);
    })
    .fail(function () {
      console.log("error");
    })
    .always(function () {
      console.log("complete");
    });

  }
  function unittabseting() {
    $.ajax({
      url: 'request/amount',
      type: 'GET',
      dataType: 'JSON',
    })
    .done(function (e) {
      console.log(e.length);
      $('#amount').empty();
      txt = '';
      $.each(e, function (index, item) {
        if (item.refnum > 0) {
          txt += '<div class="form-group">'
          + '<div class="col-xs-6">'
          + '<label for="input' + item.refnum + '" >' + item.unitname + '</label>'
          + '</div>'
          + '<div class="col-xs-6">'
          + '<input type="number" class="form-control" onchange="saveamount();" onClick="SelectAll(input' + item.refnum + ');"'
          + 'id="input' + item.refnum + '" value="' + item.count + '"/>'
          + '</div>'
          + '</div>'
          + '</div>';
        }
      });
      $('#amount').append(txt);

    })
    .fail(function () {
      console.log("error");
    })
    .always(function () {
      console.log("complete");
    });
  }
  function table_tab2(i){
    $.ajax({
      url: '<?php echo site_url('request/request_table_new');?>',
      type: 'GET',
      dataType: 'JSON',
      data: 'where=request.unit=' + i,
    })
    .done(function (e) {
      txt = '';
      $('#table_request_check tbody').empty();
      $.each(e['select'], function (index, val) {
        txt += '<tr>'
            +'<td>'+val.name+' '+val.sname+'</td>'
            +'<td>'+val.id8+'</td>'
            +'<td>'+val.belong+'</td>'
            +'<td>'+val.refnum3+'</td>'
            +'<td>'+val.refnumur+'</td>'
            +'<td>'+val.postname+' '+val.refnum4+'</td>'
            +'<td>'+val.title+'</td>'
            +'<td>'+val.refnum1+'</td>'
            +'<td>'+val.refnum2+'</td>'
            +'<td>'+val.percent+'</td>'
            +   '</tr>';
              $('#table_request_check tbody').append(txt);
      });
    })
    .fail(function () {
      console.log("error");
    })
    .always(function () {
      console.log("complete");
    });
  }
  function active(i){
    if($('#tab1').hasClass('active')){
      $('#tab1').removeClass('active');
    }
    if($('#tab_1-1').hasClass('active')){
      $('#tab_1-1').removeClass('active');
    }
    if($('#tab2').hasClass('active')){
      $('#tab2').removeClass('active');
    }
    if($('#tab_1-2').hasClass('active')){
      $('#tab_1-2').removeClass('active');
    }
    $('#tab3').addClass('active')
    $('#tab_1-3').addClass('active')
    table_tab2(i);
  }
  function loadingStart(id){
    $('#'+id+'').empty();
    $('#'+id+'').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
    console.log('loadingStart '+id);
  }
  function loadingStop(id){
    $('#'+id+'').empty();
    console.log('loadingStop '+id);
  }

function search(){
    if(navyid==null){
      datatable.clear().draw();/////////////////////////// clear datatable
      loadingStart('loading_Search');
      if($('#search_name').val()!="" || $('#search_sname').val()!="" || $('#search_id8').val()!="" ){
        $.ajax({////////////////////////////// ส่ง ajax ไป ค้นหา
          url: "<?php echo site_url('search_person_controller/search')?>",
          type: 'GET',
          dataType: 'JSON',
          data: {name: $('#search_name').val(),
          sname: $('#search_sname').val(),
          id8: $('#search_id8').val()},
        })
        .done(function (result) {
          console.log(result);
          navyid = result[0].navyid;
          datatable.rows(datatable.rows.add(result).draw()).nodes().to$().attr("id", result.navyid);
          row = $(this).closest('tr');
          $('#search_submit').focus();
        })
        .fail(function () {
          console.log("error");
        })
        .always(function () {
          console.log("complete");
          loadingStop('loading_Search');

        });
      }
      else{
        alert('ไม่สามารถค้นหาช่องว่างได้');
        loadingStop('loading_Search');
        $('#search_name').focus();
      }
    }else{


      console.log(navyid);
      detail();
      request_table();
      $('#modal-default').modal('show');
    }
  }
  function print(){
    str =""
    if($('#cAskby').selectpicker('val')==2){
      str += "having=count(request.navyid)=1&";
    }else if($('#cAskby').selectpicker('val')==3){
      str += "having=count(request.navyid)>1&";
    }

      str += "where=substring(request.askby,1,2)>="+$('#qAskbyform').selectpicker('val')+" and substring(request.askby,1,2)<="+$('#qAskbyto').selectpicker('val');

        if($('#select_order').selectpicker('val')==1){
          if($('#qunitname').selectpicker('val')!=0)
          str += ' and request.unit='+$('#qunitname').selectpicker('val')+"";

        }
        else if($('#select_order').selectpicker('val')==2){
          if($('#qunitname').selectpicker('val')!=0)
          str += ' and person.unit3='+$('#qunitname').selectpicker('val')+'';
        }
        if($('#cAskby').selectpicker('val')==4){
          str += " and person.unit3=person.unit4";
        }
        if($('#cAskby').selectpicker('val')==5){
          str += " and person.statuscode!='AA'";
        }
        if($('#oSortby').selectpicker('val')==1){
          str += "&orderby=request.unit desc";
        }else{
          str += "&orderby=substring(request.askby,1,2) asc,num asc";
        }


    window.open("<?php echo site_url('request/pdf?')?>"+str,"_blank");
  }
  </script>
