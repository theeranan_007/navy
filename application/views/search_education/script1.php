<script type="text/javascript">
    var navyid;
    var count = 0;
    var refnum;
    var row;
    var next;
    var prev;
    var c = 0;
    var limit = 10, rowtable = 0;
    var all;
    var input;
    var num = "";
    var unit4;
    var postcode;
    var yearin;
    var d;
    var general = 'T', seq = '';
    var table=false;
    var table2;
    $body = $("body");
    $(document).on({

      ajaxStart: function() {
        if(table==false){
          $body.addClass("loading");
        }
        },
      ajaxStop: function() { $body.removeClass("loading"); }
    });
    $(document).ready(function () {
        eductab_p2();
        $('#yearin').keyup(function(event){
          var str = $('#yearin').val().split("");

          if(event.which!=8){
          if(str[0]!=null && str[1]!='/'){
            $('#yearin').val(str[0]+'/');
          }
          if(str[2]=='/'){
            $('#yearin').val(str[0]+'/');
          }
        }
        });
        table2=$('#table_view').DataTable( {
            searching:false,
            dom: 'Bfrtip',
            buttons: [
            'copy','excel', 'print'
            ],
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('navyid', aData["navyid"]);
                $(nRow).attr('yearin', aData["yearin"]);
            },
            columns: [
            { "data": "batt" },
            { "data": "company" },
            { "data": "id8"},
            { "data": "name" },
            { "data": "sname" },
            { "data": "yearin" },
            { "data": "schoolname" }
            ]
        } );
        $('#input_eductab_p2').on('input', function () {
          if($('#yearin').val()==""){
            alert('กรุณาระบุผลัด');
            $('#input_eductab_p2').val("");
          }
          else{
            table=true;
            console.log('input_eductab_p2');
            eductab2_p2($('#input_eductab_p2').val().split(' ')[0]);
            $('#input_eductab2_p2').val("");
          }
        });
        $('#table_view tbody').on('click', 'tr', function () {
          table=true;
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('#table_view tbody tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        $('#table_view tbody').on('dblclick', 'tr', function () {
            navyid = $(this).attr('navyid');
            detail();
        });
        $("#imgprofile").click(function () {
            if (i == 0) {
                $("#imgprofile").css("width", "2in");
                $("#imgprofile").css("heigh", "3in");
                $("#imgprofile").css("cursor", "zoom-out");
                i++;
            } else if (i == 1) {
                $("#imgprofile").css("width", "1in");
                $("#imgprofile").css("heigh", "1.5in");
                $("#imgprofile").css("cursor", "zoom-in");
                i = 0;
            }
        });

                    $('#fadetab1').fadeIn(500);
                    $('#fadetab2').fadeIn(500);
    });
    function img() {
    var text="";
    if(oldyearin){
    text = "http://192.168.0.1/navyimages/" + oldyearin.substring(0, 1) + "." + oldyearin.substring(2) + "/" + navyid + ".jpg";

    }
    else{
    text = "http://192.168.0.1/navyimages/" + yearin.substring(0, 1) + "." + yearin.substring(2) + "/" + navyid + ".jpg";

    }
        $("#imgprofile").attr("src", text);
        $("#imgprofile").css("width", "1in");
        $("#imgprofile").css("heigh", "1.5in");

    }

    function printWindow()
    {
        var printReadyEle1 = document.getElementById("printContent1");
        var printReadyEle2 = document.getElementById("printContent2");
        var printReadyEle3 = document.getElementById("printContent3");
        var printReadyEle4 = document.getElementById("printContent4");
        var printReadyEle5 = document.getElementById("printContent5");
        var shtml = '<HTML>\n<HEAD>\n';
        if (document.getElementsByTagName != null)
        {
            var sheadTags = document.getElementsByTagName("head");
            if (sheadTags.length > 0)
                shtml += sheadTags[0].innerHTML;

        }

        shtml += '</HEAD>\n<BODY onload="window.print();">\n';
        if (printReadyEle1 != null)
        {
            shtml += '<form name = frmform1>';//
            shtml += '<div class="row"><div class="col-xs-12"><div class="text-center"><font size="6"><b>ประวัติ</b></font></div></div></div>';
            shtml += '<div class="row"><div class="col-xs-1" ></div><div class="col-xs-3" >';//
            shtml += printReadyEle1.innerHTML;
            shtml += '</div><div class="col-xs-8" >';//
            shtml += printReadyEle2.innerHTML;
            shtml += '</div></div><div class="row"><div class="col-xs-1" ></div><div class="col-xs-11" >';//
            shtml += printReadyEle4.innerHTML;
            shtml += '</div></div><div class="row"><div class="col-xs-1" ></div><div class="col-xs-11" >';//
            shtml += '<hr></div></div><div class="row"><div class="col-xs-1" ></div><div class="col-xs-11" >';//
            shtml += printReadyEle3.innerHTML;
            shtml += printReadyEle5.innerHTML;
            shtml += '</div></div>';//
        }
        shtml += '\n</form>\n</BODY>\n</HTML>';

        var printWin1 = window.open();
        printWin1.document.open();
        printWin1.document.write(shtml);
        printWin1.document.close();
    }
    function detail() {   ///////////////////////////////////รายละเอียดข้อมูล

        $("#detail_name").empty();
        $("#detail_name").empty();
        $("#detail_sname").empty();
        $("#detail_height").empty();
        $("#patient_status").empty();//.css("background-color", "").empty();
        $("#detail_id8").empty();
        $("#statuscode").empty();//.css("background-color", "").empty();
        $("#detail_yearin").empty();
        $("#detail_religion").empty();
        $("#detail_belong_to").empty();
        $("#detail_is_request").css("background-color", "").empty();
        $("#detail_percent").empty();
        $("#detail_uint3").empty();
        $("#detail_uint4").empty();
        $("#detail_uint1").empty();
        $("#detail_uint2").empty();
        $("#address").empty();
        $("#detail_blood").empty();
        $("#detail_father").empty();
        $("#detail_mother").empty();
        $("#detail_unit3").empty();
        $('#detail_company').empty();
        $('#detail_batt').empty();
        $('#detail_platoon').empty();
        $('#detail_pseq').empty();
        $("#detail_address").empty();
        $("#detail_address_mu").empty();
        $("#detail_address_soil").empty();
        $("#detail_address_road").empty();
        $("#detail_town3").empty();
        $("#detail_town2").empty();
        $("#detail_town1").empty();
        $("#detail_zipcode").empty();
        $("#detail_educname").empty();
        $("#detail_id").empty();
        $("#detail_unit1").empty();
        $("#detail_unit2").empty();
        $('#is_request').empty();
        $("#detail_regdate").empty();
        $("#detail_repdate").empty();
        $("#detail_movedate").empty();
        $("#detail_runcode").empty();
        $("#detail_birthdate").empty();
        $("#detail_id13").empty();
        $("#detail_mark").empty();
        $('#addressmove').empty();
        $('#patient_status').empty();
        $('#statuscode').empty();
        $('#detail_swimcode').empty();
        $('#detail_skillname').empty();
        $('#detail_occname').empty();
        $('#detail_schoolname').empty();
        $('#detail_mcp').empty();
        $('#detail_mc').empty();
        $('#detail_mc4').empty();
        $('#detail_groupname').empty();
        $('#detail_mc5').empty();
        $.ajax({
            url: "search_education/query_detail",
            data: "navyid=" + navyid,
            timeout: 7000, // 7 second
            type: 'GET',
            dataType: 'JSON',
            cache: false,
            success: function (result) {
                obj = result[0];
                yearin = obj.yearin;
                oldyearin = obj.oldyearin;
                id8 = obj.id8;
                name = obj.name;
                sname = obj.sname;
                cunit3 = obj.refnum3;
                img();
                $("#detail_name").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
                $("#detail_height").append(obj.height);
                if (obj.patient_status != null){
                        $('#patient_status').append(obj.patient_status);//.css("background-color", "#ff4000	");
                        $('#btn_patient_status').removeClass("btn-success");
                        $('#btn_patient_status').addClass("btn-danger");
                    }
                    else{
                        $('#btn_patient_status').removeClass("btn-danger");
                        $('#btn_patient_status').addClass("btn-success");
                    $('#patient_status').append("ปกติ");
                }
                $('#detail_religion').append(obj.religion);
                var regyear='';
                var year='';
                if(obj.oldyerin!=null){
                  year = obj.oldyearin;
                }
                else{
                  year = obj.yearin;
                }
                if(year.substring(0, 1)=="4"){
                  regyear = (parseInt(year.substring(2, 4))+1);
                }
                else{
                  regyear = (parseInt(year.substring(2, 4)));
                }
                $("#detail_id8").append("ทร."+regyear+" "+obj.id8);
                if (obj.statuscode != "AA") {
                    $("#statuscode").append(obj.title);//.css("background-color", "yellow");
                     $('#btn_statuscode').removeClass("btn-success");
                        $('#btn_statuscode').addClass("btn-danger");
                    }
                    else{
                        $('#btn_statuscode').removeClass("btn-danger");
                        $('#btn_statuscode').addClass("btn-success");
                     $("#statuscode").append(obj.title);
                }
                if (obj.oldyerin != null) {
                    $("#detail_yearin").append(obj.yearin + "(" + obj.oldyerin + ")");
                } else {
                    $("#detail_yearin").append(obj.yearin);
                }
                $('#detail_company').append(obj.company);
                $('#detail_batt').append(obj.batt);
                $('#detail_platoon').append(obj.platoon);
                $('#detail_pseq').append(obj.pseq);

                $("#detail_educname").append(obj.educname);
                if (obj.is_request != "000") {

                    $('#is_request').append('<i class="fa fa-check"></i>');

                    //$("#detail_is_request").append("ร้องขอ").css("background-color", "#40ff00");
                } else {
                    $('#is_request').append('<i class="fa fa-close"></i>');
                }
                if (obj.AddressMove == '1') {
                    $('#addressmove').append('<i class="fa fa-check"></i>');
                } else {
                    $('#addressmove').append('<i class="fa fa-close"></i>');
                }
                $("#detail_percent").append(obj.percent);
                $("#detail_uint3").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
                if ((obj.postcode == null) && (obj.item == null))
                    $("#detail_uint4").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
                else
                    $("#detail_uint4").append(obj.unit4 + " <b>หน่วย</b> " + obj.postcode + " <b>ลำดับ</b> " + obj.item);
                $("#detail_uint1").append(obj.refnum1);
                $("#detail_uint2").append(obj.refnum2);
                $("#detail_address").append(obj.address + " ม." + obj.address_mu);
                $("#detail_address_mu").append();
                if (obj.address_soil == "") {
                    obj.address_soil = " - ";
                }
                $("#detail_address_soil").append(obj.address_soil);
                if (obj.address_road == "") {
                    obj.address_road = " - ";
                }
                if(obj.refnum3 == "" || obj.refnum3 == null){
                    $("#printContent5").hide();
                    $("#detail_movedate").empty();
                    $("#detail_unit3").empty();
                }
                else{
                    $("#printContent5").show();
                }
                $("#detail_address_road").append(obj.address_road);
                $("#detail_town3").append(obj.town3);
                $("#detail_town2").append(obj.town2);
                $("#detail_town1").append(obj.town1);
                $("#detail_zipcode").append(obj.zipcode);
                $("#detail_blood").append(obj.blood);
                $("#detail_father").append(obj.father + " " + obj.fsname);
                $("#detail_mother").append(obj.mother + " " + obj.msname);
                $("#detail_unit3").append(obj.refnum3);
                $("#detail_id").append(obj.id);
                $("#detail_unit1").append(obj.refnum1);
                $("#detail_unit2").append(obj.refnum2);
                var regyear2,repdate,movedate,birthdate;
                if(obj.regdate!=null){
                  regyear2 = date(obj.regdate);
                }else{
                  regyear2='-';
                }
                if(obj.repdate!=null){
                  repdate = date(obj.repdate);
                }else{
                  repdate='-';
                }
                if(obj.movedate!=null){
                  movedate = date(obj.movedate);
                }else{
                  movedate='-';
                }
                if(obj.birthdate!=null){
                  birthdate = date(obj.birthdate);
                }else{
                  birthdate='-';
                }
                $("#detail_regdate").append(regyear2);
                $("#detail_repdate").append(repdate);
                $("#detail_movedate").append(movedate);
                $("#detail_runcode").append(obj.runcode);
                $("#detail_birthdate").append(birthdate);
                $("#detail_id13").append(obj.id13);
                id13 = obj.id13;
                $("#detail_mark").append(obj.mark);
                $('#detail_swimcode').append(obj.swimcode);
                $('#detail_skillname').append(obj.skill);
                $('#detail_occname').append(obj.occname);
                $('#detail_schoolname').append(obj.schoolname);

                $('#detail_mcp').append(obj.mcp);
                $('#detail_mc').append(obj.mc);
                $('#detail_mc4').append(obj.mc4);
                $('#detail_groupname').append(obj.groupname);
                $('#detail_mc5').append(obj.mc5);
                var start;
                if(obj.start!=null){
                  start = date(obj.start);
                }else{
                  start='-';
                }
                $('#detail_start').append(start);

            },
            error: function (err) {
                alert(err);
            }
        });
        $('#detail').modal('show');
    }
    function viewtable(j) {
        $.ajax({
            url: 'search_education/viewtable_search_education',
            type: 'GET',
            dataType: 'JSON',
            data: {yearin: $('#input_yearin').val(),
                unit3: j},
            success: function (e) {
                console.log(e);
                $('#table_hisrequest tbody').empty();
                var i = 0;
                var txthtml = "";
                $.each(e, function (index, val) {
                    /* iterate through array or object */
                    i++;
                    txthtml += "<tr>";
                    txthtml += "<td>" + val.YEARIN + "</td>";
                    if (val.unitname == null) {
                        val.unitname = '';
                    }
                    txthtml += "<td>" + val.unitname + "</td>";
                    if (val.GENERAL == null) {
                        val.GENERAL = '';
                    }
                    txthtml += "<td>" + val.GENERAL + "</td>";
                    if (val.educname == null) {
                        val.educname = '';
                    }
                    txthtml += "<td>" + val.educname + "</td>";
                    if (val.skill == null) {
                        val.skill = '';
                    }
                    txthtml += "<td>" + val.skill + "</td>";
                    txthtml += "<td>" + val.TOTAL + "</td>";
                    txthtml += "<td>";
                    txthtml += '<div class="pull-right" ><button type="button" onclick="edit(' + val.SEQ + ');" id="search_education_edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></button>';
                    txthtml += '<button type="button" id="search_education_remove" onclick="remove(' + val.SEQ + ');" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></td></div>';
                    txthtml += "</tr>";
                });
                if (i < 5) {
                    for (var c = i; c < 5; c++) {
                        txthtml += "<tr>";
                        txthtml += "<td>-</td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "</tr>";
                    }
                }
                $('#table_hisrequest tbody').append(txthtml);
            }
        })
                .done(function () {
                    console.log("success ");
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
        if (j != 0) {
            $.ajax({
                url: 'search_education/count_hisrequire',
                type: 'GET',
                dataType: 'JSON',
                data: {unit: j,
                    yearin: $('#input_yearin').val()},
            })
                    .done(function (result) {
                        console.log("success");
                        $('#total').empty();
                        $.each(result['query'], function (index, val) {
                            $('#total').append('รวม ' + val.count);
                        });
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function () {
                        console.log("complete");
                    });
        }

    }

    function eductab() {
        $.ajax({
            url: 'search_education/eductab',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_eductab').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_eductab').append($('<option>').val(val.ECODE1 + ' ' + val.EDUCNAME));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

    }
    function eductab2(i) {
        $.ajax({
            url: 'search_education/eductab2',
            type: 'GET',
            dataType: 'JSON',
            data: 'ecode1=' + i,
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_eductab2').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_eductab2').append($('<option></option>').val(val.ECODE2 + ' ' + val.EDUCNAME));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

    }
    function eductab_p2() {
        $.ajax({
            url: 'search_education/eductab_p2',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_eductab_p2').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_eductab_p2').append($('<option></option>').val(val.ECODE1 + ' ' + val.EDUCNAME));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

    }
    function eductab2_p2(i) {
      if($('#yearin').val()==""){
        alert('กรุณาระบุผลัด');
      }
      else{
        $.ajax({
            url: 'search_education/eductab2_p2',
            type: 'GET',
            dataType: 'JSON',
            data: 'ecode1=' + i+"&yearin="+ $('#yearin').val(),
        })
                .done(function (result) {
                    console.log(result['query']);
                    console.log("success eductab2_p2");
                    $('#datalist_eductab2_p2').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_eductab2_p2').append($('<option></option>').val(val.ecode2 + ' ' + val.educname));
                    });
                })
                .fail(function () {
                    console.log("error eductab2_p2");
                })
                .always(function () {
                    console.log("complete eductab2_p2");
                });
              }

    }
    function viewtable2(e1, e2,yearin) {
        table=false;
      table2.rows().remove().draw();
        $.ajax({
            url: 'search_education/viewtable',
            type: 'GET',
            dataType: 'JSON',
            data: {
              yearin: yearin,
              ecode1: e1,
              ecode2: e2},
        })
                .done(function (result) {
                    console.log("success");
                    txt = '';
                    $('#table_view tbody').empty();
                    /*$.each(result['query'], function (index, val) {
                        txt += '<tr navyid=' + val.navyid + ' >\n\
                                <td nowrap >' + val.name + ' ' + val.sname + '</td>\n\
                                <td nowrap >' + val.unit3 + '</td>\n\
                                <td nowrap >' + val.unit4 + '</td>\n\
                                <td nowrap >' + val.unit + '</td>\n\
                                <td nowrap >' + val.unit1 + '</td>\n\
                                <td nowrap >' + val.unit2 + '</td>\n\
                                <td nowrap >' + val.educname + '</td>\n\
                                <td nowrap >' + val.skill + '</td>\n\
                                <td nowrap >' + val.percent + '</td>\n\
                                <td nowrap >' + val.title + '</td>\n\
                                </tr>\n\
';
                    });
                    $('#table_view tbody').append(txt);*/
                    table2.rows(table2.rows.add(result.query).draw()).nodes().to$().attr("navyid", result.query.navyid).attr("yearin", result.query.yearin);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function radio2(i) {
        if (i == 1) {
            $('#input_eductab_p2').attr('disabled', true);
            $('#input_eductab2_p2').attr('disabled', true);
            $('#input_skill_p2').attr('disabled', true);
            $('#input_eductab_p2').val('');
            $('#input_eductab2_p2').val('');
            $('#input_skill_p2').val('');
            general = 'T';

        }
        if (i == 2) {
            $('#input_eductab_p2').attr('disabled', false);
            $('#input_eductab2_p2').attr('disabled', false);
            $('#input_skill_p2').attr('disabled', true);
            $('#input_skill_p2').val('');
            general = 'F';
            eductab_p2();
        }
        if (i == 3) {
            $('#input_eductab_p2').attr('disabled', true);
            $('#input_eductab2_p2').attr('disabled', true);
            $('#input_skill_p2').attr('disabled', false);
            $('#input_eductab_p2').val('');
            $('#input_eductab2_p2').val('');
            general = 'F';
            skilltab2();
        }
    }
    function search2() {
        if($('#yearin').val()==""){
          alert('กรุณาระบุผลัด');
        }
        else{
        viewtable2($('#input_eductab_p2').val().split(' ')[0], $('#input_eductab2_p2').val().split(' ')[0],$('#yearin').val());
      }
    }
    function date(str) {
        var text = "";
        var month = {1: 'มกราคม', 2: 'กุมภาพันธ์', 3: 'มีนาคม', 4: 'เมษายน', 5: 'พฤษภาคม', 6: 'มิถุนายน', 7: 'กรกฎาคม', 8: 'สิงหาคม', 9: 'กันยายน', 10: 'ตุลาคม', 11: 'พฤศจิกายน', 12: 'ธันวาคม'};
        text = str.substring(8, 10);
        text += " ";
        text += month[ parseInt(str.substring(5, 7)) ];
        text += " ";
        text += parseInt(str.substring(0, 4)) + 543;
        return text;
    }
    function TranscriptionFrom(){

        window.open('http://192.168.0.1/TranscriptionForm/'+ yearin.substring(0, 1) + "." + yearin.substring(2)+'/'+id13+'.pdf','_blank');

    }
</script>
