<div class="content-wrapper">
  <section class="content-header">
      <h1>
          ค้นหาตามวุฒิการศึกษา
      </h1>
  </section>
    <section class="content">

      <?php $this->load->view('modal_print');?>
        <div class="row">
            <div class="col-xs-12"  id="fadetab1"  style="display:none;">
                <div class="box box-info">
                  <div class="box-header with-border">
                      <h3 class="box-title">ฟอร์มค้นหา</h3>
                  </div>
                    <div class="box-body">
                      <div class="row">
                          <div class="col-xs-12 form-group" >
                              <div class="row">
                                  <div class="col-xs-4">
                                    <lable>ผลัด</label>
                                      <input type="text" class="form-control" id="yearin" placeholder="_/__">
                                  </div>
                                  <div class="col-xs-4">
                                    <lable>วุฒิ</label>
                                      <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_eductab_p2" list="datalist_eductab_p2"/>
                                      <datalist id="datalist_eductab_p2"></datalist>
                                  </div>
                                  <div class="col-xs-4">
                                      <lable>สาขา</label>
                                      <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_eductab2_p2" list="datalist_eductab2_p2"/>
                                      <datalist id="datalist_eductab2_p2"></datalist>
                                  </div>
                              </div>
                              <br>

                          </div>
                      </div>
                    </div>
                    <div class="box-footer with-border">
                      <button type='button' class="btn btn-success pull-right" onClick="search2();"><i class="fa fa-search"></i> ต้นหา</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default" id="result_box">
                    <div class="box-header with-border">
                        <h3 class="box-title">ผลการค้นหา</h3>
                        <p class="box-title pull-right" id="num_row"></p>
                    </div>
                    <div class="box-body">
                      <div class="row">
                          <div class="col-xs-12">
                              <div class="row" >
                                  <div class="col-xs-12">
                                      <table id="table_view" class="table" >
                                          <thead>
                                              <tr>
                                                  <th nowrap class="text-center">กองพัน</th>
                                                  <th nowrap class="text-center">กองร้อย</th>
                                                  <th nowrap class="text-center">ทะเบียน</th>
                                                  <th nowrap class="text-center">ชื่อ</th>
                                                  <th nowrap class="text-center">สกุล</th>
                                                  <th nowrap class="text-center">ผลัด</th>
                                                  <th nowrap class="text-center">สถานศึกษา</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                    <div class="box-footer with-border">
                      <button type='button' class="btn btn-success pull-right" onClick="search2();"><i class="fa fa-search"></i> ต้นหา</button>
                    </div>
                  </div>

                </div>

              </div>
    </section>
</div>
