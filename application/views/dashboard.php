 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->

      <div class="row">
        <div class="col-lg-12 col-xs-12">
          <!-- small box -->
         <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Browser Usage</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="200" width="200" style="width: 151px; height: 155px;"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">

                    <?php $num = 0; foreach ($query as $key => $value): ?>

                      <?php echo '<li><i class="fa fa-circle-o" style="color:hsl('.$num.', 100%, 50%)"></i> '.$value['unit'].'</li>' ?>

                    <?php $num+=10; endforeach; ?>
                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <?php $num = 0; foreach ($query as $key => $value): ?>

                  <?php echo '<li><a href="#">'.$value['unit'].'
                    <span class="pull-right  style="color:hsl('.$num.', 100%, 50%)">
                    <i class="fa fa-angle-down"></i> '.$value['count'].'</span></a></li>'; ?>

                <?php $num+=10; endforeach; ?>
              </ul>
            </div>
            <!-- /.footer -->
          </div>
        </div>
        <!-- ./col -->

      </div>
      <!-- /.row -->
      <!-- Main row -->


    </section>
    <!-- /.content -->
  </div>
