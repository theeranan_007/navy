<div class="content-wrapper">

  <section class="content">
    <div class="row">
      <div class="col-md-8" id="fadetab1"  style="display:none;">

        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1-1" data-toggle="tab">กรอกข้อมูล</a></li>
            <li><a href="#tab_1-2" data-toggle="tab">รายงาน</a></li>
            <li><a href="#tab_1-3" data-toggle="tab">อัพเดทหน่วย</a></li>
            <li class="pull-right header"> คัดเลือกหน่วย</li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_1-1">
              <div class="row">
                <div class="col-md-12">
                  <h4><b>ค้นหา</b></h4>
                  <div class="row" id="search">
                    <div class="col-md-4">
                      <label>ชื่อ</label>
                      <input class="form-control" id="search_name" placeholder="ชื่อ" list="datalist_name"/>
                      <datalist id="datalist_name"></datalist>
                    </div>
                    <div class="col-md-4">
                      <label>สกุล</label>
                      <input class="form-control" id="search_sname" placeholder="สกุล" list="datalist_sname"/>
                      <datalist id="datalist_sname"></datalist>
                    </div>
                    <div class="col-md-4">
                      <label>ทะเบียน</label>
                      <input class="form-control" id="search_id8" placeholder="ทะเบียน" list="datalist_id8"/>
                      <datalist id="datalist_id8"></datalist>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-12">
                      <button type="button" onclick="search();" id="search_submit" class="btn btn-primary pull-right"><i class="fa fa-search"></i> ค้นหา</button>
                    </div>
                  </div>
                  <hr/>
                  <h4><b>ผลลัพท์</b></h4>
                  <div class="row">
                    <div class="col-md-12">
                      <table id="table_search" class="table table-hover">
                        <thead>
                          <tr>
                            <th>ชื่อ</th>
                            <th>สกุล</th>
                            <th>ทะเบียน</th>
                            <th>สังกัด</th>
                            <th>หมายเหตุ</th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="modal fade" id="modal-default" tabindex=-1>
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">รายละเอียด</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-4">
                                <!--<p><b>รายละเอียด</b></p>
                                <hr>-->
                                <div class="row">
                                  <div class="col-md-5"><b>ชื่อ</b></div>
                                  <div class="col-md-7" id="detail_name">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>ผลัดที่ฝึก</b></div>
                                  <div class="col-md-7" id="detail_yearin">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>ทะเบียน</b></div>
                                  <div class="col-md-7" id="detail_id8">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>สังกัด</b></div>
                                  <div class="col-md-7" id="detail_belong_to">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>สถานะภาพ</b></div>
                                  <div class="col-md-7" id="detail_statustab">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>ยาเสพติด</b></div>
                                  <div class="col-md-7" id="patient_status">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>ศาสนา</b></div>
                                  <div class="col-md-7" id="detail_religion">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>สูง</b></div>
                                  <div class="col-md-7" id="detail_height">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>ร้องขอ</b></div>
                                  <div class="col-md-7" id="detail_is_request">-</div>
                                </div>
                                <hr>

                                <div class="row">
                                  <div class="col-md-5"><b>วุฒิการศึกษา</b></div>
                                  <div class="col-md-7" id="detail_eductab">-</div>
                                </div>

                                <div class="row">
                                  <div class="col-md-5"><b>ความรู้พิเศษ</b></div>
                                  <div class="col-md-7" id="detail_skill">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>คะแนนสอบ</b></div>
                                  <div class="col-md-7" id="detail_percent">-</div>
                                </div>

                                <div class="row">
                                  <div class="col-md-5"><b>หน่วยคัดเลือก</b></div>
                                  <div class="col-md-7" id="detail_uint4">-</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-5"><b>หน่วยสมัครใจ1</b></div>
                                  <div class="col-md-7"id="detail_uint1">-</div>
                                </div>
                                <div class="row">

                                  <div class="col-md-5"><b>หน่วยสมัครใจ2</b></div>
                                  <div class="col-md-7"id="detail_uint2">-</div>
                                </div>
                              </div>
                              <div class="col-md-8">
                                <div class="row">
                                  <div class="col-md-3">
                                    <label>ตำแหน่ง</label>
                                  </div>
                                  <div class="col-md-9">
                                    <select type="text" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" id="input_positioncode" >
                                    </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-3">
                                    <label>หน่วยที่เลือก</label>
                                  </div>
                                  <div class="col-md-9">
                                    <select type="text" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" id="input_unitname" disabled>
                                    </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-3">
                                    <label>ลำดับ</label>
                                  </div>
                                  <div class="col-md-9">
                                    <input type="number" class="form-control" id="input_num" data-live-search="true" data-show-subtext="true" disabled/>
                                    <div id="alert_num"></div>
                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-offset-3 col-md-9">
                                    <div class="form-group">
                                      <button type="button" id="selectexam_add" class="btn btn-success btn-flat pull-right" disabled=true><i class="fa fa-plus-square"> เพื่ม </i></button>
                                    </div>
                                  </div>
                                </div>
                                <!-- <div class='row'>

                                <div style="height: 245px;overflow-y: auto;overflow-x: hidden">
                                <table id="table_postcode" class="table" >
                                <thead>
                                <tr>
                                <th  class="text-center">สังกัด</th>
                                <th  class="text-center">ชื่อ-นามสกุล</th>
                                <th  class="text-center">ผลัด</th>
                                <th  class="text-center">ทะเบียน</th>
                                <th  class="text-center">หน่วย</th>
                                <th  class="text-center">ลำดับ</th>
                                <th  class="text-center">หมายเหตุ</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            <tr>
                            <td>1</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                          <td>2</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                        <td>3</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                      <td>4</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                    <td>5</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div> -->

        </div>
      </div>

    </div>
    <div id="loading_Modal"></div>
    <!-- ทำโหลดหน้า modal -->
    <!-- /.modal-content -->
    <!-- <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
  </div> -->
</div>
<!-- /.modal-dialog -->
</div>

</div>
</div>
</div>
</div>
<div id="loading_Search"></div>

<div class="tab-pane" id="tab_1-2">
  <div class="row">
    <div class="col-md-offset-3 col-md-6">
      <label>ตรวจสอบตาม</label>
      <select id="select_order" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
        <option value="1">
          หน่วยทีคัดเลือก
        </option>
        <option value="2">
          หน่วยที่ตก
        </option>
      </select>
    </div>
  </div>
  <div class="row">
    <div class="col-md-offset-3 col-md-6" id="sh_unitname">
      <label>ระบุหน่วย</label>
      <select id="qunitname" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
      </select>
    </div>

  </div>
  <div class="row">
    <div class="col-md-offset-3 col-md-6" id="sh_Askbyform">
      <label>ระบุตำแหน่ง</label>
      <select id="qPosition" class="form-control selectpicker" data-live-search="true" data-show-subtext="true">
      </select>

    </div>
  </div>
  <div class="row">
    <div class="col-md-offset-3 col-md-6" >
      <label>ตรวจสอบข้อมูลซ้ำ</label>
      <select id="cAskby" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
        <option value="1">
          ทั้งหมด
        </option>
        <option value="2">
          ไม่ซ้ำ
        </option>
        <option value="3">
          ขอซ้ำกับร้องขอ
        </option>
        <option value="4">
          ขอซ้ำกับมีเหตุ
        </option>
      </select>
    </div>
  </div>
  <div class="row">
    <div class="col-md-offset-3 col-md-6" >
      <label>เรียงตาม</label>
      <select id="oSortby" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
        <option value="2">
          ตำแหน่งคัดเลือก
        </option>
        <option value="1">
          หน่วยคัดเลือก
        </option>

      </select>
    </div>
  </div>

  <div class="row">
    <div class="col-md-offset-3 col-md-6" >
      <br />
      <button type="button" class="btn btn-primary btn-block" onclick="print();"><i class="fa fa-print"></i> ออกรายงาน</button>
    </div>
  </div>
</div>
<div class="tab-pane" id="tab_1-3">
  <div class="row">
    <div class="col-md-offset-3 col-md-6">
      <label>ระบตำแหน่ง</label>
      <select id="update_position" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
      </select>
    </div>
  </div>
  <div class="row">
    <div class="col-md-offset-3 col-md-6">
      <label>ระบุหน่วย</label>
      <select id="update_unittab" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" disabled>
      </select>
    </div>
  </div>
  <div class="row">
    <div class="col-md-offset-3 col-md-6">
      <label>ทั้งหมด</label>
      <input type="number" id="cposition" class="form-control"  disabled="true">
    </div>
  </div>
  <div class="row">
    <div class="col-md-offset-3 col-md-6">
      <label>ระบุจำนวน</label>
      <input type="number" id="update_limit" class="form-control"  disabled="true">
    </div>
  </div>
  <!-- <div class="row">
    <div class="col-md-offset-3 col-md-6">
      <label>อัพเดทหน่วย</label>
      <select id="update_unit3" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
        <option value="1">
          เฉพาะคนที่ยังไม่มีหน่วย
        </option>
        <option value="2">
          ทั้งหมด
        </option>
      </select>
    </div>
  </div> -->
  <!-- <div class="row">
    <div class="col-md-offset-3 col-md-6" id="sh_unitname">
      <label>สถานะภาพ</label>
      <select id="update_status" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
        <option value="1">ปกติ</option>
        <option value="2">ไม่ปกติ</option>
        <option value="3">ทังหมด</option>
      </select>
    </div>
  </div> -->
  <div class="row">
    <div class="col-md-offset-3 col-md-6" >
      <br />
      <button type="button" id="update_submit" disabled="true" class="btn btn-primary btn-block" onclick="update_selectexam();"><i class="fa fa-upload"></i> อัพเดท</button>
    </div>
  </div>
</div>
</div>
</div>
</div>
<div class="col-md-4" id="fadetab2"  style="display:none;">
  <div class="box box-info">
    <div class="box-body">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab2default" data-toggle="tab">ยอดคัดเลือก</a></li>
        <li><a href="#tab1default" data-toggle="tab">ยอดรวม</a></li>
        <li><a href="#tab3default" data-toggle="tab">ตั้งค่า</a></li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel with-nav-tabs skin-blue">
          <div class="panel-body">
            <div class="tab-content">
              <div class="tab-pane fade" id="tab1default">
                <div class="row" id="countall">
                </div>
              </div>
              <div class="tab-pane fade in active" id="tab2default">
                <div class="row" id="count">
                </div>
              </div>
              <div class="tab-pane fade" id="tab3default">
                <button type="button" onclick="saveamount();" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                <div class="form-group" id="amount">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<br/>
</section>
</div>
