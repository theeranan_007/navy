<script type="text/javascript">
var navyid;
var count = 0;
var refnum;
var row;
var next;
var prev;
var c = 0;
var limit = 10, rowtable = 0;
var all;
var input;
var num = "";
var unit4;
var postcode;
var yearin;
var d;
var item;
var table=false;
var datatable = $('#table_search').DataTable({
    searching:false,
    ordering: false,
    bLengthChange: false,
    fnCreatedRow: function (nRow, aData, iDataIndex) {
        $(nRow).attr('id', aData["navyid"]);
    },
    columns: [
        {"data": "name"},
        {"data": "sname"},
        {"data": "belong"},
        {"data": "id8"},
        {"data": "oldyearin"}
    ]
});

$body = $("body");
$(document).on({

    ajaxStart: function() {
        if(table==false){
            $body.addClass("loading");
        }
    },
    ajaxStop: function() { $body.removeClass("loading"); }
});

$(document).ready(function () {
    unittab();
    count1();
    countall();
    unittabseting();
    $('#update_position').on('change',function(){
        if($('#update_position').selectpicker('val')!=0){
            $('#update_unittab').prop('disabled',false).selectpicker('refresh');
        }else{
            $('#update_unittab').prop('disabled',true).selectpicker('refresh');
        }
    });
    $('#update_unittab').on('change',function(){
        if($('#update_unittab').selectpicker('val')!=0){
            $('#update_submit').prop('disabled',false);
            $('#update_limit').prop('disabled',false);
            var selectexamtotal =  0;
            var unit = $('#update_unittab').selectpicker('val');
            $.ajax({
                url: '<?php echo site_url('selectexam/count_position')?>',
                type: 'GET',
                dataType: 'JSON',
                data: {postcode: $('#update_position').selectpicker('val'),
                        unit4: unit}
            })
            .done(function(result) {
                if(result[0]!=null){
                $('#cposition').val(result[0]['countposition']);
                selectexamtotal = result[0]['countposition'];
                var Bigtotal = parseInt($('#count_amount'+unit).text()) - parseInt($('#count_total'+unit).text());
                if(Bigtotal<=selectexamtotal){
                    $('#update_limit').val(Bigtotal).attr('max',Bigtotal);
                }
                else{
                    $('#update_limit').val(selectexamtotal).attr('max',selectexamtotal);
                }
            }
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });


        }else{
            $('#update_submit').prop('disabled',true);
            $('#update_limit').val('').prop('disabled',true);
        }
    })
    $('#table_search tbody').on('dblclick', 'tr', function () {///////////////////////////// ดับเบิลคลิก เลือกรายชื่อ table search ///////////////////////
        $('#modal-default').modal('show');//////////////////////////// เปิด modal/////////////////
        navyid = $(this).attr('id');//////////////////// get navyid จาก this tr
        detail();/////////////////////// ดีงรายละเอียด
    });
    $('#modal-default').on('shown.bs.modal', function () {
        $('#input_positioncode').selectpicker('toggle').selectpicker('toggle');
    });
    $('#modal-default').on('hidden.bs.modal', function () {/////////////////เมื่อ modal close
        navyid=null;
        $("#detail_name").empty();
        $("#detail_name").empty();
        $("#detail_sname").empty();
        $("#detail_height").empty();
        $("#patient_status").css("background-color", "").empty();
        $("#detail_id8").empty();
        $("#detail_statustab").css("background-color", "").empty();
        $("#detail_yearin").empty();
        $("#detail_religion").empty();
        $("#detail_belong_to").empty();
        $("#detail_eductab").empty();
        $("#detail_is_request").css("background-color", "").empty();
        $("#detail_percent").empty();
        $("#detail_uint3").empty();
        $("#detail_uint4").empty();
        $("#detail_uint1").empty();
        $("#detail_uint2").empty();
        $("#detail_skill").empty();
        confirmcodeunit = null;
        editcheck = 0;
        $('#input_askby').prop('disabled',true);
        $('#input_askby2').prop('disabled',true);
        $('#input_num').prop('disabled',true);
        $('#input_remark').prop('disabled',true);
        $('#input_remark2').prop('disabled',true);
        $('#input_unitname').selectpicker('val',0);
        $('#input_askby').selectpicker('val','01');
        $('#input_askby2').val("");
        $('#input_num').val("");
        $('#input_remark').val("");
        $('#input_remark2').val("");
        if($('#request_add').hasClass('btn-warning')){
            $('#request_add').empty();
            $('#request_add').removeClass('btn-warning').addClass('btn-success').append('<i class="fa fa-plus-square"> เพิ่ม </i>');
        }
        $('#search input').val("");
        $('#search_name').focus();
    })
    $('#input_positioncode').on('change',function(){
        if($('#input_positioncode').selectpicker('val')!=0){
            $('#input_unitname').prop('disabled',false).selectpicker('refresh');
            $('#input_unitname').selectpicker('toggle');


        }else{
            $('#input_unitname').prop('disabled',true).selectpicker('refresh');
            $('#input_positioncode').selectpicker('toggle');

        }
    })
    $('#input_unitname').on('change',function(){
        if($('#input_unitname').selectpicker('val')!=0){
            $('#input_num').prop('disabled',false).selectpicker('refresh');
            $('#input_num').focus();
            if(num==null){
                $.ajax({
                    url: '<?php echo site_url('selectexam/num')?>',
                    type: 'GET',
                    dataType: 'JSON',
                    data:{postcode:$('#input_positioncode').selectpicker('val'),
                    unit4:$('#input_unitname').selectpicker('val'),}
                })
                .done(function(result) {
                    $('#input_num').val(parseInt(result['num'][0]['maxNUM'])+1);
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            }
            else{
                $('#input_num').val(num);
            }
        }else{
            $('#input_num').prop('disabled',true).selectpicker('refresh');
            $('#input_unitname').focus();
            $('#input_num').val("");
        }
    })
    $('#input_num').keypress(function(event) {
        if(event.which==13){
            if($('#input_num').val()!=""){
                $('#selectexam_add').prop('disabled',false)
                $.ajax({
                    url: '<?php echo site_url('selectexam/checknum')?>',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {position:$('#input_positioncode').selectpicker('val'),
                    unit4: $('#input_unitname').selectpicker('val'),
                    num:$('#input_num').val()}
                })
                .done(function(result) {
                    if(result['unit'][0]!=null){
                        $('#alert_num').attr('style','background:#f39b12;').append("***ลำดับถูกใช้กับ พลฯ "+result['unit'][0]['name']+" "+result['unit'][0]['sname']+"***");
                        $('#input_num').focus();
                    }
                    else{
                        $('#selectexam_add').focus();
                    }
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

            }
            else{
                $.alertable.alert("กรุณาระบุลำดับ")
            }

        }
    });
    $.ajax({
        url: 'selectexam/position_selectexam',
        type: 'GET',
        dataType: 'JSON',
    })
    .done(function (e) {
        console.log(e.length);
        $('#input_positioncode').empty();
        $('#input_positioncode')
        .append($("<option></option>")
        .attr("value", 0)
        .text("เลือกตำแหน่ง"));
        $('#qPosition')
        .append($("<option></option>")
        .attr("value", 0)
        .text("เลือกตำแหน่ง"));
        $('#update_position')
        .append($("<option></option>")
        .attr("value", 0)
        .text("เลือกตำแหน่ง"));
        $.each(e, function (index, item) {
            $('#input_positioncode')
            .append($("<option></option>")
            .attr("value", item.code)
            .text( item.name));
            $('#qPosition')
            .append($("<option></option>")
            .attr("value", item.code)
            .text( item.name));
            $('#update_position')
            .append($("<option></option>")
            .attr("value", item.code)
            .text( item.name));
        });
        $('#input_positioncode').selectpicker('refresh');
        $('#qPosition').selectpicker('refresh');
        $('#update_position').selectpicker('refresh');
    })
    .fail(function () {
        console.log("error");
    })
    .always(function () {
        console.log("complete");
    });
    $('#search_id8').keyup (function(event){
        console.log(event);
        if(event.which!=8){
            var str=$('#search_id8').val().split("");
            if(str[0]!=null && str[1]!='.')
            $('#search_id8').val(str[0]+'.');

            if(str[2]=="."){
                $('#search_id8').val(str[0]+'.');
            }
            else if(str[2]!=null && str[3]!='.'){
                $('#search_id8').val(str[0]+str[1]+str[2]+'.0000');
            }
            if(str[8]!=null){
                str[4]=str[5];
                str[5]=str[6];
                str[6]=str[7];
                str[7]=str[8];
                $('#search_id8').val(str[0]+str[1]+str[2]+str[3]+str[4]+str[5]+str[6]+str[7]);
            }
        }
    });
    $('#search_name').keypress(function(event){//////////////////////////// ทำ event ให้กับ ฟอร์ม input search
        if(event.which==13){////////////////////////เมื่อกด enter
            $('#search_sname').focus();
        }
    });
    $('#search_sname').keypress(function(event){//////////////////////////// ทำ event ให้กับ ฟอร์ม input search
        if(event.which==13){////////////////////////เมื่อกด enter
            $('#search_id8').focus();
        }
    });
    $('#search_id8').keypress(function(event){//////////////////////////// ทำ event ให้กับ ฟอร์ม input search
        if(event.which==13){////////////////////////เมื่อกด enter
            $('#search_submit').focus();
        }
    });
    $('#selectexam_add').on('click',function () {
        insert();
    });
    $('#search_person').on('input', function (e) {
        var id = $('#search_person').val().split(" ");
        console.log(e);
        if (e.which == 13 || e.which == 0 || id[5] != null) {

            //alert(id);
            navyid = id[5];
            detail();

        }
    });
    $('#input_unit3').keypress(function (e) {
        if (e.which == 13) {
            unit3_change();
        }
    });
    $('#fadetab1').fadeIn(500);
    $('#fadetab2').fadeIn(500);
});
/////////////////////////////////////// queryข้อมูลจาก databast ลง table//////////////////////////////////////////////
function detail() {   ///////////////////////////////////รายละเอียดข้อมูล
    num=null
    $("#detail_name").empty();
    $("#detail_name").empty();
    $("#detail_sname").empty();
    $("#detail_height").empty();
    $("#patient_status").css("background-color", "").empty();
    $("#detail_id8").empty();
    $("#detail_statustab").css("background-color", "").empty();
    $("#detail_yearin").empty();
    $("#detail_religion").empty();
    $("#detail_belong_to").empty();
    $("#detail_eductab").empty();
    $("#detail_is_request").css("background-color", "").empty();
    $("#detail_percent").empty();
    $("#detail_uint3").empty();
    $("#detail_uint4").empty();
    $("#detail_uint1").empty();
    $("#detail_uint2").empty();
    $("#detail_skill").empty();
    $.ajax({
        url: "selectexam/query_detail",
        data: "navyid=" + navyid,

        type: 'GET',
        dataType: 'JSON',
        cache: false,
        success: function (result) {
            obj = result[0];
            yearin = obj.yearin;
            id8 = obj.id8;
            name = obj.name;
            sname = obj.sname;
            cunit3 = obj.refnum3;
            $("#detail_name").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
            $("#detail_height").append(obj.height);
            if (obj.patient_status != null) {
                if (obj.patient_status != "")
                $('#patient_status').append(obj.patient_status).css("background-color", "#ff4000    ");
            }
            $('#detail_religion').append(obj.religion);
            $("#detail_id8").append(obj.id8);
            if (obj.statuscode != "AA") {
                $("#detail_statustab").append(obj.title + "(" + obj.statuscode + ")").css("background-color", "yellow");
            } else {
                $("#detail_statustab").append(obj.title + "(" + obj.statuscode + ")");
            }
            if (obj.oldyerin != null) {
                $("#detail_yearin").append(obj.yearin + "(" + obj.oldyerin + ")");
            } else {
                $("#detail_yearin").append(obj.yearin);
            }
            if (obj.pseq < 10) {
                $("#detail_belong_to").append(obj.company + "/" + obj.batt + "(" + obj.platoon + "0" + obj.pseq + ")");
            } else {
                $("#detail_belong_to").append(obj.company + "/" + obj.batt + "(" + obj.platoon + obj.pseq + ")");
            }
            $("#detail_eductab").append(obj.educname);
            if (obj.is_request != "000") {
                $("#detail_is_request").append("ร้องขอ").css("background-color", "#40ff00");
            }
            $("#detail_percent").append(obj.percent);
            $("#detail_uint3").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
            if ((obj.postcode == null) && (obj.item == null)) {
                $("#detail_uint4").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
                $('#input_positioncode').selectpicker("val",0);
                $('#input_unitname').selectpicker("val",0);
                $('#input_num').val("");
            } else {
                $("#detail_uint4").append(obj.postname + " <b>หน่วย</b> " + obj.refnum4 + " <b>ลำดับ</b> " + obj.item);
                $('#input_positioncode').selectpicker("val",obj.postcode);
                $('#input_unitname').prop('disabled',false).selectpicker("val",obj.unit4).selectpicker('refresh');
                $('#input_num').prop('disabled',false).val(obj.item);
                $('#selectexam_add').prop('disabled',false);
                num = obj.item;
                unit4 = obj.unit4;
                postcode = obj.postcode;
                item = obj.item;
                viewtable();
            }

            $("#detail_uint1").append(obj.refnum1);
            $("#detail_uint2").append(obj.refnum2);
            $("#detail_skill").append(obj.skill);

        },
        error: function (err) {
            alert(err);
        }
    });
}
function unittab() {
    $.ajax({
        url: "selectexam/unittab",
        type: 'GET',
        async: false,
        dataType: 'JSON',
        cache: false,
        success: function (result) {
            $('#input_unitname').empty();
            $('#qunitname').empty();
            $('#update_unittab').empty();

            var tmp = 0;
            $.each(result['unit'], function (key, value) {////////////////////////////// หน่วยที่เลือก(unittab)
                $('#input_unitname')
                .append($("<option></option>")
                .attr("value", value['refnum'])
                .text(value['unitname']));
                $('#qunitname')
                .append($("<option></option>")
                .attr("value", value['refnum'])
                .text(value['unitname']));
                $('#update_unittab')
                .append($("<option></option>")
                .attr("value", value['refnum'])
                .text(value['unitname']));
            });
            $('#input_unitname').selectpicker('val');
            $('#qunitname').selectpicker('val');
            $('#update_unittab').selectpicker('val');
        },
        error: function (err) {
            alert(err);
        }
    });
}
function hideload() {
    document.getElementById("loading").style.display = 'none';
}
function showload() {
    document.getElementById("loading").style.display = 'block';
}
function update_unit34() {
    $.ajax({
        url: "selectexam/update_unit34",
        success: function (result) {
            alert("success");
        },
        error: function (err) {
            alert(err);
        }
    });
}
function insert() {
    $.ajax({
        url: 'selectexam/insert',
        type: 'GET',

        dataType: 'JSON',
        data: {unit4: unit4,
            postcode: postcode,
            item: $('#input_num').val(),
            yearin: yearin,
            navyid: navyid},
            success: function (e) {
                detail();
                $('#close').focus();
            }
        });

    }
    function viewtable() {
        $.ajax({
            url: 'selectexam/viewtable_selectexam',
            type: 'GET',
            dataType: 'JSON',
            data: {postcode: postcode,
                unit4, unit4},
                success: function (e) {
                    console.log(e);
                    $('#table_postcode tbody').empty();
                    var i = 0;
                    var txthtml = "";
                    $.each(e, function (index, val) {
                        /* iterate through array or object */
                        i++;
                        txthtml += "<tr>";

                        txthtml += "<td>" + val.belong + "</td>";
                        txthtml += "<td>" + val.name + " " + val.sname + "</td>";
                        txthtml += "<td>" + val.yearin + "</td>";
                        txthtml += "<td>" + val.id8 + "</td>";
                        txthtml += "<td>" + val.unitname + "</td>";
                        txthtml += "<td>" + val.item + "</td>";
                        if (val.oldyearin == null)
                        val.oldyearin = "";
                        txthtml += "<td>" + val.oldyearin + "</td>";
                        txthtml += "<td>";
                        txthtml += '<div class="pull-right" ><button type="button" onclick="edit(' + val.navyid + ');" id="selectexam_edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></button>';
                        txthtml += '<button type="button" id="selectexam_remove" onclick="remove(' + val.navyid + ');" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></td></div>';
                        txthtml += "</tr>";

                    });
                    if (i < 5) {
                        for (var c = i; c < 5; c++) {
                            txthtml += "<tr>";
                            txthtml += "<td>-</td>";
                            txthtml += "<td></td>";
                            txthtml += "<td></td>";
                            txthtml += "<td></td>";
                            txthtml += "<td></td>";
                            txthtml += "<td></td>";
                            txthtml += "<td></td>";
                            txthtml += "<td></td>";
                            txthtml += "</tr>";
                        }
                    }
                    $('#table_postcode tbody').append(txthtml).hide().fadeIn(500);
                }
            })
            .done(function () {
                console.log("success");
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });

        }

        function edit(id) {
            navyid = id;
            detail();
        }
        function remove(id) {
            $.ajax({
                url: 'selectexam/remove',
                type: 'GET',
                dataType: 'JSON',
                data: {navyid: id},
            })
            .done(function () {
                console.log("success");
                if (id == navyid) {
                    detail();
                }
                viewtable();
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });

        }
        function count1() {
            $.ajax({
                url: 'selectexam/count',
                type: 'GET',
                dataType: 'JSON',
            })
            .done(function (result) {
                console.log("success");
                $('#count').empty();
                var txt = '<div class="col-xs-4" id="label"><b>หน่วย</b></div>'
                + '<div class="col-xs-2" id="count_amount_selectexam"><b>ต้องการ</b></div>'
                + '<div class="col-xs-3" id="count_selectexam"><b>คัดเลือก</b></div>'
                + '<div class="col-xs-3" id="count_selectexam_unit3"><b>ได้ตามคัดเลือก</b></div>';
                $.each(result, function (index, val) {

                    if (val.unit != 'ไม่ได้ระบุ') {
                        if (parseInt(val.count2) == parseInt(val.all) && parseInt(val.count) <= parseInt(val.count2)) {
                            console.log(val.unit + " " + val.count + " = " + val.all);
                            txt += '<div class="col-xs-4 tsuccess"  id="label'+val.refnum+'"><b>' + val.unit + '</b></div>'

                            + '<div class="col-xs-2 tsuccess" id="count_amount_selectexam'+val.refnum+'">' + val.all + '</div>'
                            + '<div class="col-xs-3 tsuccess" id="count_selectexam'+val.refnum+'">' + val.count + '</div>'
                            + '<div class="col-xs-3 tsuccess" id="count_selectexam_unit3'+val.refnum+'">' + val.count2 + '</div>';

                        } else if (parseInt(val.count2) > parseInt(val.all))
                        {
                            console.log(val.unit + " " + val.count + " > " + val.all);
                            txt += '<div class="col-xs-4 twarning" id="label'+val.refnum+'"><b>' + val.unit + '</b></div>'

                            + '<div class="col-xs-2 twarning" id="count_amount_selectexam'+val.refnum+'">' + val.all + '</div>'
                            + '<div class="col-xs-3 twarning" id="count_selectexam'+val.refnum+'">'+ val.count + '</div>'
                            + '<div class="col-xs-3 twarning" id="count_selectexam_unit3'+val.refnum+'">' + val.count2 + '</div>';
                        } else
                        {
                            console.log(val.unit + " " + val.count + " < " + val.all);
                            txt += '<div class="col-xs-4" id="label'+val.refnum+'"><b>' + val.unit + '</b></div>'

                            + '<div class="col-xs-2" id="count_amount_selectexam'+val.refnum+'">' + val.all + '</div>'
                            + '<div class="col-xs-3" id="count_selectexam'+val.refnum+'">' + val.count + '</div>'
                            + '<div class="col-xs-3" id="count_selectexam_unit3'+val.refnum+'">' + val.count2 + '</div>';
                        }
                    }

                });
                $('#count').append(txt).hide().fadeIn(500);

            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });

        }
        function countall() {
            $.ajax({
                url: 'selectexam/countall',
                type: 'GET',
                dataType: 'JSON',
            })
            .done(function (result) {
                console.log("success");
                $('#countall').empty();
                var txt = '<div class="col-xs-5" id="label_total"><b>หน่วย</b></div>'

                + '<div class="col-xs-3" id="count_amount"><b>ยอดต้องการ</b></div>'
                + '<div class="col-xs-3"id="count_total"><b>ยอดปัจจุบัน</b></div>';
                $.each(result, function (index, val) {
                    if (val.unit != 'ไม่ได้ระบุ') {
                        if (parseInt(val.count) == parseInt(val.all)) {
                            console.log(val.unit + " " + val.count + " = " + val.all);
                            txt += '<div class="col-xs-5 tsuccess" id="label_total'+val.refnum+'"><b>' + val.unit + '</b></div>'

                            + '<div class="col-xs-3 tsuccess" id="count_amount'+val.refnum+'">' + val.all + '</div>'
                            + '<div class="col-xs-3 tsuccess" id="count_total'+val.refnum+'">' + val.count + '</div>';

                        } else if (parseInt(val.count) > parseInt(val.all))
                        {
                            console.log(val.unit + " " + val.count + " > " + val.all);
                            txt += '<div class="col-xs-5 twarning" id="label_total'+val.refnum+'"><b>' + val.unit + '</b></div>'

                            + '<div class="col-xs-3 twarning" id="count_amount'+val.refnum+'">' + val.all + '</div>'
                            + '<div class="col-xs-3 twarning" id="count_total'+val.refnum+'">' + val.count + '</div>';
                        } else
                        {
                            console.log(val.unit + " " + val.count + " < " + val.all);
                            txt += '<div class="col-xs-5" id="label_total'+val.refnum+'"><b>' + val.unit + '</b></div>'

                            + '<div class="col-xs-3" id="count_amount'+val.refnum+'">' + val.all + '</div>'
                            + '<div class="col-xs-3" id="count_total'+val.refnum+'">' + val.count + '</div>';
                        }
                    }
                });
                $('#countall').append(txt).hide().fadeIn(500);
                unit = $('#update_unittab').selectpicker('val');

                if(unit!=0){
                    var Bigtotal = parseInt($('#count_amount'+unit).text()) - parseInt($('#count_total'+unit).text());
                    var selectexamtotal =  parseInt($('#count_selectexam'+unit).text()) - parseInt($('#count_selectexam_unit3'+unit).text())
                    if(Bigtotal<=selectexamtotal){
                        $('#update_limit').val(Bigtotal).attr('max',Bigtotal);
                    }
                    else{
                        $('#update_limit').val(selectexamtotal).attr('max',selectexamtotal);
                    }
                }
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });

        }
        function unittabseting() {
            $.ajax({
                url: 'selectexam/amount',
                type: 'GET',
                dataType: 'JSON',
            })
            .done(function (e) {
                console.log(e.length);
                $('#amount').empty();
                txt = '';
                $.each(e, function (index, item) {
                    if (item.refnum > 0) {
                        txt += '<div class="form-group">'
                        + '<div class="col-xs-6">'
                        + '<label for="input' + item.refnum + '" >' + item.unitname + '</label>'
                        + '</div>'
                        + '<div class="col-xs-6">'
                        + '<input type="number" class="form-control" onchange="saveamount();" onClick="SelectAll(input' + item.refnum + ');"'
                        + 'id="input' + item.refnum + '" value="' + item.count + '"/>'
                        + '</div>'
                        + '</div>'
                        + '</div>';
                    }
                });
                $('#amount').append(txt).hide().fadeIn(500);

            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });
        }
        function saveamount() {
            data = "";
            for (var i = 1; i <= 30; i++) {
                data += '&' + i + "=" + $('#input' + i).val();
            }
            $.ajax({
                url: 'request/saveamount?' + data,
                type: 'GET',
                dataType: 'JSON',
            })
            .done(function () {
                console.log("success");
                count1();
                countall();
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });
        }
        function table_tab2() {
            $.ajax({
                url: 'selectexam/selectexam_table_new',
                type: 'GET',
                dataType: 'JSON',
                data: 'sql=selectexam.unit4=' + d,
            })
            .done(function (e) {

                var txt = '';
                $('#select_refnum tbody').empty();
                $.each(e, function (index, val) {
                    if (val.title != 'ปกติ')
                    txt += '<tr style="background-color:rgb(244, 220, 66); " navyid="' + val.navyid + '">';
                    else
                    txt += '<tr navyid="' + val.navyid + '">';

                    txt += '<td nowrap >' + val.name + ' ' + val.sname + '</td>'
                    + '<td nowrap >' + val.refnum3 + '</td>'//หน่วยปัจจุบัน
                    + '<td nowrap >' + val.refnum4 + '</td>'
                    + '<td nowrap >' + val.postname + '</td>'
                    + '<td nowrap >' + val.item + '</td>'
                    //+ '<td>'+val.remark+'</td>'
                    // + '<td>'+val.remark2+'</td>'
                    + '<td nowrap >' + val.title + '</td>'
                    + '</tr>';
                });
                $('#select_refnum').append(txt).hide().fadeIn(500);
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });
        }
        function detail_manual() {   ///////////////////////////////////รายละเอียดข้อมูล

            $("#detail_name_manual").empty();
            $("#detail_sname_manual").empty();
            $("#detail_height_manual").empty();
            $("#patient_status_manual").css("background-color", "").empty();
            $("#detail_id8_manual").empty();
            $("#detail_statustab_manual").css("background-color", "").empty();
            $("#detail_yearin_manual").empty();
            $("#detail_religion_manual").empty();
            $("#detail_belong_to_manual").empty();
            $("#detail_eductab_manual").empty();
            $("#detail_is_request_manual").css("background-color", "").empty();
            $("#detail_percent_manual").empty();
            $("#detail_uint3_manual").empty();
            $("#detail_uint4_manual").empty();
            $("#detail_uint1_manual").empty();
            $("#detail_uint2_manual").empty();
            $("#detail_skill_manual").empty();
            $('#input_unitname4').val('');
            $.ajax({
                url: "request/query_detail",
                data: "navyid=" + navyid,

                type: 'GET',
                dataType: 'JSON',
                cache: false,
                success: function (result) {
                    obj = result[0];
                    yearin = obj.yearin;
                    id8 = obj.id8;
                    name = obj.name;
                    sname = obj.sname;
                    cunit3 = obj.refnum3;
                    $("#detail_name_manual").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
                    $("#detail_height_manual").append(obj.height);
                    if (obj.patient_status != null) {
                        if (obj.patient_status != "")
                        $('#patient_status_manual').append(obj.patient_status).css("background-color", "#ff4000    ");
                    }
                    $('#detail_religion_manual').append(obj.religion);
                    $("#detail_id8_manual").append(obj.id8);
                    if (obj.statuscode != "AA") {
                        $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")").css("background-color", "yellow");
                    } else {
                        $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")");
                    }
                    if (obj.oldyerin != null) {
                        $("#detail_yearin_manual").append(obj.yearin + "(" + obj.oldyerin + ")");
                    } else {
                        $("#detail_yearin_manual").append(obj.yearin);
                    }
                    if (obj.pseq < 10) {
                        $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + "0" + obj.pseq + ")");
                    } else {
                        $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + obj.pseq + ")");
                    }
                    $("#detail_eductab_manual").append(obj.educname);
                    if (obj.is_request != "000") {
                        $("#detail_is_request_manual").append("ร้องขอ").css("background-color", "#40ff00");
                    }
                    $("#detail_percent_manual").append(obj.percent);
                    $("#detail_uint3_manual").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
                    if ((obj.postcode == null) && (obj.item == null))
                    $("#detail_uint4_manual").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
                    else
                    $("#detail_uint4_manual").append(obj.refnum4 + " <b>หน่วย</b> " + obj.postname + " <b>ลำดับ</b> " + obj.item);
                    $("#detail_uint1_manual").append(obj.refnum1);
                    $("#detail_uint2_manual").append(obj.refnum2);
                    $("#detail_skill_manual").append(obj.skill);
                    $('#input_unitname4').val(obj.unit3 + ' ' + obj.refnum3);
                },
                error: function (err) {
                    alert(err);
                }
            });
        }
        function activetab(i){
            if($('#tab1').hasClass('active')){
                $('#tab1').removeClass('active');
            }
            if($('#panelRequest1').hasClass('in active')){
                $('#panelRequest1').removeClass('in active');

            }
            if($('#tab2').hasClass('active')==false){
                $('#tab2').addClass('active');
            }
            if($('#panelRequest2').hasClass('in active')==false){
                $('#panelRequest2').addClass('in active');
            }
            $('#input_unitname2').val(i);
            d = i
            table_tab2();
        }
        function loadingStart(id){
            $('#'+id+'').empty();
            $('#'+id+'').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            console.log('loadingStart '+id);
        }
        function loadingStop(id){
            $('#'+id+'').empty();
            console.log('loadingStop '+id);
        }

        function search(){
            if(navyid==null){
                datatable.clear().draw();/////////////////////////// clear datatable
                loadingStart('loading_Search');
                if($('#search_name').val()!="" || $('#search_sname').val()!="" || $('#search_id8').val()!="" ){
                    $.ajax({////////////////////////////// ส่ง ajax ไป ค้นหา
                        url: "<?php echo site_url('search_person_controller/search')?>",
                        type: 'GET',
                        dataType: 'JSON',
                        data: {name: $('#search_name').val(),
                        sname: $('#search_sname').val(),
                        id8: $('#search_id8').val()},
                    })
                    .done(function (result) {
                        console.log(result);
                        navyid = result[0].navyid;
                        datatable.rows(datatable.rows.add(result).draw()).nodes().to$().attr("id", result.navyid);
                        row = $(this).closest('tr');
                        $('#search_submit').focus();
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function () {
                        console.log("complete");
                        loadingStop('loading_Search');

                    });
                }
                else{
                    alert('ไม่สามารถค้นหาช่องว่างได้');
                    loadingStop('loading_Search');
                    $('#search_name').focus();
                }
            }else{


                console.log(navyid);
                detail();
                $('#modal-default').modal('show');
            }
        }
        function print(){
            str =""
            if($('#oSortby').selectpicker('val')==1){
                str += "orderby=person.unit3 asc,selectexam.postcode asc,selectexam.item asc &";
            }
            if($('#oSortby').selectpicker('val')==2){
                str += "orderby=selectexam.postcode asc,selectexam.item asc &";
            }
            str += "where=selectexam.navyid is not null";
            if($('#qunitname').selectpicker('val')!=0){
                if($('#select_order').selectpicker('val')==1){
                    str += " and selectexam.unit4="+$('#qunitname').selectpicker('val');
                }
                else if($('#select_order').selectpicker('val')==2){
                    str += " and person.unit3="+$('#qunitname').selectpicker('val');
                }
                if($('#qPosition').selectpicker('val')!=0){
                    str += " and selectexam.postcode="+$('#qPosition').selectpicker('val');
                }
                if($('#cAskby').selectpicker('val')==2){
                    str += ' and request.navyid is null and person.statuscode ="AA"'
                }
                else if($('#cAskby').selectpicker('val')==3){
                    str += ' and request.navyid is not null';
                }
                else if($('#cAskby').selectpicker('val')==4){
                    str += ' and person.statuscode !="AA"';
                }
            }else{
                if($('#qPosition').selectpicker('val')!=0){
                    str += " and selectexam.postcode="+$('#qPosition').selectpicker('val');

                }
                if($('#cAskby').selectpicker('val')==2){
                    str += ' and request.navyid is null and person.statuscode ="AA"'
                }
                else if($('#cAskby').selectpicker('val')==3){
                    str += ' and request.navyid is not null';
                }
                else if($('#cAskby').selectpicker('val')==4){
                    str += ' and person.statuscode!="AA"';
                }

            }

            window.open("<?php echo site_url('selectexam/pdf?')?>"+str,"_blank");
        }
        function update_selectexam(){
          if($('#update_limit').val()!=0){
            str = "where=person.unit3=0 and selectexam.unit4="+$('#update_unittab').selectpicker('val')+"&limit="+$('#update_limit').val();

            $.ajax({
                url: '<?php echo site_url('selectexam/update_unit3')?>',
                type: 'GET',
                dataType: 'JSON',
                data: str,
            })
            .done(function(result) {
                console.log("success");
                $.alertable.alert("Error"+result);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {

                count1();
                countall();
                unittabseting();
                console.log("complete");

            });
}else{
  $.alertable.alert("ไม่สามารถอัพเดท 0 จำนวนได้");
}
        }
        </script>
