<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ค้นหาข้อมูล ทหารพลกองประจำการ
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-info" id="search_box">
                    <div class="box-header with-border">
                        <h3 class="box-title">ฟอร์มค้นหา</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body" id="form">
                            <div id="notification"></div>
                            <div class="form-group" >
                                <div class="col-md-3">
                                    <label>ผลัด</label>
                                    <input type="text" class="form-control" id="yearin" placeholder="_/__">
                                </div>
                                <div class="col-md-5">
                                    <label>เลขประจำตัวประชาชน</label>
                                        <input type="text" class="form-control" id="id13">
                                </div>
                                <div class="col-md-2">
                                        <label>ทะเบียน</label>
                                        <input type="text" class="form-control" id="id8" placeholder="_._.____">
                                </div>
                                <div class="col-md-2">
                                    <label>รหัส</label>
                                        <input type="text" class="form-control" id="runcode">
                                </div>
                                <div class="col-md-4">
                                    <label>ชื่อ</label>
                                        <input type="text" class="form-control" id="name" placeholder="ชื่อ">
                                </div>
                                <div class="col-md-4">
                                    <label>นามสกุล</label>
                                        <input type="text" class="form-control" id="sname" placeholder="นามสกุล">
                                </div>
                                <div class="col-md-4">

                                    <label>จังหวัด</label>
                                        <input type="text" class="form-control" id="towncode" list="towncode_list">
                                        <datalist id="towncode_list">
                                        </datalist>
                                    </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="button" class="btn btn-default">clear</button>
                                <button type="button" id="btn_search" class="btn btn-info pull-right"><i class="fa fa-search"></i>&nbsp;Search</button>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default" id="result_box">
                    <div class="box-header">
                        <h3 class="box-title">ผลการค้นหา</h3>
                        <p class="box-title pull-right" id="num_row"></p>
                    </div>
                    <!-- /.box-header -->

<?php $this->load->view('modal_print');?>
                    <div class="box-body">

                        <table id="result_search" class="table table-condensed " >
                            <thead>
                                <tr>
                                    <th  class="text-center">ผลัด</th>
                                    <th  class="text-center">ชื่อ</th>
                                    <th  class="text-center">นามสกุล</th>
                                    <th  class="text-center">กองร้อย</th>
                                    <th  class="text-center">กองพัน</th>
                                    <th  class="text-center">ทะเบียน</th>
                                    <th  class="text-center">หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
