  ﻿<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js">
  </script>
  <script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js">
  </script>
  <script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">
  </script>
  <script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js">
  </script>
  <script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js">
  </script>
  <script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js">
  </script>
  <script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js">
  </script>
  <script type="text/javascript">
      var navyid;
      var yearin;
      var oldyearin;
      var id8;
      var name;
      var sname;
      var unitname;
      var askcode;
      var asktext;
      var num;
      var remark;
      var remark2;
      var selectid;
      var refnum;
      var confirmcodeunit;
      var unit3;
      var cunit3;
      var id13;
      var tmp = 0;
          $body = $("body");
              $(document).on({
      ajaxStart: function() { $body.addClass("loading");    },
       ajaxStop: function() { $body.removeClass("loading"); }
  });
      $(document).ready(function () {
          towncode();
          //////////////////////progress bar/////////////////////////////////////////
          var progress = parseInt(10 / 100 * 100, 10);
          $('#progress .progress-bar').css(
                  'width',
                  progress + '%'
                  );
          /////////////////////////////////////////////////////enter in input//////////////////////////////////////////////
          $('#form input').keypress(function (e) {

              if (e.which == 13) {
                  inserttable();
              }
          });
          $('#btn_search').on('click',function () {
                  inserttable();
          });
          $('#yearin').keyup(function(event){
            var str = $('#yearin').val().split("");

            if(event.which!=8){
            if(str[0]!=null && str[1]!='/'){
              $('#yearin').val(str[0]+'/');
            }
            if(str[2]=='/'){
              $('#yearin').val(str[0]+'/');
            }
          }
          });
          $('#id8').keyup (function(event){
            console.log(event);
            if(event.which!=8){
              var str=$('#id8').val().split("");
             if(str[0]!=null && str[1]!='.')
                $('#id8').val(str[0]+'.');
              }
              if(str[2]=="."){
                $('#id8').val(str[0]+'.');
              }
              else if(str[2]!=null && str[3]!='.'){
                 $('#id8').val(str[0]+str[1]+str[2]+'.0000');
               }
             if(str[8]!=null){
               str[4]=str[5];
               str[5]=str[6];
               str[6]=str[7];
               str[7]=str[8];
               $('#id8').val(str[0]+str[1]+str[2]+str[3]+str[4]+str[5]+str[6]+str[7]);
             }
          });
          /////////////////////////////////////////////////////end enter input////////////////////////////////////////////////////

          ////////////////////////////////////////////////////click table////////////////////////////////////////////////////////
          $('#result_search tbody').on('dblclick', 'tr', function () {
              navyid = $(this).attr('id');
              detail();
          });
          $('#result_search tbody').on('click', 'tr', function () {
              if ($(this).hasClass('selected')) {
                  $(this).removeClass('selected');
              } else {
                  table.$('tr.selected').removeClass('selected');
                  $(this).addClass('selected');
              }
          });
          ////////////////////////////////////////////////////end click table///////////////////////////////////////////////////


          ///////////////////////////////////////////////////end click table request////////////////////////////////////////////////////////

          ////////////////////////////////////////////////////set table//////////////////////////////////////////////////////////
          table = $('#result_search').DataTable({
              searching: false,
              dom: 'Bfrtip',
               buttons: [
              'excel', 'print'
              ],
              "fnCreatedRow": function (nRow, aData, iDataIndex) {
                  $(nRow).attr('id', aData["navyid"]);
              },
              "columns": [{
                      "title": "ผลัด",
                      "data": "yearin"
                  }, {
                      "title": "ชื่อ",
                      "data": "name"
                  }, {
                      "title": "นามสกุล",
                      "data": "sname"
                  }, {
                      "title": "กองร้อย",
                      "data": "company"
                  }, {
                      "title": "กองพัน",
                      "data": "batt"
                  }, {
                      "title": "ทะเบียน",
                      "data": "id8"
                  }, {
                      "title": "หมายเหตุ",
                      "data": "oldyearin"
                  }]
          });
          ///////////////////////////////////////////////end set table/////////////////////////////////////////////////////
      });
      /////////////////////////////////////// queryข้อมูลจาก databast ลง table//////////////////////////////////////////////
      function inserttable() {
          table.clear().draw();
          if($('#yearin').val()!=''|| $('#id13').val()!=''|| $('#runcode').val()!=''|| $('#name').val()!=''|| $('#sname').val()!=''|| $('#id8').val()!=''|| towncode!=''){
          var towncode = "";
                  towncode = $('#towncode').val();
                  towncode = towncode.substring(0, 2);
          $.ajax({
              url: "search/query",
              data: "yearin=" + $('#yearin').val() + '&id13='+$('#id13').val() +  "&runcode=" + $('#runcode').val() + "&name=" + $('#name').val() + "&sname=" + $('#sname').val() + "&id8=" + $('#id8').val() + "&towncode=" + towncode,
              timeout: 300000, // 3 second
              type: 'GET',
              dataType: 'JSON',
              cache: false,
              success: function (result) {
                  var count = Object.keys(result).length;
                  var danger_notification = "";
                  danger_notification += '<div class="alert alert-danger alert-dismissible">';
                  danger_notification += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                  danger_notification += '<h4><i class="icon fa fa-check"></i>ไม่พบรายการ</h4>';
                  danger_notification += '</div>'
                  var success_notification = "";
                  success_notification += '<div class="alert alert-success alert-dismissible">';
                  success_notification += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                  success_notification += '<h4><i class="icon fa fa-check"></i>พบพลทหารจำนวน ' + count + ' นาย</h4>';
                  success_notification += '</div>'
                  if (count == 0) {
                      $("#result_box").removeAttr('class');
                      $("#result_box").attr('class', 'box box-danger');
                      $("#search_box").removeAttr('class');
                      $("#search_box").attr('class', 'box box-danger');
                      $('#notification').empty();
                      $(danger_notification).appendTo("#notification").hide().slideDown(1000).fadeOut(2000);
                      $('#num_row').html("ไม่พบรายการ");
                  } else {
                      $("#result_box").removeAttr('class');
                      $("#result_box").attr('class', 'box box-success');
                      $("#search_box").removeAttr('class');
                      $("#search_box").attr('class', 'box box-success');
                      $('#notification').empty();
                      $(success_notification).appendTo("#notification").hide().slideDown(1000).fadeOut(2000);
                      $('#num_row').html("พบพลทหารจำนวน " + count + " นาย");
                  }

                  var i = table.rows.add(result).draw();
                  table.rows(i).nodes().to$().attr("id", result['navyid']);
              },
              error: function (err) {
                  alert(err);
              }
          });
        }
        else{
          alert('ไม่สามารถค้นหาช่องว่างได้');
        }
      }
      function detail() {   ///////////////////////////////////รายละเอียดข้อมูล

          $("#detail_name").empty();
          $("#detail_name").empty();
          $("#detail_sname").empty();
          $("#detail_height").empty();
          $("#patient_status").empty();//.css("background-color", "").empty();
          $("#detail_id8").empty();
          $("#statuscode").empty();//.css("background-color", "").empty();
          $("#detail_yearin").empty();
          $("#detail_religion").empty();
          $("#detail_belong_to").empty();
          $("#detail_is_request").css("background-color", "").empty();
          $("#detail_percent").empty();
          $("#detail_uint3").empty();
          $("#detail_uint4").empty();
          $("#detail_uint1").empty();
          $("#detail_uint2").empty();
          $("#address").empty();
          $("#detail_blood").empty();
          $("#detail_father").empty();
          $("#detail_mother").empty();
          $("#detail_unit3").empty();
          $('#detail_company').empty();
          $('#detail_batt').empty();
          $('#detail_platoon').empty();
          $('#detail_pseq').empty();
          $("#detail_address").empty();
          $("#detail_address_mu").empty();
          $("#detail_address_soil").empty();
          $("#detail_address_road").empty();
          $("#detail_town3").empty();
          $("#detail_town2").empty();
          $("#detail_town1").empty();
          $("#detail_zipcode").empty();
          $("#detail_educname").empty();
          $("#detail_id").empty();
          $("#detail_unit1").empty();
          $("#detail_unit2").empty();
          $('#is_request').empty();
          $("#detail_regdate").empty();
          $("#detail_repdate").empty();
          $("#detail_movedate").empty();
          $("#detail_runcode").empty();
          $("#detail_birthdate").empty();
          $("#detail_id13").empty();
          $("#detail_mark").empty();
          $('#addressmove').empty();
          $('#patient_status').empty();
          $('#statuscode').empty();
          $('#detail_swimcode').empty();
          $('#detail_skillname').empty();
          $('#detail_occname').empty();
          $('#detail_schoolname').empty();
          $('#detail_mcp').empty();
          $('#detail_mc').empty();
          $('#detail_mc4').empty();
          $('#detail_groupname').empty();
          $('#detail_mc5').empty();
          $('#detail_start').empty();
          $.ajax({
              url: "search/query_detail",
              data: "navyid=" + navyid,
              timeout: 7000, // 7 second
              type: 'GET',
              dataType: 'JSON',
              cache: false,
              success: function (result) {
                  obj = result[0];
                  yearin = obj.yearin;
                  oldyearin = obj.oldyearin;
                  id8 = obj.id8;
                  name = obj.name;
                  sname = obj.sname;
                  cunit3 = obj.refnum3;
                  img();
                  $("#detail_name").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
                  $("#detail_height").append(obj.height);
                  if (obj.patient_status != null){
                          $('#patient_status').append(obj.patient_status);//.css("background-color", "#ff4000	");
                          $('#btn_patient_status').removeClass("btn-success");
                          $('#btn_patient_status').addClass("btn-danger");
                      }
                      else{
                          $('#btn_patient_status').removeClass("btn-danger");
                          $('#btn_patient_status').addClass("btn-success");
                      $('#patient_status').append("ปกติ");
                  }
                  $('#detail_religion').append(obj.religion);
                  var regyear='';
                  var year='';
                  if(obj.oldyerin!=null){
                    year = obj.oldyearin;
                  }
                  else{
                    year = obj.yearin;
                  }
                  if(year.substring(0, 1)=="4"){
                    regyear = (parseInt(year.substring(2, 4))+1);
                  }
                  else{
                    regyear = (parseInt(year.substring(2, 4)));
                  }
                  $("#detail_id8").append("ทร."+regyear+" "+obj.id8);
                  if (obj.statuscode != "AA") {
                      $("#statuscode").append(obj.title);//.css("background-color", "yellow");
                       $('#btn_statuscode').removeClass("btn-success");
                          $('#btn_statuscode').addClass("btn-danger");
                      }
                      else{
                          $('#btn_statuscode').removeClass("btn-danger");
                          $('#btn_statuscode').addClass("btn-success");
                       $("#statuscode").append(obj.title);
                  }
                  if (obj.oldyerin != null) {
                      $("#detail_yearin").append(obj.yearin + "(" + obj.oldyerin + ")");
                  } else {
                      $("#detail_yearin").append(obj.yearin);
                  }
                  $('#detail_company').append(obj.company);
                  $('#detail_batt').append(obj.batt);
                  $('#detail_platoon').append(obj.platoon);
                  $('#detail_pseq').append(obj.pseq);

                  $("#detail_educname").append(obj.educname);
                  if (obj.is_request != "000") {

                      $('#is_request').append('<i class="fa fa-check"></i>');

                      //$("#detail_is_request").append("ร้องขอ").css("background-color", "#40ff00");
                  } else {
                      $('#is_request').append('<i class="fa fa-close"></i>');
                  }
                  if (obj.AddressMove == '1') {
                      $('#addressmove').append('<i class="fa fa-check"></i>');
                  } else {
                      $('#addressmove').append('<i class="fa fa-close"></i>');
                  }
                  $("#detail_percent").append(obj.percent);
                  $("#detail_uint3").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
                  if ((obj.postcode == null) && (obj.item == null))
                      $("#detail_uint4").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
                  else
                      $("#detail_uint4").append(obj.unit4 + " <b>หน่วย</b> " + obj.postcode + " <b>ลำดับ</b> " + obj.item);
                  $("#detail_uint1").append(obj.refnum1);
                  $("#detail_uint2").append(obj.refnum2);
                  $("#detail_address").append(obj.address + " ม." + obj.address_mu);
                  $("#detail_address_mu").append();
                  if (obj.address_soil == "") {
                      obj.address_soil = " - ";
                  }
                  $("#detail_address_soil").append(obj.address_soil);
                  if (obj.address_road == "") {
                      obj.address_road = " - ";
                  }
                  if(obj.refnum3 == "" || obj.refnum3 == null){
                      $("#printContent5").hide();
                      $("#detail_movedate").empty();
                      $("#detail_unit3").empty();
                  }
                  else{
                      $("#printContent5").show();
                  }
                  $("#detail_address_road").append(obj.address_road);
                  $("#detail_town3").append(obj.town3);
                  $("#detail_town2").append(obj.town2);
                  $("#detail_town1").append(obj.town1);
                  $("#detail_zipcode").append(obj.zipcode);
                  $("#detail_blood").append(obj.blood);
                  $("#detail_father").append(obj.father + " " + obj.fsname);
                  $("#detail_mother").append(obj.mother + " " + obj.msname);
                  $("#detail_unit3").append(obj.refnum3);
                  $("#detail_id").append(obj.id);
                  $("#detail_unit1").append(obj.refnum1);
                  $("#detail_unit2").append(obj.refnum2);
                  var regyear2,repdate,movedate,birthdate;
                  if(obj.regdate!=null){
                    regyear2 = date(obj.regdate);
                  }else{
                    regyear2='-';
                  }
                  if(obj.repdate!=null){
                    repdate = date(obj.repdate);
                  }else{
                    repdate='-';
                  }
                  if(obj.movedate!=null){
                    movedate = date(obj.movedate);
                  }else{
                    movedate='-';
                  }
                  if(obj.birthdate!=null){
                    birthdate = date(obj.birthdate);
                  }else{
                    birthdate='-';
                  }
                  $("#detail_regdate").append(regyear2);
                  $("#detail_repdate").append(repdate);
                  $("#detail_movedate").append(movedate);
                  $("#detail_runcode").append(obj.runcode);
                  $("#detail_birthdate").append(birthdate);
                  $("#detail_id13").append(obj.id13);
                  id13 = obj.id13;
                  $("#detail_mark").append(obj.mark);
                  $('#detail_swimcode').append(obj.swimcode);
                  $('#detail_skillname').append(obj.skill);
                  $('#detail_occname').append(obj.occname);
                  $('#detail_schoolname').append(obj.schoolname);

                  $('#detail_mcp').append(obj.mcp);
                  $('#detail_mc').append(obj.mc);
                  $('#detail_mc4').append(obj.mc4);
                  $('#detail_groupname').append(obj.groupname);
                  $('#detail_mc5').append(obj.mc5);
                  var start;
                  if(obj.start!=null){
                    start = date(obj.start);
                  }else{
                    start='-';
                  }
                  $('#detail_start').append(start);

              },
              error: function (err) {
                  alert(err);
              }
          });
          $('#detail').modal('show');
      }
      function set_val_input() {
          $('#input_unitname').val(refnum);
          $('#input_askby').val(askcode);
          $('#input_askby2').val(asktext);
          setnum();
          $('#input_remark').val(remark);
          $('#input_remark2').val(remark2);
      }
      var i = 0;
      $("#imgprofile").click(function () {
          if (i == 0) {
              $("#imgprofile").css("width", "2in");
              $("#imgprofile").css("heigh", "3in");
              $("#imgprofile").css("cursor", "zoom-out");
              i++;
          } else if (i == 1) {
              $("#imgprofile").css("width", "1in");
              $("#imgprofile").css("heigh", "1.5in");
              $("#imgprofile").css("cursor", "zoom-in");
              i = 0;
          }
      });


      function towncode() {
          $.ajax({
              url: "search/towncode",
              timeout: 3000, // 3 second
              type: 'GET',
              async: false,
              dataType: 'JSON',
              cache: false,
              success: function (result) {
                  $('#towncode').empty();
                  var tmp = 0;
                  $.each(result, function (key, value) {////////////////////////////// หน่วยที่เลือก(unittab)
                      $('#towncode_list')
                              .append($("<option></option>")
                                      .attr("value",value['towncode']+' ' +value['townname']));
                  });
              },
              error: function (err) {
                  alert(err);
              }
          });
      }

      function img() {
  var text="";
  if(oldyearin){
   text = "http://192.168.0.1/navyimages/" + oldyearin.substring(0, 1) + "." + oldyearin.substring(2) + "/" + navyid + ".jpg";

  }
  else{
      text = "http://192.168.0.1/navyimages/" + yearin.substring(0, 1) + "." + yearin.substring(2) + "/" + navyid + ".jpg";

  }
          $("#imgprofile").attr("src", text);
          $("#imgprofile").css("width", "1in");
          $("#imgprofile").css("heigh", "1.5in");

      }

      function printWindow()
      {
          var printReadyEle1 = document.getElementById("printContent1");
          var printReadyEle2 = document.getElementById("printContent2");
          var printReadyEle3 = document.getElementById("printContent3");
          var printReadyEle4 = document.getElementById("printContent4");
          var printReadyEle5 = document.getElementById("printContent5");
          var shtml = '<HTML>\n<HEAD>\n';
          if (document.getElementsByTagName != null)
          {
              var sheadTags = document.getElementsByTagName("head");
              if (sheadTags.length > 0)
                  shtml += sheadTags[0].innerHTML;

          }

          shtml += '</HEAD>\n<BODY onload="window.print();">\n';
          if (printReadyEle1 != null)
          {
              shtml += '<form name = frmform1>';//
              shtml += '<div class="row"><div class="col-xs-12"><div class="text-center"><font size="6"><b>ประวัติ</b></font></div></div></div>';
              shtml += '<div class="row"><div class="col-xs-1" ></div><div class="col-xs-3" >';//
              shtml += printReadyEle1.innerHTML;
              shtml += '</div><div class="col-xs-8" >';//
              shtml += printReadyEle2.innerHTML;
              shtml += '</div></div><div class="row"><div class="col-xs-1" ></div><div class="col-xs-11" >';//
              shtml += printReadyEle4.innerHTML;
              shtml += '</div></div><div class="row"><div class="col-xs-1" ></div><div class="col-xs-11" >';//
              shtml += '<hr></div></div><div class="row"><div class="col-xs-1" ></div><div class="col-xs-11" >';//
              shtml += printReadyEle3.innerHTML;
              shtml += printReadyEle5.innerHTML;
              shtml += '</div></div>';//
          }
          shtml += '\n</form>\n</BODY>\n</HTML>';

          var printWin1 = window.open();
          printWin1.document.open();
          printWin1.document.write(shtml);
          printWin1.document.close();
      }
      function date(str) {
        var text = "";
        if(parseInt(str.substring(5, 7))>=1 &&  parseInt(str.substring(5, 7))<=12){
          var text = "";
          var month = {1: 'มกราคม', 2: 'กุมภาพันธ์', 3: 'มีนาคม', 4: 'เมษายน', 5: 'พฤษภาคม', 6: 'มิถุนายน', 7: 'กรกฎาคม', 8: 'สิงหาคม', 9: 'กันยายน', 10: 'ตุลาคม', 11: 'พฤศจิกายน', 12: 'ธันวาคม'};
          text = str.substring(8, 10);
          text += " ";
          text += month[ parseInt(str.substring(5, 7)) ];
          text += " ";
          text += parseInt(str.substring(0, 4)) + 543;
        }
        else{
          text = "-";
        }
          return text;
      }
      function TranscriptionFrom(){

          window.open('http://192.168.0.1/TranscriptionForm/'+ yearin.substring(0, 1) + "." + yearin.substring(2)+'/'+id13+'.pdf','_blank');

      }
  </script>
