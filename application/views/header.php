<!DOCTYPE html>
<html>
<font size="2.5">
  <head id="head">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="<?php echo site_url();?>assets/images/icon.png">
    <title>ระบบกำลังพล</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="<?php echo site_url();?>assets/bootstrap/css/bootstrap-theme.min.css"> -->

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="
    https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo site_url();?>assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/ionicons/css/ionicons.min.css"><!-- daterange picker -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/datepicker/datepicker3.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/bootstrap-select/dist/css/bootstrap-select.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/dist/css/AdminLTE.min.css">
    <!-- Theme style -->
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo site_url();?>assets/DataTables/extensions/FixedColumns/css/fixedColumns.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo site_url();?>assets/plugins/jquery-alertable/jquery.alertable.css">
    <!-- <link rel="stylesheet" href="<?php echo site_url();?>assets/bootstrap-toggle/css/bootstrap-toggle.css"> -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/bootstrap-toggle/css/bootstrap-toggle.min.css">
    <style type="text/css">
    /* Start by setting display:none to make this hidden.
    Then we position it in relation to the viewport window
    with position:fixed. Width, height, top and left speak
    for themselves. Background we set to 80% white with
    our animation centered, and no-repeating */

    			.parent {
    				height: 300px;
    			}

    			.fixTable {
    				width: 1800px !important;
    			}
    table{
      border:1px solid #878787;
       border-bottom:1px solid #878787;
    }
    th,td{
      white-space:nowrap;
      border:1px solid #878787;
    }
    .form-control,.select2{
       border-color:#878787;
    }
    .tsuccess{
      background-color:#5cb85c;
    }
    .twarning{
      background-color:#f39c12;
    }
    .tdanger{
      background-color:#dd4b39;
    }
    input.form-control:focus{
    /* ทำให้เวลาคลิกแล้วมันจะเหมือมเด้งออกมา
 -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box; */
  -webkit-border-radius: 3px;
  border-radius: 3px;
  -o-text-overflow: clip;
  text-overflow: clip;
  -webkit-box-shadow: 2px 2px 17px 4px #828282 ;
  box-shadow: 2px 2px 17px 4px #828282 ;
    }
    hr{
       border-top: 1px solid #878787
    }
    .moda11111l {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
      url('http://i.stack.imgur.com/FhHRx.gif')
      50% 50%
      no-repeat;
    }

    /* When the body has the loading class, we turn
    the scrollbar off with overflow:hidden */
    body.loading {
      overflow: hidden;
    }

    /* Anytime the body has the loading class, our
    modal element will be visible */
    body.loading .moda11111l {
      display: block;
    }
    .modal.modal-wide .modal-dialog {
      width: 80%;
      border-bottom:1px solid #eee;
      background-color: #0480be;
    }
    .bd-detail {
      padding-top: .75rem;
      padding-bottom: .75rem;
      background-color: rgba(86,61,124,.15);
      border: 1px solid rgba(86,61,124,.2);
    }
    .bd-2 {
      padding-top: .75rem;
      padding-bottom: .75rem;
      background-color: rgba(86,61,124,.15);
      border: 1px solid rgba(86,61,124,.2);
    }
    </style>
  </head>
  <body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <a href="<?php echo site_url();?>main" class="navbar-brand"><i class="fa fa-anchor"></i><b>&nbsp;ระบบ</b>กำลังพล
              </a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
              <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <?php if ($controller=="search1" || $controller=="search") echo '<li class="dropdown active messages-menu">'; else echo '<li class="dropdown messages-menu">';?>
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-search"> งานสอบถามข้อมูล</i>
                  </a>
                  <ul class="dropdown-menu">
                    <?php if ($controller=="search") echo '<li class="active">'; else echo '<li>';?><a href="<?php echo site_url();?>search"><i class="fa fa-circle-o"></i> ทหารพลกองประจำการ</a></li>
                    <?php if ($controller=="search_education") echo '<li class="active">'; else echo '<li>';?><a href="<?php echo site_url();?>search_education"><i class="fa fa-circle-o"></i>ค้นหาตามวุฒิการศึกษา</a></li>
                    <?php if ($controller=="search_skill") echo '<li class="active">'; else echo '<li>';?><a href="<?php echo site_url();?>search_skill"><i class="fa fa-circle-o"></i>ค้นหาตามความสามารถพิเศษ</a></li>
                  </ul>
                </li>
                <?php if ($controller=="cansusRegistration") echo '<li class="active">'; else echo '<li>';?>
                  <a href="<?php echo site_url();?>cansusRegistration">
                    <i class="fa fa-circle-o"></i> ทะเบียนบ้าน</a></li>

                    <?php if ($controller=="request" || $controller=="incident" || $controller=="selectexam" || $controller=="education" || $controller=="SBPs5"  || $controller=="height"  || $controller=="voluteer" || $controller=="change") echo '<li class="dropdown active messages-menu">'; else echo '<li class="dropdown messages-menu">';?>
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-edit"> แบ่งทหาร</i>
                      </a>
                      <ul class="dropdown-menu">
                        <?php if ($controller=="request") echo '<li class="active">'; else echo '<li>';?>
                          <a href="<?php echo site_url();?>request">
                            <i class="fa fa-circle-o"></i> ร้องขอ</a></li>
                            <?php if ($controller=="incident") echo '<li class="active">'; else echo '<li>';?>
                              <a href="<?php echo site_url();?>incident">
                                <i class="fa fa-circle-o"></i> มีเหตุ</a></li>
                            <?php if ($controller=="selectexam") echo '<li class="active">'; else echo '<li>';?>
                              <a href="<?php echo site_url();?>selectexam">
                                <i class="fa fa-circle-o"></i> คัดเลือกหน่วย</a></li>

                                    <?php if ($controller=="education") echo '<li class="active">'; else echo '<li>';?>
                                      <a href="<?php echo site_url();?>education">
                                        <i class="fa fa-circle-o"></i> ความรู้/ความสามารถ/วุฒิการศึกษา</a></li>
                                        <?php if ($controller=="province") echo '<li class="active">'; else echo '<li>';?>
                                          <a href="<?php echo site_url();?>province">
                                            <i class="fa fa-circle-o"></i> แบ่งตามจังหวัด</a></li>
                                            <?php if ($controller=="voluteer") echo '<li class="active">'; else echo '<li>';?>
                                              <a href="<?php echo site_url();?>voluteer">
                                                <i class="fa fa-circle-o"></i> ความสมัครใจ</a></li>
                                                <?php if ($controller=="changeUnit") echo '<li class="active">'; else echo '<li>';?>
                                                  <a href="<?php echo site_url();?>changeUnit">
                                                    <i class="fa fa-circle-o"></i> สับเปลี่ยน</a></li>
                                                  </ul>
                                                </li>
                                                <?php if ($controller=="request_print" || $controller=="report") echo '<li class="dropdown active messages-menu">'; else echo '<li class="dropdown messages-menu">';?>
                                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-file-o"> รายงาน</i>
                                                  </a>
                                                  <ul class="dropdown-menu">
                                                    <?php if ($controller=="request" && $method == "pdf") echo '<li class="active">'; else echo '<li>';?>
                                                      <a href="<?php echo site_url();?>request/pdf?sql=">
                                                        <i class="fa fa-circle-o"></i> รายชื่อร้องขอ</a></li>
                                                        <?php if ($controller == "selectexam" && $method == "pdf") echo '<li class="active">';
                                                        else echo '<li>'; ?>
                                                          <a href="<?php echo site_url(); ?>selectexam/pdf?=">
                                                            <i class="fa fa-circle-o"></i> รายชื่อคัดเลือก</a></li>
                                                            <?php if ($controller == "report" && $method == "person_asc") echo '<li class="active">';
                                                            else echo '<li>'; ?>
                                                              <a href="<?php echo site_url(); ?>report/person_asc">
                                                                <i class="fa fa-circle-o"></i> รายชื่อตามอักษร</a></li>
                                                              </ul>
                                                            </li>
                                                            <?php if ($controller=="fromInsert") echo '<li class="dropdown active messages-menu">'; else echo '<li class="dropdown messages-menu">';?>
                                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="fa fa-file-o"> กรอกข้อมูล</i>
                                                              </a>
                                                              <ul class="dropdown-menu">
                                                                <?php if ($controller=="fromInsert") echo '<li class="active">'; else echo '<li>';?>
                                                                  <a href="<?php echo site_url();?>fromInsert">
                                                                    <i class="fa fa-circle-o"></i> เพิ่มข้อมูล</a></li>
                                                                  </ul>
                                                                </li>
                                                                <li class="dropdown user user-menu">
                                                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                    <img src="http://192.168.0.1/webnavy/img/<?php echo $img;?>" class="user-image" alt="User Image">
                                                                    <span class="hidden-xs"><?php if($firstname == "" and $lastname == "")
                                                                    {
                                                                      echo "firstname lastname";
                                                                    }
                                                                    else

                                                                    echo $firstname." ".$lastname?></span>
                                                                  </a>
                                                                  <ul class="dropdown-menu">
                                                                    <!-- User image -->
                                                                    <li class="user-header">
                                                                      <img src="http://192.168.0.1/webnavy/img/<?php echo $img;?>" class="img-circle" alt="User Image">

                                                                      <p>
                                                                        <?php echo $email?>
                                                                        <small>Member since Nov. 2012</small>
                                                                      </p>
                                                                    </li>
                                                                    <!-- Menu Body -->
                                                                    <li class="user-body">
                                                                      <div class="row">
                                                                        <div class="col-xs-4 text-center">
                                                                          <a href="#">Followers</a>
                                                                        </div>
                                                                        <div class="col-xs-4 text-center">
                                                                          <a href="#">Sales</a>
                                                                        </div>
                                                                        <div class="col-xs-4 text-center">
                                                                          <a href="#">Friends</a>
                                                                        </div>
                                                                      </div>
                                                                      <!-- /.row -->
                                                                    </li>
                                                                    <!-- Menu Footer-->
                                                                    <li class="user-footer">
                                                                      <div class="pull-left">
                                                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                                                      </div>
                                                                      <div class="pull-right">
                                                                        <a href="<?php echo site_url('login/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                                                                      </div>
                                                                    </li>
                                                                  </ul>
                                                                </li>
                                                                <!-- Control Sidebar Toggle Button -->
                                                                <li>
                                                                  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                                                </li>
                                                              </ul>
                                                            </div>
                                                          </div>
                                                          </nav>
                                                        </header>
                                                        <!-- Left side column. contains the logo and sidebar -->
                                                        <body>
