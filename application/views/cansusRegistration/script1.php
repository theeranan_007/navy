
<script  type="text/javascript" >
    $body = $("body");
    $(document).on({
        ajaxStart: function() { $body.addClass("loading");    },
        ajaxStop: function() { $body.removeClass("loading"); }
    });
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
  });
    var currentDate = new Date();
$("#out_date").datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true              //Set เป็นปี พ.ศ.
            }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
    var i=1;
    var table;
    $(document).ready(function () {
        unittab();
        //towncode1();
        $('#search input').keydown(function(event) {
            console.log($(this));
        });
        $('#search input').keypress(function(event){
            if(event.which==13){
            $.ajax({
                url: 'cansusRegistration/query',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    id13:  $('#search_id13').val(),
                    name:  $('#search_name').val(),
                    sname: $('#search_sname').val(),
                    limit:1,
                    icheck:1
                },
            })
            .done(function(result) {
                $('#data_id13').empty();
                $('#data_name').empty();
                $('#data_sname').empty();
                $.each(result, function(index, val) {
                    $('#data_id13').append($('<option></option>').val(val.id13).text(val.name+" "+val.sname));
                    $('#data_name').append($('<option></option>').val(val.name).text(val.sname));
                    $('#data_sname').append($('<option></option>').val(val.sname));
                });
                if(result[0]!=null){
                    $('#search_id13').val(result[0].id13);
                    $('#search_name').val(result[0].name);
                    $('#search_sname').val(result[0].sname);
                    $('#address').val(result[0].address);
                    $('#address_mu').val(result[0].address_mu);
                    $('#address_soil').val(result[0].address_soil);
                    $('#address_road').val(result[0].address_road);
                    $('#towncode1').val(result[0].town1);
                    $('#towncode2').val(result[0].town2);
                    $('#towncode3').val(result[0].town3);
                }

            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");

            });
            }

        });


        table=$('#data_people').DataTable( {
            searching:false,
            dom: 'Bfrtip',
            scrollX: true,
            buttons: [
            'copy','excel', 'print'
            ],
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', aData["navyid"]);
            },
            columns: [
            { "data": "id13" ,"width":"200px" ,"class":"text-center"},
            { "data": "name" },
            { "data": "sname" },
            { "data": "yearin" },
            { "data": "id8" },
            { "data": "unit3" },
            { "data": "status" },
            { "data": "report_number" ,"width":"100px" },
            { "data": "book_number" ,"width":"100px" },
            { "data": "rank" }
            ]
        } );
        $('#show_people').on('ifUnchecked', function(event){
            i=1;
        });
        $('#show_people').on('ifChecked',function(event){
            i=0;
        });
        $('#show_people').on('ifChanged', function(event){
            console.log(i);
        });
        $("#form input").keypress(function(event) {
            if(event.which==13){
                query();
            }
        });
    });
    function query(){
        table.rows().remove().draw();
        $.ajax({
            url: 'cansusRegistration/query',
            type: 'GET',
            dataType: 'JSON',
            data: {yearin:$('#form #yearin').val(),
            name:$('#form #name').val(),
            sname:$('#form #sname').val(),
            id13:$('#form #id13').val(),
            book_number:$('#form #book_number').val(),
            rank:$('#form #rank').val(),
            unit3:$('#form #unit3').val(),
            icheck:i}
            ,
        })
        .done(function(result) {
            table.rows(table.rows.add(result).draw()).nodes().to$().attr("id", result['navyid']);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }
    function unittab(){
        $.ajax({
            url: 'cansusRegistration/unittab',
            type: 'GET',
            dataType:'JSON',
        })
        .done(function(result) {
           $('#data_cansusRegistration').empty();
           console.log(result);
           $.each(result, function(index, val) {
            $('#data_cansusRegistration').append($('<option></option>').val(val.unitname));
        });
       })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

    }
    function towncode1(){
        $.ajax({
            url: 'cansusRegistration/towncode1',
            dataType: 'JSON',
        })
        .done(function(result) {
            $.each(result, function(index, val) {
                $('#data_cansusRegistration').append($('<option></option>').val(val.unitname));
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

    }
</script>
