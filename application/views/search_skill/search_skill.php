<div class="content-wrapper">
    <style type="text/css">

        .panel.with-nav-tabs .panel-heading{
            padding: 5px 5px 0 5px;
        }
        .panel.with-nav-tabs .nav-tabs{
            border-bottom: none;
        }
        .panel.with-nav-tabs .nav-justified{
            margin-bottom: -1px;
        }
        /********************************************************************/
        /*** PANEL DEFAULT ***/
        .with-nav-tabs.panel-default .nav-tabs > li > a,
        .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
        }
        .with-nav-tabs.panel-default .nav-tabs > .open > a,
        .with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
            background-color: #ddd;
            border-color: transparent;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.active > a,
        .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
            color: #555;
            background-color: #fff;
            border-color: #ddd;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f5f5f5;
            border-color: #ddd;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #777;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #ddd;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #555;
        }
        /********************************************************************/
        /*** PANEL PRIMARY ***/
        .with-nav-tabs.panel-primary .nav-tabs > li > a,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
        }
        .with-nav-tabs.panel-primary .nav-tabs > .open > a,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
            background-color: #3071a9;
            border-color: transparent;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
            color: #428bca;
            background-color: #fff;
            border-color: #428bca;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #428bca;
            border-color: #3071a9;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #fff;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #3071a9;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            background-color: #4a9fe9;
        }
        /********************************************************************/
        /*** PANEL SUCCESS ***/
        .with-nav-tabs.panel-success .nav-tabs > li > a,
        .with-nav-tabs.panel-success .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li > a:focus {
            color: #3c763d;
        }
        .with-nav-tabs.panel-success .nav-tabs > .open > a,
        .with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-success .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li > a:focus {
            color: #3c763d;
            background-color: #d6e9c6;
            border-color: transparent;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.active > a,
        .with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
            color: #3c763d;
            background-color: #fff;
            border-color: #d6e9c6;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #3c763d;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #d6e9c6;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #3c763d;
        }
        /********************************************************************/
        /*** PANEL INFO ***/
        .with-nav-tabs.panel-info .nav-tabs > li > a,
        .with-nav-tabs.panel-info .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
            color: #31708f;
        }
        .with-nav-tabs.panel-info .nav-tabs > .open > a,
        .with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-info .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
            color: #31708f;
            background-color: #bce8f1;
            border-color: transparent;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.active > a,
        .with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
            color: #31708f;
            background-color: #fff;
            border-color: #bce8f1;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #31708f;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #bce8f1;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #31708f;
        }
        /********************************************************************/
        /*** PANEL WARNING ***/
        .with-nav-tabs.panel-warning .nav-tabs > li > a,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
            color: #8a6d3b;
        }
        .with-nav-tabs.panel-warning .nav-tabs > .open > a,
        .with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
            color: #8a6d3b;
            background-color: #faebcc;
            border-color: transparent;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a,
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
            color: #8a6d3b;
            background-color: #fff;
            border-color: #faebcc;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #fcf8e3;
            border-color: #faebcc;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #8a6d3b;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #faebcc;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #8a6d3b;
        }
        /********************************************************************/
        /*** PANEL DANGER ***/
        .with-nav-tabs.panel-danger .nav-tabs > li > a,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
            color: #a94442;
        }
        .with-nav-tabs.panel-danger .nav-tabs > .open > a,
        .with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
            color: #a94442;
            background-color: #ebccd1;
            border-color: transparent;
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a,
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
            color: #a94442;
            background-color: #fff;
            border-color: #ebccd1;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f2dede; /* bg color */
            border-color: #ebccd1; /* border color */
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #a94442; /* normal text color */
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #ebccd1; /* hover bg color */
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff; /* active text color */
            background-color: #a94442; /* active bg color */
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="col-xs-12"  id="fadetab1"  style="display:none;">
                <div class="box box-info">
                    <div class="box-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab">กรอกอัตราตามกพ.ทร</a></li>
                            <li><a href="#tab2" data-toggle="tab">เลือกลงหน่วย</a></li>
                            <p class="pull-right">ความรู้/ความสามารถ/วุฒิการศึกษา</p>
                        </ul>
                        <!--</div>
                            <div class="box-body">-->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="panel with-nav-tabs skin-blue">
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="tab1">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-2"><b>ผลัด</b></div>
                                                            <div class="col-xs-4">
                                                                <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_yearin" placeholder="_/__"/>
                                                            </div>
                                                            <div class="col-xs-2"><b>หน่วย</b></div>
                                                            <div class="col-xs-4">
                                                                <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_unit3" list="datalist_unittab"/>
                                                                <datalist id="datalist_unittab"></datalist>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-xs-12 form-group" >
                                                        <div class="row">
                                                            <div class="col-xs-2"></div>
                                                            <div class="col-xs-3" style="height: 34px;"><input id="form1" type="radio" name="r1" onclick="radio(1);" value="1" checked> สายสามัญ</div>
                                                            <div class="col-xs-3"></div>
                                                            <div class="col-xs-4"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-2"></div>
                                                            <div class="col-xs-3"><input id="form2" type="radio" name="r1" onclick="radio(2);" value="2" > ความรู้ การศึกษา</div>
                                                            <div class="col-xs-3">
                                                                <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_eductab" list="datalist_eductab"/>
                                                                <datalist id="datalist_eductab"></datalist>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_eductab2" list="datalist_eductab2"/>
                                                                <datalist id="datalist_eductab2"></datalist>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-2"></div>
                                                            <div class="col-xs-3"><input id="form3" type="radio" name="r1" onclick="radio(3);" value="3" > ความสามารถพิเศษ</div>
                                                            <div class="col-xs-3">
                                                                <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_skill" list="datalist_skill"/>
                                                                <datalist id="datalist_skill"></datalist>
                                                            </div>
                                                            <div class="col-xs-4"></div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-xs-2">จำนวน</div>
                                                            <div class="col-xs-3"><input type="number" class="form-control"  id="input_total" value="0"/></div>
                                                            <div class="col-xs-3">
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <button type="button" class="btn btn-success"><i class="fa fa-save"></i> บันทึก</button></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-xs-10"></div>
                                                    <div class="col-xs-2">
                                                        <label class=" pull-right" id="total">รวม 0</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12"style="height: 490px; overflow-x: auto;overflow-y: auto;">
                                                        <table id="table_hisrequest" class="table" >
                                                            <thead>
                                                                <tr>
                                                                    <th  class="text-center">ผลัด</th>
                                                                    <th  class="text-center">หน่วย</th>
                                                                    <th  class="text-center">สามัญ</th>
                                                                    <th  class="text-center">วุฒิ</th>
                                                                    <th  class="text-center">ความสามารถพิเศษ</th>
                                                                    <th  class="text-center">จำนวน</th>
                                                                    <th  class="text-center"></th>


                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab2">
                                              <div class="row">
                                                  <div class="col-xs-12 form-group" >
                                                      <div class="row">
                                                          <div class="col-xs-2"></div>
                                                          <div class="col-xs-3" style="height: 34px;"><input id="formp2_1" type="radio" name="r2" onclick="radio2(1);" value="1" checked> สายสามัญ</div>
                                                          <div class="col-xs-3"></div>
                                                          <div class="col-xs-4"></div>
                                                      </div>
                                                      <div class="row">
                                                          <div class="col-xs-2"></div>
                                                          <div class="col-xs-3"><input id="formp2_2" type="radio" name="r2" onclick="radio2(2);" value="2" > ความรู้ การศึกษา</div>
                                                          <div class="col-xs-3">
                                                              <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_eductab_p2" list="datalist_eductab_p2"/>
                                                              <datalist id="datalist_eductab_p2"></datalist>
                                                          </div>
                                                          <div class="col-xs-4">
                                                              <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_eductab2_p2" list="datalist_eductab2_p2"/>
                                                              <datalist id="datalist_eductab2_p2"></datalist>
                                                          </div>
                                                      </div>
                                                      <div class="row">
                                                          <div class="col-xs-2"></div>
                                                          <div class="col-xs-3"><input id="formp2_3" type="radio" name="r2" onclick="radio2(3);" value="3" > ความสามารถพิเศษ</div>
                                                          <div class="col-xs-3">
                                                              <input class="form-control" onClick="this.setSelectionRange(0, this.value.length)" id="input_skill_p2" list="datalist_skill_p2"/>
                                                              <datalist id="datalist_skill_p2"></datalist>
                                                          </div>
                                                          <div class="col-xs-4"></div>
                                                      </div>
                                                      <br>
                                                      <div class="row">
                                                          <div class="col-xs-2"></div>
                                                          <div class="col-xs-3"></div>
                                                          <div class="col-xs-3"></div>
                                                          <div class="col-xs-4"><button type='button' class="btn btn-success pull-right" onClick="search2();"><i class="fa fa-search"></i> ต้นหา</button></div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <hr>
                                                <div class="row" >
                                                    <div class="col-xs-8" style="overflow-x: auto;">
                                                        <table id="table_view" class="table" >
                                                            <thead>
                                                                <tr>
                                                                    <th nowrap class="text-center">ชื่อ-สกุล</th>
                                                                    <th nowrap class="text-center">หน่วยลง</th>
                                                                    <th nowrap class="text-center">หน่วยคัดเลือก</th>
                                                                    <th nowrap class="text-center">ร้องขอ</th>
                                                                    <th nowrap class="text-center">สมัครใจ1</th>
                                                                    <th nowrap class="text-center">สมัครใจ2</th>
                                                                    <th nowrap class="text-center">วุฒิ</th>
                                                                    <th nowrap class="text-center">ความสามารถพิเศษ</th>
                                                                    <th nowrap class="text-center">คะแนนสอบ</th>
                                                                    <th nowrap class="text-center">สถานภาพ</th>


                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-xs-4">
                                                            <!--<p><b>รายละเอียด</b></p>
                                                            <hr>-->
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ชื่อ</b></div>
                                                            <div class="col-xs-7" id="detail_name_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ผลัดที่ฝึก</b></div>
                                                            <div class="col-xs-7" id="detail_yearin_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ทะเบียน</b></div>
                                                            <div class="col-xs-7" id="detail_id8_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>สังกัด</b></div>
                                                            <div class="col-xs-7" id="detail_belong_to_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>สถานะภาพ</b></div>
                                                            <div class="col-xs-7" id="detail_statustab_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ยาเสพติด</b></div>
                                                            <div class="col-xs-7" id="patient_status_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-5"><b>ศาสนา</b></div>
                                                            <div class="col-md-7" id="detail_religion_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>สูง</b></div>
                                                            <div class="col-xs-7" id="detail_height_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ร้องขอ</b></div>
                                                            <div class="col-xs-7" id="detail_is_request_manual">-</div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-xs-5"><label>เลือกหน่วย</label></div>
                                                            <div class="col-xs-7">
                                                                <input type="text" class="form-control"  onClick="SelectAll('input_unitname4');" id="input_unitname4" list="datalist_unitname"/>
                                                                <datalist id="datalist_unitname"></datalist>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-5"><b>หน่วยคัดเลือก</b></div>
                                                            <div class="col-xs-7" id="detail_uint4_manual">-</div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>วุฒิการศึกษา</b></div>
                                                            <div class="col-xs-7" id="detail_eductab_manual">-</div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ความรู้พิเศษ</b></div>
                                                            <div class="col-xs-7" id="detail_skill_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>คะแนนสอบ</b></div>
                                                            <div class="col-xs-7" id="detail_percent_manual">-</div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-5"><b>หน่วยสมัครใจ1</b></div>
                                                            <div class="col-xs-7"id="detail_uint1_manual">-</div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-xs-5"><b>หน่วยสมัครใจ2</b></div>
                                                            <div class="col-xs-7"id="detail_uint2_manual">-</div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="panelRequest3">
                                                <div class="row">
                                                    <div class="col-xs-5"><label>เลือกหน่วย</label></div>
                                                    <div class="col-xs-7">
                                                        <input type="text" class="form-control"  onClick="SelectAll('input_unitname4');" id="input_unitname4" list="datalist_unitname"/>
                                                        <datalist id="datalist_unitname"></datalist>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab4default">Default 4</div>
                                            <div class="tab-pane fade" id="tab5default">Default 5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4"  id="fadetab2"  style="display:none;">
                <div class="box box-info">
                    <div class="box-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab2default" data-toggle="tab">ยอดคัดเลือก</a></li>
                            <li><a href="#tab1default" data-toggle="tab">ยอดรวม</a></li>
                            <li><a href="#tab3default" data-toggle="tab">ตั้งค่า</a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel with-nav-tabs skin-blue">
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade" id="tab1default">
                                            <div class="row" id="countall">
                                            </div>
                                        </div>
                                        <div class="tab-pane fade in active" id="tab2default">
                                            <div class="row" id="count">
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab3default">
                                            <button type="button" onclick="saveamount();" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                                            <div class="form-group" id="amount">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/>
    </section>
</div>
