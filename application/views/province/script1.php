<script type="text/javascript">
    var navyid;
    var input;
    var armid='';
    var table=false;
    $body = $("body");
    $(document).on({

      ajaxStart: function() {
        if(table==false){
          $body.addClass("loading");
        }
        },
      ajaxStop: function() { $body.removeClass("loading"); }
    });
    $(document).ready(function () {
        armtown(4);
        datalistunittab();
        countall();
        unittabseting();
        $('#sounth_select').on('change', function () {
            armid = $(this).find(":selected").val();
            person_armtown(armid);
        });
        $('#table_sounth_select tbody').on('click', 'tr', function () {
          table=true;
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('#table_sounth_select tbody tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            navyid = $(this).attr('navyid');
            console.log(navyid);
            detail_manual();
        });
        $('#input_unittab3').on('keypress', function (event) {
          table=false;
          if(event.which==13){
            var str = $('#input_unittab3').val();
            str = str.split(' ');
            if(str[0]!=null){
                update_unit3(str[0]);
                person_armtown(armid);
            }
            }
        });

            $('#fadetab1').fadeIn(500);
            $('#fadetab2').fadeIn(500);
    });
    function update_unit3(input) {
        $.ajax({
            url: "selectexam/update_unit3",
            data: "navyid=" + navyid + "&unit3=" + input,
            timeout: 3000, // 3 second
            type: 'GET',
            async: false,
            cache: false,
            success: function () {
                countall();
            },
            error: function (err) {
                alert(err);
            }
        });
    }
    function datalistunittab() {
        $.ajax({
            url: 'province/unittab',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("Query datalist unittab >>> success");
                    console.log(result);
                    $('#datalist_unittab').empty();
                    $.each(result['query'], function (index, item) {
                        $('#datalist_unittab')
                                .append($("<option></option>")
                                        .attr("value", item.refnum + ' ' + item.unitname));
                    });

                })
                .fail(function () {
                    console.log("Query datalist unittab >>> error");
                })
                .always(function () {
                    console.log("Query datalist unittab >>> complete");
                });
    }
    function detail_manual() {   ///////////////////////////////////รายละเอียดข้อมูล

        $("#detail_name_manual").empty();
        $("#detail_sname_manual").empty();
        $("#detail_height_manual").empty();
        $("#patient_status_manual").css("background-color", "").empty();
        $("#detail_id8_manual").empty();
        $("#detail_statustab_manual").css("background-color", "").empty();
        $("#detail_yearin_manual").empty();
        $("#detail_religion_manual").empty();
        $("#detail_belong_to_manual").empty();
        $("#detail_eductab_manual").empty();
        $("#detail_is_request_manual").css("background-color", "").empty();
        $("#detail_percent_manual").empty();
        $("#detail_uint3_manual").empty();
        $("#detail_uint4_manual").empty();
        $("#detail_uint1_manual").empty();
        $("#detail_uint2_manual").empty();
        $("#detail_skill_manual").empty();
        $('#input_unitname4').val('');
        $.ajax({
            url: "province/query_detail",
            data: "navyid=" + navyid,
            timeout: 3000, // 3 second
            type: 'GET',
            dataType: 'JSON',
            cache: false,
            success: function (result) {
                obj = result[0];
                yearin = obj.yearin;
                id8 = obj.id8;
                name = obj.name;
                sname = obj.sname;
                cunit3 = obj.refnum3;
                $("#detail_name_manual").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
                $("#detail_height_manual").append(obj.height);
                if (obj.patient_status != null) {
                    if (obj.patient_status != "")
                        $('#patient_status_manual').append(obj.patient_status).css("background-color", "#ff4000    ");
                }
                $('#detail_religion_manual').append(obj.religion);
                $("#detail_id8_manual").append(obj.id8);
                if (obj.statuscode != "AA") {
                    $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")").css("background-color", "yellow");
                } else {
                    $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")");
                }
                if (obj.oldyerin != null) {
                    $("#detail_yearin_manual").append(obj.yearin + "(" + obj.oldyerin + ")");
                } else {
                    $("#detail_yearin_manual").append(obj.yearin);
                }
                if (obj.pseq < 10) {
                    $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + "0" + obj.pseq + ")");
                } else {
                    $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + obj.pseq + ")");
                }
                $("#detail_eductab_manual").append(obj.educname);
                if (obj.is_request != "000") {
                    $("#detail_is_request_manual").append("ร้องขอ").css("background-color", "#40ff00");
                }
                $("#detail_percent_manual").append(obj.percent);
                $("#detail_uint3_manual").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
                if ((obj.postcode == null) && (obj.item == null))
                    $("#detail_uint4_manual").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
                else
                    $("#detail_uint4_manual").append(obj.refnum4 + " <b>หน่วย</b> " + obj.postname + " <b>ลำดับ</b> " + obj.item);
                $("#detail_uint1_manual").append(obj.refnum1);
                $("#detail_uint2_manual").append(obj.refnum2);
                $("#detail_skill_manual").append(obj.skill);
                $('#input_unittab3').val(obj.unit3 + ' ' + obj.refnum3);
            },
            error: function (err) {
                alert(err);
            }
        });

    }
    function armtown(i) {
        $.ajax({
            url: 'province/armtown',
            type: 'GET',
            dataType: 'JSON',
            data: {legion: i.toString()},
        })
                .done(function (result) {
                    console.log("success");
                    console.log(result);
                    $('#sounth_select').empty();
                    $('#sounth_select').append($("<option selected></option>")
                            .attr("value", "")
                            .text("ไม่ระบุ"));
                    $.each(result['query'], function (index, val) {
                        console.log(val);
                        $('#sounth_select').append($("<option></option>")
                                .attr("value", val.ARMID)
                                .text(val.ARMNAME));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                    person_armtown('');
                });

    }
    function person_armtown(i) {
        $.ajax({
            url: 'province/person_armtown',
            type: 'GET',
            dataType: 'JSON',
            data: {armid: i.toString()},
        })
                .done(function (result) {
                    console.log("success");
                    console.log(result);
                    txt = '';
                     $('#table_sounth_select tbody').empty();
                    $.each(result['query'], function (index, val) {
                        txt += '<tr navyid='+val.navyid+'>\n\
                            <td>'+val.name + ' '+val.sname+'</td>\n\
                            <td>'+val.u3+'</td>\n\
                            <td>'+val.u1+'</td>\n\
                            <td>'+val.u2+'</td>\n\
                            <td>'+val.percent+'</td>\n\
                            <td>'+val.title+'</td>\n\
                            <td>'+val.id8+'</td>\n\
                            </tr>';
                    });
                    $('#table_sounth_select tbody').append(txt);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
     function saveamount() {
        data = "";
        for (var i = 1; i <= 30; i++) {
            data += '&' + i + "=" + $('#input' + i).val();
        }
        $.ajax({
            url: 'voluteer/saveamount?' + data,
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function () {
                    console.log("success");
                    countvoluteer();
                    countall();
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function countvoluteer() {
        $.ajax({
            url: 'voluteer/unittab',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("Query unittab >>> success");
                    console.log(result);
                    $('#count').empty();
                    var txt = '<div class="col-xs-3" id="label"><b>หน่วย</b></div>'
                            + '<div class="col-xs-3" id="labelall"><b>ต้องการ</b></div>'
                            + '<div class="col-xs-3" id="labelunit1"><b>สมัครใจ1</b></div>'
                            + '<div class="col-xs-3" id="labelunit2"><b>สมัครใจ2</b></div>';
                    $.each(result['query'], function (index, val) {
                        txt += '<div class="col-xs-4" id="label' + val.refnum + '"><b>' + val.unitname + '</b></div>'
                                + '<div class="col-xs-3" id="count_all' + val.refnum + '">0</div>'
                                + '<div class="col-xs-2" id="count_unit1' + val.refnum + '">0</div>'
                                + '<div class="col-xs-3" id="count_unit2' + val.refnum + '">0</div>';
                    });
                    $('#count').append(txt);
                })
                .fail(function () {
                    console.log("Query unittab >>> error");
                })
                .always(function () {
                    console.log("Query unittab >>> complete");


                    $.ajax({
                        url: 'voluteer/countall',
                        type: 'GET',
                        dataType: 'JSON',
                    })
                            .done(function (result) {
                                console.log("Query countall >>> success");
                                console.log(result);
                                $.each(result['query'], function (index, val) {
                                    $('#count_all' + val.unit + '').empty();
                                    $('#count_all' + val.unit + '').append(val.count);
                                });
                            })
                            .fail(function () {
                                console.log("Query countall >>> error");
                            })
                            .always(function () {
                                console.log("Query countall >>> complete");

                                $.ajax({
                                    url: 'voluteer/countunit1',
                                    type: 'GET',
                                    dataType: 'JSON',
                                })
                                        .done(function (result) {
                                            console.log("Query countunit1 >>> success");
                                            console.log(result);
                                            $.each(result['query'], function (index, val) {
                                                $('#count_unit1' + val.unit + '').empty();
                                                $('#count_unit1' + val.unit + '').append(val.count);
                                            });
                                        })
                                        .fail(function () {
                                            console.log("Query countunit1 >>> error");
                                        })
                                        .always(function () {
                                            console.log("Query countunit1 >>> complete");

                                            $.ajax({
                                                url: 'voluteer/countunit2',
                                                type: 'GET',
                                                dataType: 'JSON',
                                            })
                                                    .done(function (result) {
                                                        console.log("Query countunit2 >>> success");
                                                        console.log(result);
                                                        $.each(result['query'], function (index, val) {
                                                            $('#count_unit2' + val.unit + '').empty();
                                                            $('#count_unit2' + val.unit + '').append(val.count);
                                                        });
                                                    })
                                                    .fail(function () {
                                                        console.log("Query countunit2 >>> error");
                                                    })
                                                    .always(function () {
                                                        console.log("Query countunit2 >>> complete");
                                                    });
                                        });
                            });
                });

    }
    function countall() {
        $.ajax({
            url: 'voluteer/unittab',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("Query countall unittab >>> success");
                    console.log(result);
                    $('#countall').empty();
                    var txt = '<div class="col-xs-5" id="label"><b>หน่วย</b></div>'
                            + '<div class="col-xs-3" id="labelall"><b>ยอดต้องการ</b></div>'
                            + '<div class="col-xs-3" id="labelunit1"><b>ยอดปัจจุบัน</b></div>'
                    $.each(result['query'], function (index, val) {
                        txt += '<div class="col-xs-5" id="label' + val.refnum + '"><b>' + val.unitname + '</b></div>'
                                + '<div class="col-xs-3" id="countall_all' + val.refnum + '">0</div>'
                                + '<div class="col-xs-2" id="countall_unit3' + val.refnum + '">0</div>'
                    });
                    $('#countall').append(txt);
                })
                .fail(function () {
                    console.log("Query countall unittab >>> error");
                })
                .always(function () {
                    console.log("Query countall unittab >>> complete");


                    $.ajax({
                        url: 'voluteer/countall',
                        type: 'GET',
                        dataType: 'JSON',
                    })
                            .done(function (result) {
                                console.log("Query countall countall >>> success");
                                console.log(result);
                                $.each(result['query'], function (index, val) {
                                    $('#countall_all' + val.unit + '').empty();
                                    $('#countall_all' + val.unit + '').append(val.count);
                                });
                            })
                            .fail(function () {
                                console.log("Query countall countall >>> error");
                            })
                            .always(function () {
                                console.log("Query countall countall >>> complete");

                                $.ajax({
                                    url: 'voluteer/countunit3',
                                    type: 'GET',
                                    dataType: 'JSON',
                                })
                                        .done(function (result) {
                                            console.log("Query countall countunit3 >>> success");
                                            console.log(result);
                                            $.each(result['query'], function (index, val) {
                                                $('#countall_unit3' + val.unit3 + '').empty();
                                                $('#countall_unit3' + val.unit3 + '').append(val.count);
                                            });
                                        })
                                        .fail(function () {
                                            console.log("Query countall countunit3 >>> error");
                                        })
                                        .always(function () {
                                            console.log("Query countall countunit3 >>> complete");
                                        });
                            });
                });

    }
    function unittabseting() {
        $.ajax({
            url: 'voluteer/amount',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (e) {
                    console.log(e.length);
                    $('#amount').empty();
                    txt = '';
                    $.each(e, function (index, item) {
                        if (item.refnum > 0) {
                            txt += '<div class="form-group">'
                                    + '<div class="col-xs-6">'
                                    + '<label for="input' + item.refnum + '" >' + item.unitname + '</label>'
                                    + '</div>'
                                    + '<div class="col-xs-6">'
                                    + '<input type="number" class="form-control" onchange="saveamount();" onClick="SelectAll(input' + item.refnum + ');"'
                                    + 'id="input' + item.refnum + '" value="' + item.count + '"/>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';
                        }
                    });
                    $('#amount').append(txt);

                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
</script>
