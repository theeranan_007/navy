<script type="text/javascript">
    var navyid = '';

    $(document).ready(function () {
        unittab();
        viewtable();
        $('#search_person').on('input', function () {
            var fullname = $('#search_person').val();
            var name, sname, id8;
            if (fullname != "") {
                fullname = fullname.split(" ");
                if (fullname[0] != null)
                    name = fullname[0];
                else
                    name = "";
                if (fullname[1] != null)
                    sname = fullname[1];
                else
                    sname = "";
                if (fullname[2] != null)
                    id8 = fullname[2];
                else
                    id8 = "";
                if (fullname[5] != null) {
                    navyid = fullname[5];
                    $.ajax({
                        url: 'fromInsert/detail',
                        type: 'GET',
                        dataType: 'JSON',
                        data: {navyid: navyid},
                    }).done(function (e) {
                        console.log(e.length);
                        $('#datalist_person').empty();
                        $.each(e, function (index, item) {
                            $('#input_yearin').val(item.yearin);
                            $('#input_name').val(item.name);
                            $('#input_sname').val(item.sname);
                            $('#input_id8').val(item.id8);
                            $('#input_address').val(item.address);
                            $('#input_unit3').val(item.unit3 + ' ' + item.unitname);
                        });
                    })
                            .fail(function () {
                                console.log("error");
                            })
                            .always(function () {
                                console.log("complete");
                            });
                } else
                    navyid = "";

            }
            $.ajax({
                url: 'fromInsert/person',
                type: 'GET',
                dataType: 'JSON',
                data: {name: name,
                    sname: sname,
                    id8: id8},
            })
                    .done(function (e) {
                        console.log(e.length);
                        $('#datalist_person').empty();
                        $.each(e, function (index, item) {
                            $('#datalist_person')
                                    .append($("<option>")
                                            .attr("value", item.name));
                        });
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function () {
                        console.log("complete");
                    });

        });
        $('#input_id8').on("keyup", function (event) {
            var selection = window.getSelection().toString();
            if (selection !== "") {
                return;
            }
            if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
                return;
            }
            var $this = $(this);
            var input = $this.val();
            input = input.replace(/[^เธ-เธฎ0-9]+/g, "");


            var chunk = [];
            var split = 0;
            for (var i = 0, len = input.length; i < len; i += split) {
                if (i == 0 || i == 1) {
                    split = 1;
                } else {
                    split = 4;
                }

                if (split == 4 && i == 6) {
                    break;
                }
                chunk.push(input.substr(i, split));
            }
            $this.val(function () {
                return chunk.join(".");
            });
        });
        $("#input_yearin").on("keyup", function (event) {
            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
            if (selection !== "") {
                return;
            }

            // When the arrow keys are pressed, abort.
            if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
                return;
            }

            var $this = $(this);
            var input = $this.val();
            input = input.replace(/[^0-9]+/g, "");

            var chunk = [];

            if (input.length >= 3) {
                var one = input.substr(0, 1);
                var two = input.substr(1, input.length);
                chunk.push(one);
                chunk.push(two);
            } else {
                chunk.push(input);
            }

            $this.val(function () {
                return chunk.join("/");
            });
            viewtable();
        });

    });
    function unittab() {
        $.ajax({
            url: 'voluteer/unittab',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_unit3').empty();
                    $.each(result['query'], function (index, item) {

                        $('#datalist_unit3')
                                .append($("<option>")
                                        .attr("value", item.refnum + ' ' + item.unitname));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
        $('#input_unit3').keypress(function (event) {
            if (event.which == 13) {
                save();
            }
        });

    }
    function save() {
        var cutstring = $('#input_unit3').val();
        cutstring = cutstring.split(' ');
        if (cutstring[0] != null) {
            $.ajax({
                url: 'fromInsert/save',
                type: 'GET',
                data: 'navyid=' + navyid + '&yearin="' + $('#input_yearin').val() + '"&name="' + $('#input_name').val() + '"&sname="' + $('#input_sname').val() + '"&id8="' + $('#input_id8').val() + '"&address="' + $('#input_address').val() + '"&unit3="' + cutstring[0] + '"',
            })
                    .done(function () {
                        console.log("success");
                        alert('บันทึกสำเร็จ');
                    })
                    .fail(function (xhr, status, error) {
                        console.log('error');
                        console.log(xhr);
                        console.log(status);
                        console.log(error);
                    })
                    .always(function () {
                        console.log("complete");
                        //$('#input_name').val('');
                        $('#input_sname').val('');
                        $('#input_id8').val('');
                        $('#input_address').val('');
                        $('#input_unit3').val('');
                        $('#search_person').val('');
                        $('#input_name').focus();
                        navyid = '';
                        viewtable();

                    });
        } else {
            alert('error');
        }

    }
    function viewtable() {
        $.ajax({
            url: 'fromInsert/viewtable',
            type: 'GET',
            dataType: 'JSON',
            data: {yearin: $('#input_yearin').val()},
            success: function (e) {
                console.log(e);
                $('#table_person tbody').empty();
                $('#total').empty();
                var i = 0;
                var txthtml = "";
                $.each(e, function (index, val) {
                    /* iterate through array or object */
                    i++;
                    txthtml += "<tr>";

                    txthtml += "<td>" + i + "</td>";
                    txthtml += "<td>" + val.name + " " + val.sname + "</td>";
                    txthtml += "<td>" + val.yearin + "</td>";
                    txthtml += "<td>" + val.id8 + "</td>";
                    txthtml += "<td>" + val.address + "</td>";
                    txthtml += "<td>" + val.unitname + "</td>";
                    txthtml += "</tr>";

                });
                if (i < 5) {
                    for (var c = i; c < 5; c++) {
                        txthtml += "<tr>";
                        txthtml += "<td>-</td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "</tr>";
                    }
                }
                $('#table_person tbody').append(txthtml);
                $('#total').append('รวม '+i);
            }
        })
                .done(function () {
                    console.log("success");
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
</script>

