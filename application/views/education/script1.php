<script type="text/javascript">
    var navyid;
    var count = 0;
    var refnum;
    var row;
    var next;
    var prev;
    var c = 0;
    var limit = 10, rowtable = 0;
    var all;
    var input;
    var num = "";
    var unit4;
    var postcode;
    var yearin;
    var d;
    var general = 'T', seq = '';
    var table=false;
    var table2;
    $body = $("body");
    $(document).on({

      ajaxStart: function() {
        if(table==false){
          $body.addClass("loading");
        }
        },
      ajaxStop: function() { $body.removeClass("loading"); }
    });
    $(document).ready(function () {
        radio(1);
        radio2(1)
        unittab();
        count1();
        countall();
        unittabseting();
        eductab();
        eductab_p2();
        table2=$('#table_view').DataTable( {
            searching:false,
            dom: 'Bfrtip',
            scrollX: true,
            buttons: [
            'copy','excel', 'print'
            ],
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('navyid', aData["navyid"]);
            },
            columns: [
            { "data": "name" ,},
            { "data": "unit3" },
            { "data": "unit4"},
            { "data": "unit" },
            { "data": "unit1" },
            { "data": "unit2" },
            { "data": "educname" },
            { "data": "skill"  },
            { "data": "percent" },
            { "data": "title" }
            ]
        } );
        $('#input_unit3').on('input', function () {
            if ($('#input_unit3').val().split(' ')[0] != '') {
                viewtable($('#input_unit3').val().split(' ')[0]);
            }
        });
        $('#input_total').keypress(function (event) {
            if (event.which == 13) {
                save();
            }
        });
        $('#input_yearin').on('input', function () {
            if ($('#input_yearin').val() != '') {
                viewtable(0);
            }
        });
        $('').keypress(function(event){

        });
        $('#input_unitname4').on('keypress', function (event) {
          table=false;
          if(event.which==13){
            if ($('#input_unitname4').val().split(' ')[0] != '') {
                $.ajax({
                    url: 'education/update_unit3',
                    type: 'GET',
                    data:{navyid: navyid,
                            unit3:$('#input_unitname4').val().split(' ')[0]},
                })
                        .done(function () {
                           console.log("Update unit3 success");
                   eductab2_p2($('#input_eductab_p2').val().split(' ')[0]);
                        })
                        .fail(function (str,e,d) {
                            console.log("error "+str+" "+e+" "+d);
                        })
                        .always(function () {
                            console.log("complete");
                        });
            }
          }
        });
        $('#input_eductab').on('input', function () {
            console.log('input_eductab');
            eductab($('#input_eductab').val().split(' ')[0]);
        });
        $('#input_eductab_p2').on('input', function () {
            console.log('input_eductab2');
            eductab2($('#input_eductab2').val().split(' ')[0]);
        });
        $('#input_eductab2').on('input', function () {
            console.log('input_eductab_p2');
            eductab_p2($('#input_eductab_p2').val().split(' ')[0]);
        });
        $('#input_eductab2_p2').on('input', function () {
            console.log('input_eductab2_p2');
            eductab2_p2($('#input_eductab2_p2').val().split(' ')[0]);
        });

        $('#input_unitname2').on('input', function () {
            var refnum = $('#input_unitname2').val();
            refnum = refnum.split(" ");
            if (refnum[0] != null) {
                d = refnum[0];
            } else {
                d = "";
            }
            table_tab2();
        });

        $('#table_view tbody').on('click', 'tr', function () {
          table=true;
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('#table_view tbody tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            navyid = $(this).attr('navyid');
            console.log(navyid);
            detail_manual();
        });

                    $('#fadetab1').fadeIn(500);
                    $('#fadetab2').fadeIn(500);
    });


    function viewtable(j) {
        $.ajax({
            url: 'education/viewtable_education',
            type: 'GET',
            dataType: 'JSON',
            data: {yearin: $('#input_yearin').val(),
                unit3: j},
            success: function (e) {
                console.log(e);
                $('#table_hisrequest tbody').empty();
                var i = 0;
                var txthtml = "";
                $.each(e, function (index, val) {
                    /* iterate through array or object */
                    i++;
                    txthtml += "<tr>";
                    txthtml += "<td>" + val.YEARIN + "</td>";
                    if (val.unitname == null) {
                        val.unitname = '';
                    }
                    txthtml += "<td>" + val.unitname + "</td>";
                    if (val.GENERAL == null) {
                        val.GENERAL = '';
                    }
                    txthtml += "<td>" + val.GENERAL + "</td>";
                    if (val.educname == null) {
                        val.educname = '';
                    }
                    txthtml += "<td>" + val.educname + "</td>";
                    if (val.skill == null) {
                        val.skill = '';
                    }
                    txthtml += "<td>" + val.skill + "</td>";
                    txthtml += "<td>" + val.TOTAL + "</td>";
                    txthtml += "<td>";
                    txthtml += '<div class="pull-right" ><button type="button" onclick="edit(' + val.SEQ + ');" id="education_edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></button>';
                    txthtml += '<button type="button" id="education_remove" onclick="remove(' + val.SEQ + ');" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></td></div>';
                    txthtml += "</tr>";
                });
                if (i < 5) {
                    for (var c = i; c < 5; c++) {
                        txthtml += "<tr>";
                        txthtml += "<td>-</td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "<td></td>";
                        txthtml += "</tr>";
                    }
                }
                $('#table_hisrequest tbody').append(txthtml);
            }
        })
                .done(function () {
                    console.log("success ");
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
        if (j != 0) {
            $.ajax({
                url: 'education/count_hisrequire',
                type: 'GET',
                dataType: 'JSON',
                data: {unit: j,
                    yearin: $('#input_yearin').val()},
            })
                    .done(function (result) {
                        console.log("success");
                        $('#total').empty();
                        $.each(result['query'], function (index, val) {
                            $('#total').append('รวม ' + val.count);
                        });
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function () {
                        console.log("complete");
                    });
        }

    }
    function unittab() {
        $.ajax({
            url: 'education/unittab_education',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (e) {
                    console.log(e.length);
                    $('#datalist_unittab').empty();
                    $.each(e, function (index, item) {
                        $('#datalist_unittab')
                                .append($("<option></option>")
                                        .attr("value", item.name));
                        $('#datalist_unitname')
                                .append($("<option></option>")
                                        .attr("value", item.name));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function count1() {
        $.ajax({
            url: 'education/count',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#count').empty();
                    var txt = '<div class="col-xs-4" id="label"><b>หน่วย</b></div>'
                            + '<div class="col-xs-2" id="count_all"><b>ต้องการ</b></div>'
                            + '<div class="col-xs-3"id="count_requestall"><b>คัดเลือก</b></div>'
                            + '<div class="col-xs-3" ><b>ได้ตามคัดเลือก</b></div>';
                    $.each(result, function (index, val) {
                        if (val.unit != 'ไม่ได้ระบุ') {
                            if (parseInt(val.count2) == parseInt(val.all) && parseInt(val.count) <= parseInt(val.count2)) {
                                console.log(val.unit + " " + val.count + " = " + val.all);
                                txt += '<div class="col-xs-4" style="background-color:#73e600; id="label"><b>' + val.unit + '</b></div>'

                                        + '<div class="col-xs-2" style="background-color:#73e600;" id="count_all">' + val.all + '</div>'
                                        + '<div class="col-xs-3" style="background-color:#73e600; id="count_requestall">' + val.count + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #73e600;" id="count_all">' + val.count2 + '</div>';
                            } else if (parseInt(val.count2) > parseInt(val.all))
                            {
                                console.log(val.unit + " " + val.count + " > " + val.all);
                                txt += '<div class="col-xs-4" style="background-color: #f45f42; id="label"><b>' + val.unit + '</b></div>'

                                        + '<div class="col-xs-2" style="background-color: #f45f42;" id="count_all">' + val.all + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #f45f42; id="count_requestall">' + val.count + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #f45f42;" id="count_all">' + val.count2 + '</div>';
                            } else if (parseInt(val.count) > parseInt(val.count2))
                            {
                                console.log(val.unit + " " + val.count + " > " + val.all);
                                txt += '<div class="col-xs-4" style="background-color: #ff6600; id="label"><b>' + val.unit + '</b></div>'

                                        + '<div class="col-xs-2" style="background-color: #ff6600;" id="count_all">' + val.all + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #ff6600; id="count_requestall">' + val.count + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #ff6600;" id="count_all">' + val.count2 + '</div>';
                            } else
                            {
                                console.log(val.unit + " " + val.count + " < " + val.all);
                                txt += '<div class="col-xs-4" style="background-color: #ffffff; id="label"><b>' + val.unit + '</b></div>'

                                        + '<div class="col-xs-2" style="background-color: #ffffff;" id="count_all">' + val.all + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #ffffff; id="count_requestall">' + val.count + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #ffffff;" id="count_all">' + val.count2 + '</div>';
                            }
                        }
                    });
                    $('#count').append(txt);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function countall() {
        $.ajax({
            url: 'education/countall',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#countall').empty();
                    var txt = '<div class="col-xs-5" id="label"><b>หน่วย</b></div>'

                            + '<div class="col-xs-3" id="count_all"><b>ยอดต้องการ</b></div>'
                            + '<div class="col-xs-3"id="count_requestall"><b>ยอดปัจจุบัน</b></div>';
                    $.each(result, function (index, val) {
                        if (val.unit != 'ไม่ได้ระบุ') {
                            if (parseInt(val.count) == parseInt(val.all)) {
                                console.log(val.unit + " " + val.count + " = " + val.all);
                                txt += '<div class="col-xs-5" style="background-color:#73e600; id="label"><b>' + val.unit + '</b></div>'

                                        + '<div class="col-xs-3" style="background-color:#73e600;" id="count_all">' + val.all + '</div>'
                                        + '<div class="col-xs-3" style="background-color:#73e600; id="count_requestall">' + val.count + '</div>';
                            } else if (parseInt(val.count) > parseInt(val.all))
                            {
                                console.log(val.unit + " " + val.count + " > " + val.all);
                                txt += '<div class="col-xs-5" style="background-color: #f45f42; id="label"><b>' + val.unit + '</b></div>'

                                        + '<div class="col-xs-3" style="background-color: #f45f42;" id="count_all">' + val.all + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #f45f42; id="count_requestall">' + val.count + '</div>';
                            } else
                            {
                                console.log(val.unit + " " + val.count + " < " + val.all);
                                txt += '<div class="col-xs-5" style="background-color: #ffffff; id="label"><b>' + val.unit + '</b></div>'

                                        + '<div class="col-xs-3" style="background-color: #ffffff;" id="count_all">' + val.all + '</div>'
                                        + '<div class="col-xs-3" style="background-color: #ffffff; id="count_requestall">' + val.count + '</div>';
                            }
                        }
                    });
                    $('#countall').append(txt);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function unittabseting() {
        $.ajax({
            url: 'education/amount',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (e) {
                    console.log(e.length);
                    $('#amount').empty();
                    txt = '';
                    $.each(e, function (index, item) {
                        if (item.refnum > 0) {
                            txt += '<div class="form-group">'
                                    + '<div class="col-xs-6">'
                                    + '<label for="input' + item.refnum + '" >' + item.unitname + '</label>'
                                    + '</div>'
                                    + '<div class="col-xs-6">'
                                    + '<input type="number" class="form-control" onchange="saveamount();" onClick="SelectAll(input' + item.refnum + ');"'
                                    + 'id="input' + item.refnum + '" value="' + item.count + '"/>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';
                        }
                    });
                    $('#amount').append(txt);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function saveamount() {
        data = "";
        for (var i = 1; i <= 30; i++) {
            data += '&' + i + "=" + $('#input' + i).val();
        }
        $.ajax({
            url: 'request/saveamount?' + data,
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function () {
                    console.log("success");
                    count1();
                    countall();
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function detail_manual() {   ///////////////////////////////////รายละเอียดข้อมูล

        $("#detail_name_manual").empty();
        $("#detail_sname_manual").empty();
        $("#detail_height_manual").empty();
        $("#patient_status_manual").css("background-color", "").empty();
        $("#detail_id8_manual").empty();
        $("#detail_statustab_manual").css("background-color", "").empty();
        $("#detail_yearin_manual").empty();
        $("#detail_religion_manual").empty();
        $("#detail_belong_to_manual").empty();
        $("#detail_eductab_manual").empty();
        $("#detail_is_request_manual").css("background-color", "").empty();
        $("#detail_percent_manual").empty();
        $("#detail_uint3_manual").empty();
        $("#detail_uint4_manual").empty();
        $("#detail_uint1_manual").empty();
        $("#detail_uint2_manual").empty();
        $("#detail_skill_manual").empty();
        $('#input_unitname4').val('');
        $.ajax({
            url: "request/query_detail",
            data: "navyid=" + navyid,
            type: 'GET',
            dataType: 'JSON',
            cache: false,
            success: function (result) {
                obj = result[0];
                yearin = obj.yearin;
                id8 = obj.id8;
                name = obj.name;
                sname = obj.sname;
                cunit3 = obj.refnum3;
                $("#detail_name_manual").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
                $("#detail_height_manual").append(obj.height);
                if (obj.patient_status != null) {
                    if (obj.patient_status != "")
                        $('#patient_status_manual').append(obj.patient_status).css("background-color", "#ff4000    ");
                }
                $('#detail_religion_manual').append(obj.religion);
                $("#detail_id8_manual").append(obj.id8);
                if (obj.statuscode != "AA") {
                    $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")").css("background-color", "yellow");
                } else {
                    $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")");
                }
                if (obj.oldyerin != null) {
                    $("#detail_yearin_manual").append(obj.yearin + "(" + obj.oldyerin + ")");
                } else {
                    $("#detail_yearin_manual").append(obj.yearin);
                }
                if (obj.pseq < 10) {
                    $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + "0" + obj.pseq + ")");
                } else {
                    $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + obj.pseq + ")");
                }
                $("#detail_eductab_manual").append(obj.educname);
                if (obj.is_request != "000") {
                    $("#detail_is_request_manual").append("ร้องขอ").css("background-color", "#40ff00");
                }
                $("#detail_percent_manual").append(obj.percent);
                $("#detail_uint3_manual").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
                if ((obj.postcode == null) && (obj.item == null))
                    $("#detail_uint4_manual").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
                else
                    $("#detail_uint4_manual").append(obj.refnum4 + " <b>หน่วย</b> " + obj.postname + " <b>ลำดับ</b> " + obj.item);
                $("#detail_uint1_manual").append(obj.refnum1);
                $("#detail_uint2_manual").append(obj.refnum2);
                $("#detail_skill_manual").append(obj.skill);
                $('#input_unitname4').val(obj.unit3 + ' ' + obj.refnum3);
            },
            error: function (err) {
                alert(err);
            }
        });
    }
    function eductab() {
        $.ajax({
            url: 'education/eductab',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_eductab').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_eductab').append($('<option>').val(val.ECODE1 + ' ' + val.EDUCNAME));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

    }
    function eductab2(i) {
        $.ajax({
            url: 'education/eductab2',
            type: 'GET',
            dataType: 'JSON',
            data: 'ecode1=' + i,
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_eductab2').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_eductab2').append($('<option></option>').val(val.ECODE2 + ' ' + val.EDUCNAME));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

    }
    function eductab_p2() {
        $.ajax({
            url: 'education/eductab_p2',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_eductab_p2').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_eductab_p2').append($('<option></option>').val(val.ECODE1 + ' ' + val.EDUCNAME));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

    }
    function eductab2_p2(i) {
        $.ajax({
            url: 'education/eductab2_p2',
            type: 'GET',
            dataType: 'JSON',
            data: 'ecode1=' + i,
        })
                .done(function (result) {
                    console.log(result['query']);
                    console.log("success eductab2_p2");
                    $('#datalist_eductab2_p2').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_eductab2_p2').append($('<option></option>').val(val.ecode2 + ' ' + val.educname));
                    });
                })
                .fail(function () {
                    console.log("error eductab2_p2");
                })
                .always(function () {
                    console.log("complete eductab2_p2");
                });

    }
    function skilltab() {
        $.ajax({
            url: 'education/skilltab',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_skill').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_skill').append($('<option></option>').val(val.SKILLCODE + ' ' + val.SKILL));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

    }
    function skilltab2() {
        $.ajax({
            url: 'education/skilltab2',
            type: 'GET',
            dataType: 'JSON',
        })
                .done(function (result) {
                    console.log("success");
                    $('#datalist_skill_p2').empty();
                    $.each(result['query'], function (index, val) {
                        $('#datalist_skill_p2').append($('<option></option>').val(val.skillcode + ' ' + val.skill));
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

    }
    function radio(i) {
        if (i == 1) {
            $('#input_eductab').attr('disabled', true);
            $('#input_eductab2').attr('disabled', true);
            $('#input_skill').attr('disabled', true);
            $('#input_eductab').val('');
            $('#input_eductab2').val('');
            $('#input_skill').val('');
            general = 'T';
        }
        if (i == 2) {
            $('#input_eductab').attr('disabled', false);
            $('#input_eductab2').attr('disabled', false);
            $('#input_skill').attr('disabled', true);
            $('#input_skill').val('');
            general = 'F';
            eductab();
        }
        if (i == 3) {
            $('#input_eductab').attr('disabled', true);
            $('#input_eductab2').attr('disabled', true);
            $('#input_skill').attr('disabled', false);
            $('#input_eductab').val('');
            $('#input_eductab2').val('');
            general = 'F';
            skilltab();
        }
    }
    function save() {
        var ecode1, ecode2, skill;
        if ($('#input_eductab').val().split(' ')[0] != null) {
            ecode1 = $('#input_eductab').val().split(' ')[0];
        } else {
            ecode1 = '';
        }
        if ($('#input_eductab2').val().split(' ')[0] != null) {
            ecode2 = $('#input_eductab2').val().split(' ')[0];
        } else {
            ecode2 = '';
        }
        if ($('#input_skill').val().split(' ')[0] != null) {
            skill = $('#input_skill').val().split(' ')[0];
        } else {
            skill = '';
        }
        $.ajax({
            url: 'education/save',
            type: 'GET',
            dataType: 'JSON',
            data: {seq: seq,
                yearin: $('#input_yearin').val(),
                unit: $('#input_unit3').val().split(' ')[0],
                general: general,
                ecode1: ecode1,
                ecode2: ecode2,
                skill: skill,
                total: parseInt($('#input_total').val())},
        })
                .done(function () {
                    console.log("success");
                    viewtable($('#input_unit3').val().split(' ')[0]);
                    seq = '';
                    alert('บันทึกเรียบร้อย');
                    $('#input_skill').val('');
                    $('#input_skill').focus();
                    $('#input_eductab2').val('');
                    $('#input_eductab2').focus();
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function remove(id) {
        $.ajax({
            url: 'education/remove_hisrequire',
            type: 'GET',
            dataType: 'JSON',
            data: {seq: id},
        })
                .done(function () {
                    console.log("success");
                    viewtable($('#input_unit3').val().split(' ')[0]);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function edit(id) {

        $.ajax({
            url: 'education/edit_hisrequire',
            type: 'GET',
            dataType: 'JSON',
            data: {seq: id},
        })
                .done(function (result) {
                    console.log("success");
                    $.each(result['query'], function (index, val) {
                        $('#input_yearin').val(val.YEARIN);
                        $('#input_unit3').val(val.refnum + ' ' + val.unitname);
                        if (val.GENERAL == 'T') {
                            $('input[value=1]').attr('checked', true);
                            radio(1);
                        } else if (val.eductab != null) {
                            $('input[value=2]').attr('checked', true);
                            radio(2);
                            $('#input_eductab').val(val.eductab);
                            $('#input_eductab2').val(val.eductab2);
                        } else {
                            $('input[value=3]').attr('checked', true);
                            radio(3);
                            $('#input_skill').val(val.skill);
                        }
                        $('#input_total').val(parseInt(val.TOTAL));
                        seq = id;
                    });
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function viewtable2(gen,e1, e2,skill) {
        table=false;
      table2.rows().remove().draw();
        $.ajax({
            url: 'education/viewtable',
            type: 'GET',
            dataType: 'JSON',
            data: {
              gen:gen,
              ecode1: e1,
              ecode2: e2,
              skill:skill},
        })
                .done(function (result) {
                    console.log("success");
                    txt = '';
                    $('#table_view tbody').empty();
                    /*$.each(result['query'], function (index, val) {
                        txt += '<tr navyid=' + val.navyid + ' >\n\
                                <td nowrap >' + val.name + ' ' + val.sname + '</td>\n\
                                <td nowrap >' + val.unit3 + '</td>\n\
                                <td nowrap >' + val.unit4 + '</td>\n\
                                <td nowrap >' + val.unit + '</td>\n\
                                <td nowrap >' + val.unit1 + '</td>\n\
                                <td nowrap >' + val.unit2 + '</td>\n\
                                <td nowrap >' + val.educname + '</td>\n\
                                <td nowrap >' + val.skill + '</td>\n\
                                <td nowrap >' + val.percent + '</td>\n\
                                <td nowrap >' + val.title + '</td>\n\
                                </tr>\n\
';
                    });
                    $('#table_view tbody').append(txt);*/
                    table2.rows(table2.rows.add(result.query).draw()).nodes().to$().attr("navyid", result.query.navyid);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    }
    function radio2(i) {
        if (i == 1) {
            $('#input_eductab_p2').attr('disabled', true);
            $('#input_eductab2_p2').attr('disabled', true);
            $('#input_skill_p2').attr('disabled', true);
            $('#input_eductab_p2').val('');
            $('#input_eductab2_p2').val('');
            $('#input_skill_p2').val('');
            general = 'T';

        }
        if (i == 2) {
            $('#input_eductab_p2').attr('disabled', false);
            $('#input_eductab2_p2').attr('disabled', false);
            $('#input_skill_p2').attr('disabled', true);
            $('#input_skill_p2').val('');
            general = 'F';
            eductab_p2();
        }
        if (i == 3) {
            $('#input_eductab_p2').attr('disabled', true);
            $('#input_eductab2_p2').attr('disabled', true);
            $('#input_skill_p2').attr('disabled', false);
            $('#input_eductab_p2').val('');
            $('#input_eductab2_p2').val('');
            general = 'F';
            skilltab2();
        }
    }
    function search2() {
        viewtable2(general,$('#input_eductab_p2').val().split(' ')[0], $('#input_eductab2_p2').val().split(' ')[0],$('#input_skill_p2').val().split(" ")[0]);
    }
</script>
