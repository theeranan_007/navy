<script type="text/javascript">
var navyid;
var count = 0;
var refnum;
var row;
var next;
var prev;
var c = 0;
var limit = 10, rowtable = 0;
var all;
var d;
var input;
var table=false;
var oldnavyid = null;
var datatable = table_datatable();
$body = $("body");
$(document).on({

  ajaxStart: function() {
    if(table==false){
      $body.addClass("loading");
    }
    },
  ajaxStop: function() { $body.removeClass("loading"); }
});
$(document).ready(function () {
    $('#report_status').empty();
    $('#report_status')
    .append($("<option></option>")
    .attr("value", "รวม")
    .text("ทั้งหมด"));
    $.ajax({
        url: '<?php echo site_url('incident/statustab')?>',
        type: 'GET',
        dataType: 'JSON'
    })
    .done(function(result) {
        console.log("success");
        $.each(result,function(index, value) {
            $('#report_status')
            .append($("<option></option>")
            .attr("value", value['statuscode'])
            .text(value['title']));
        });
        $('#report_status').selectpicker('refresh');
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });

    count1();
    countall();
    unittabseting();
    unittab();
    $('#input_status').on('input',function(){
        console.log($('#input_status').attr('key'));
    });
    $.ajax({
        url: 'incident/status',
        type: 'GET',
        dataType: 'JSON',
    })
    .done(function (e) {
        {
            console.log(e.length);
            $('#input_status').empty();
            $.each(e, function (index, item) {
                if(item.statuscode==""){
                    item.statuscode="ALL";
                }
                $('#input_status')
                .append($("<option></option>")
                .attr("value", item.statuscode)
                .text(item.status));
            });
            $('#input_status').selectpicker('refresh');
        }
    })
    .fail(function () {
        console.log("error");
    })
    .always(function () {
        console.log("complete");
    });
    $('#select_refnum tbody').on('click', 'tr', function () {
        table=true;
        if (navyid!=$(this).attr('id') &&  $(this).hasClass('tsuccess')) {
            $(this).removeClass('tsuccess');
        } else {
            $('#select_refnum tbody tr.tsuccess').removeClass('tsuccess');
            $(this).addClass('tsuccess');
            navyid = $(this).attr('id');
            if(oldnavyid!=navyid){
                detail_manual();
                oldnavyid=navyid;
            }
        }

    });
    $('#button_status').on('click', function () {
        d = $('#input_status').selectpicker('val');
        navyid=null;
        incident_table();

    });
    $('#input_unit3').on('change',function (e) {

        $('#button_submit');
    });
    $('#updateunit').click(function () {
        update_unt34();
    });

    $('#fadetab1').fadeIn('400');
    $('#fadetab2').fadeIn('400');
});
function detail_manual() {   ///////////////////////////////////รายละเอียดข้อมูล

    $("#detail_name_manual").empty();
    $("#detail_sname_manual").empty();
    $("#detail_height_manual").empty();
    $("#patient_status_manual").css("background-color", "").empty();
    $("#detail_id8_manual").empty();
    $("#detail_statustab_manual").css("background-color", "").empty();
    $("#detail_yearin_manual").empty();
    $("#detail_religion_manual").empty();
    $("#detail_belong_to_manual").empty();
    $("#detail_eductab_manual").empty();
    $("#detail_is_request_manual").css("background-color", "").empty();
    $("#detail_percent_manual").empty();
    $("#detail_uint3_manual").empty();
    $("#detail_uint4_manual").empty();
    $("#detail_uint1_manual").empty();
    $("#detail_uint2_manual").empty();
    $("#detail_skill_manual").empty();
    $('#input_unitname4').selectpicker('').selectpicker('refresh');
    $.ajax({
        url: "<?php echo site_url('incident/query_detail')?>",
        data: "navyid=" + navyid,

        type: 'GET',
        dataType: 'JSON',
        cache: false,
        success: function (result) {
            obj = result[0];
            yearin = obj.yearin;
            id8 = obj.id8;
            name = obj.name;
            sname = obj.sname;
            cunit3 = obj.refnum3;
            $("#detail_name_manual").append(obj.name + "&nbsp;&nbsp;&nbsp;" + obj.sname);
            $("#detail_height_manual").append(obj.height);
            if (obj.patient_status != null) {
                if (obj.patient_status != "")
                $('#patient_status_manual').append(obj.patient_status).css("background-color", "#ff4000    ");
            }
            $('#detail_religion_manual').append(obj.religion);
            $("#detail_id8_manual").append(obj.id8);
            if (obj.statuscode != "AA") {
                $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")").css("background-color", "yellow");
            } else {
                $("#detail_statustab_manual").append(obj.title + "(" + obj.statuscode + ")");
            }
            if (obj.oldyerin != null) {
                $("#detail_yearin_manual").append(obj.yearin + "(" + obj.oldyerin + ")");
            } else {
                $("#detail_yearin_manual").append(obj.yearin);
            }
            if (obj.pseq < 10) {
                $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + "0" + obj.pseq + ")");
            } else {
                $("#detail_belong_to_manual").append(obj.company + "/" + obj.batt + "(" + obj.platoon + obj.pseq + ")");
            }
            $("#detail_eductab_manual").append(obj.educname);
            if (obj.is_request != "000") {
                $("#detail_is_request_manual").append("ร้องขอ").css("background-color", "#40ff00");
            }
            $("#detail_percent_manual").append(obj.percent);
            $("#detail_uint3_manual").append("<b>หน่วยปัจจุบัน:</b> " + obj.refnum3 + "&nbsp;&nbsp;&nbsp;&nbsp");
            if ((obj.postcode == null) && (obj.item == null))
            $("#detail_uint4_manual").append("- <b>หน่วย</b> - <b>ลำดับ</b> -");
            else
            $("#detail_uint4_manual").append(obj.refnum4 + " <b>หน่วย</b> " + obj.postname + " <b>ลำดับ</b> " + obj.item);
            $("#detail_uint1_manual").append(obj.refnum1);
            $("#detail_uint2_manual").append(obj.refnum2);
            $("#detail_skill_manual").append(obj.skill);
            $('#input_unitname4').val(obj.unit3+' '+obj.refnum3);
            // $("#input_unit3").val(obj.refnum3).selectpicker('refresh');
            // $("#input_unit3").selectpicker('toggle').selectpicker('toggle');
        },
        error: function (err) {
            alert(err);
        }
    });
    $.ajax({
        url: 'request/request_table_new',
        type: 'GET',
        dataType: 'JSON',
        data: 'where=person.navyid=' + navyid,
    })
    .done(function (e) {
        var txt='';
        $("#detail_request").empty();
        $.each(e['select'], function (index, val) {
            if(val['selectcode']==1){
                row = '<div class="tsuccess row">';
            }else{
                row = '<div class="row">';
            }
            txt+= row+' '
            +'<div class="col-md-5"><b>ขอลงหน่วย</b></div>'
            +'<div class="col-md-7" id="detail_unit_manual">'+val.refnum2+'</div>'
            +'</div>'
            +row+' '
            +'<div class="col-md-5"><b>ผู้ขอ</b></div>'
            +'<div class="col-md-7" id="detail_askby_manual">'+val.askby+'</div>'
            +'</div>'
            +row+' '
            +'<div class="col-md-5"><b>หมายเหตุ</b></div>'
            +'<div class="col-md-7" id="detail_remark_manual">'+val.remark+'</div>'
            +'</div>'
            +row+' '
            +'<div class="col-md-5"><b>ขอต่อ</b></div>'
            +'<div class="col-md-7" id="detail_remark2_manual">'+val.remark2+'</div>'
            +'</div>'
            +'<br>';
        });
        $("#detail_request").append(txt);
    })
    .fail(function () {
        console.log("error");
    })
    .always(function () {
        console.log("complete");
    });

}

function unittab() {
    $.ajax({
        url: "incident/unittab",
        type: 'GET',
        async: false,
        dataType: 'JSON',
        cache: false,
        success: function (result) {
            $('#input_unit3').empty();
            var tmp = 0;
            $.each(result['unit'], function (key, value) {////////////////////////////// หน่วยที่เลือก(unittab)
                $('#input_unit3')
                .append($("<option></option>")
                .attr("value", value['refnum'])
                .text(value['refnum']+" "+value['unitname']));
                if(value['refnum']==0){
                    $('#report_unit3')
                    .append($("<option></option>")
                    .attr("value", value['refnum'])
                    .text("ทั้งหมด"));
                }else{
                    $('#report_unit3')
                    .append($("<option></option>")
                    .attr("value", value['refnum'])
                    .text(value['refnum']+" "+value['unitname']));
                }
            });
            $('#input_unit3').selectpicker('refresh');
            $('#report_unit3').selectpicker('refresh');
        },
        error: function (err) {
            alert(err);
        }
    });
}

function hideload() {
    //document.getElementById("loading").style.display = 'none';
}
function showload() {
    //document.getElementById("loading").style.display = 'block';
}
function unit3_change() {
    $.ajax({
        url: '<?php echo site_url('incident/update_unit3')?>',
        type: 'GET',
        dataType:'JSON',
        data: {navyid: navyid,
            unit3:$('#input_unit3').selectpicker('val')}
        })
        .done(function() {
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
            incident_table();
            count1();
            countall();
        });

    }
    function update_unit34() {
        $.ajax({
            url: "incident/update_unit34",
            cache: false,
            success: function (result) {
                alert(result);
            },
            error: function (err) {
                alert(err);
            }
        });
    }

    function incident_table(){
        loadingStart('loading_search');
        if(d=="ALL"){
            d="รวม";
        }
        // datatable.clear().draw();
        var option="";
        $.ajax({
            url: '<?php echo site_url('incident/unittab')?>',
            type: 'GET',
            dataType: 'JSON'
        })
        .done(function(result) {
            $.each(result['unit'], function (key, value) {////////////////////////////// หน่วยที่เลือก(unittab)

                option += "<option value="+value['refnum']+">"+value['refnum']+" "+value['unitname']+"</option>";
            });


            $.ajax({
                url: 'incident/incident_table',
                type: 'GET',
                dataType: 'JSON',
                data: {status: d},
            })
            .done(function (e) {
              datatable.destroy();

                $('#select_refnum tbody').empty();
                txt="";
                $.each(e, function (index, val) {
                    if(val.refnumur == null)
                    val.refnumur = '-';
                    if(val.refnum4 == null)
                    val.refnum4 = '-';
                    if(val.oldyearin!=null && val.oldyearin !="")
                    val.yearin += " ("+val.oldyearin+")";
                    if(navyid==val.navyid){
                        txt = '<tr class="tsuccess" id="'+val.navyid+'">'
                    }
                    else{
                        txt = '<tr id="'+val.navyid+'">'
                    }
                    // if(val.oldyearin!=null && val.oldyearin !=""){
                    //     val.yearin += " ("+val.oldyearin+")";
                    // }

                    switch(val.batt){
                        case "5":{
                            val.belong += " ไม่มีความรู้";
                        }break;
                        case "6":{
                            val.belong += " ลาศึกษาต่อ";
                        }break;
                        case "7":{
                            val.belong += " หนีระหว่างนำส่ง";
                        }break;
                    }
                    // e[index].name+=' '+val.sname;
                    // e[index].select_unit3 = '<select id="setunit3_'+val.navyid+'" class="form-control selectpicker" onchange="update_unit3('+val.navyid+');" data-live-search="true" data-size="5">'+option
                    // + '</select>';
                    txt+='<td>'+val.name+' '+val.sname+'</td>'
                    +'<td><select id="setunit3_'+val.navyid+'" class="form-control selectpicker" onchange="update_unit3('+val.navyid+');" data-live-search="true" data-size="5">'+option
                    + '</select></td>'
                    +'<td>'+val.id8+'</td>'
                    +'<td>'+val.belong+'</td>'
                    +'<td>'+val.yearin+ '</td>'
                    +'<td>'+val.refnum4+'</td>'
                    +'<td>'+val.refnumur+'</td>'
                    +'<td>'+val.refnum1+'</td>'
                    +'<td>'+val.refnum2+'</td>'
                    +'<td>'+val.percent+'</td>'
                    +'<td>'+val.eductab+'</td>'
                    +'<td>'+val.status+'</td>'
                    +'</tr>';
                    $('#select_refnum tbody').append(txt)
                    $('#setunit3_'+val.navyid+'').selectpicker('val',val.unit3).selectpicker('refresh');
                });
                $("#select_refnum").tableHeadFixer({"left" : 2});
                // var i =datatable.rows.add(e).draw();
                // datatable.rows(i).nodes().to$().attr("id", e);
                // $('.selectpicker').selectpicker('refresh');
                datatable = table_datatable();
                loadingStop('loading_search');
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");

            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }
    function saveamount() {
        data = "";
        for (var i = 1; i <= 30; i++) {
            data += '&' + i + "=" + $('#input' + i).val();
        }
        $.ajax({
            url: 'request/saveamount?' + data,
            type: 'GET',
            dataType: 'JSON',
        })
        .done(function () {
            console.log("success");
            count1();
            countall();
        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });


    }
    function count1() {
        $.ajax({
            url: '<?php echo site_url('request/countunittab')?>',
            type: 'GET',
            dataType: 'JSON',
        })
        .done(function (result) {
            console.log("Query countall unittab >>> success");
            console.log(result);
            $('#count').empty();
            var txt = '<div class="col-xs-5" id="label"><b>หน่วย</b></div>'
            + '<div class="col-xs-3" id="labelall"><b>ยอดต้องการ</b></div>'
            + '<div class="col-xs-3" id="labelunit1"><b>มีเหตุ</b></div>'
            $.each(result['query'], function (index, val) {
                txt += '<div class="col-xs-5" id="label' + val.refnum + '"><b>' + val.unitname + '</b></div>'
                + '<div class="col-xs-3" id="countall_all' + val.refnum + '">0</div>'
                + '<div class="col-xs-2" id="countall_incident' + val.refnum + '">0</div>'
            });
            $('#count').append(txt);
        })
        .fail(function () {
            console.log("Query countall unittab >>> error");
        })
        .always(function () {
            console.log("Query countall unittab >>> complete");


            $.ajax({
                url: '<?php echo site_url('request/countamount')?>',
                type: 'GET',
                dataType: 'JSON',
            })
            .done(function (result) {
                console.log("Query countamount countall >>> success");
                console.log(result);
                $.each(result['query'], function (index, val) {
                    $('#countall_all' + val.unit + '').empty();
                    $('#countall_all' + val.unit + '').append(val.count);
                });
            })
            .fail(function () {
                console.log("Query countamount countall >>> error");
            })
            .always(function () {
                console.log("Query countamount countall >>> complete");

                $.ajax({
                    url: '<?php echo site_url('incident/count')?>',
                    type: 'GET',
                    dataType: 'JSON',
                })
                .done(function (result) {
                    console.log("Query countall countunit3 >>> success");
                    console.log(result);
                    $.each(result, function (index, val) {
                        $('#countall_incident' + val.unit + '').empty();
                        $('#countall_incident' + val.unit + '').append(val.count);
                        console.log(parseInt($('#countall_all' + val.unit + '').text())+" "+parseInt($('#countall_incident' + val.unit + '').text()));
                        if(parseInt($('#countall_all' + val.unit + '').text()) == parseInt($('#countall_incident' + val.unit + '').text())) {
                            $('#label' + val.unit + '').addClass('tsuccess');
                            $('#countall_all' + val.unit + '').addClass('tsuccess');
                            $('#countall_incident' + val.unit+ '').addClass('tsuccess');
                        }
                        else if(parseInt($('#countall_all' + val.unit + '').text()) < parseInt($('#countall_incident' + val.unit + '').text())) {
                            $('#label' + val.unit + '').addClass('twarning');
                            $('#countall_all' + val.unit + '').addClass('twarning');
                            $('#countall_incident' + val.unit + '').addClass('twarning');
                        }
                    });
                })
                .fail(function () {
                    console.log("Query countall countunit3 >>> error");
                })
                .always(function () {
                    console.log("Query countall countunit3 >>> complete");
                });
            });
        });

    }
    function countall() {
        $.ajax({
            url: 'incident/countall',
            type: 'GET',
            dataType: 'JSON',
        })
        .done(function (result) {
            console.log("success");
            $('#countall').empty();
            var txt = '<div class="col-md-6" id="label"><b>หน่วย</b></div>'

            + '<div class="col-md-3" id="count_all"><b>ยอดต้องการ</b></div>'
            + '<div class="col-md-3"id="count_requestall"><b>ยอดปัจจุบัน</b></div>';
            $.each(result, function (index, val) {
                if (val.unit != 'ไม่ได้ระบุ') {
                    if (parseInt(val.count) == parseInt(val.all)) {
                        console.log(val.unit + " " + val.count + " = " + val.all);
                        txt += '<div class="col-md-6 tsuccess" id="label"><b>' + val.unit + '</b></div>'

                        + '<div class="col-md-3 tsuccess" id="count_all">' + val.all + '</div>'
                        + '<div class="col-md-3 tsuccess" id="count_requestall">' + val.count + '</div>';

                    } else if (parseInt(val.count) > parseInt(val.all))
                    {
                        console.log(val.unit + " " + val.count + " > " + val.all);
                        txt += '<div class="col-md-6 twarning" id="label"><b>' + val.unit + '</b></div>'

                        + '<div class="col-md-3 twarning" id="count_all">' + val.all + '</div>'
                        + '<div class="col-md-3 twarning" id="count_requestall">' + val.count + '</div>';
                    } else
                    {
                        console.log(val.unit + " " + val.count + " < " + val.all);
                        txt += '<div class="col-md-6" id="label"><b>' + val.unit + '</b></div>'

                        + '<div class="col-md-3" id="count_all">' + val.all + '</div>'
                        + '<div class="col-md-3" id="count_requestall">' + val.count + '</div>';
                    }
                }
            });
            $('#countall').append(txt);
        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });

    }
    function unittabseting() {
        $.ajax({
            url: 'incident/amount',
            type: 'GET',
            dataType: 'JSON',
        })
        .done(function (e) {
            console.log(e.length);
            $('#amount').empty();
            txt = '';
            $.each(e, function (index, item) {
                if (item.refnum > 0) {
                    txt += '<div class="form-group">'
                    + '<div class="col-md-6">'
                    + '<label for="input' + item.refnum + '" >' + item.unitname + '</label>'
                    + '</div>'
                    + '<div class="col-md-6">'
                    + '<input type="number" class="form-control" onchange="saveamount();" onClick="SelectAll(input' + item.refnum + ');"'
                    + 'id="input' + item.refnum + '" value="' + item.count + '"/>'
                    + '</div>'
                    + '</div>'
                    + '</div>';
                }
            });
            $('#amount').append(txt);

        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });

    }
    function print(){
        str =""
        if($('#report_sortby').selectpicker('val')==1){
            str += "orderby=person.unit3 asc &";
        }
        if($('#report_sortby').selectpicker('val')==2){
            str += "orderby=person.statuscode asc,person.unit3 asc &";
        }
        str += "where=";
        if($('#report_unit3').selectpicker('val')!=0){
            str += "person.unit3="+$('#report_unit3').selectpicker('val');
            if($('#report_status').selectpicker('val')!="รวม"){
                str += 'and person.statuscode="'+$('#report_status').selectpicker('val')+'"';
                if($('#report_duplicate').selectpicker('val')==2){
                    str += ' and request.navyid is null and selectexam.navyid is null';
                }
                else if($('#report_duplicate').selectpicker('val')==3){
                    str += ' and selectexam.navyid is not null';
                }
                else if($('#report_duplicate').selectpicker('val')==4){
                    str += ' and request.navyid is not null';
                }
            }else{
                str += 'person.statuscode!="AA"';
                if($('#report_duplicate').selectpicker('val')==2){
                    str += ' and request.navyid is null and selectexam.navyid is null';
                }
                else if($('#report_duplicate').selectpicker('val')==3){
                    str += ' and selectexam.navyid is not null';
                }
                else if($('#report_duplicate').selectpicker('val')==4){
                    str += ' and request.navyid is not null';
                }
            }

        }
        else{
            if($('#report_status').selectpicker('val')!="รวม"){
                str += 'person.statuscode="'+$('#report_status').selectpicker('val')+'"';
                if($('#report_duplicate').selectpicker('val')==2){
                    str += ' and request.navyid is null and selectexam.navyid is null';
                }
                else if($('#report_duplicate').selectpicker('val')==3){
                    str += ' and selectexam.navyid is not null';
                }
                else if($('#report_duplicate').selectpicker('val')==4){
                    str += ' and request.navyid is not null';
                }
            }
            else{
                str += 'person.statuscode!="AA"';
                if($('#report_duplicate').selectpicker('val')==2){
                    str += ' and request.navyid is null and selectexam.navyid is null';
                }
                else if($('#report_duplicate').selectpicker('val')==3){
                    str += ' and selectexam.navyid is not null';
                }
                else if($('#report_duplicate').selectpicker('val')==4){
                    str += ' and request.navyid is not null';
                }
            }

        }



        window.open("<?php echo site_url('incident/pdf?')?>"+str,"_blank");
    }
    function update_unit3(id){
      table = false;
        $.ajax({
            url: "incident/update_unit3",
            data: "navyid=" + id + "&unit3=" + $('#setunit3_'+navyid+'').selectpicker('val'),
        }).done(function (){
            count1();
            countall();
            navyid = $('#'+id).next().addClass('tsuccess').attr('id');
            if(navyid!=null){
                $('#setunit3_'+navyid).selectpicker('toggle').selectpicker('toggle');
                //detail_manual();
            }
        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });

    }
    function loadingStart(id){
        $('#'+id+'').empty();
        $('#'+id+'').append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
        console.log('loadingStart '+id);
    }
    function loadingStop(id){
        $('#'+id+'').empty();
        console.log('loadingStop '+id);
    }
    function table_datatable(){
      return $('#select_refnum').DataTable({
          searching: false,
          paging:         false,
          "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false
      });
    }
    </script>
