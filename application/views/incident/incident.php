<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-md-9"  id="fadetab1"  style="display:none;">

        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1-1" data-toggle="tab">กรอกข้อมูล</a></li>
            <li><a href="#tab_1-2" data-toggle="tab">รายงาน</a></li>
            <li class="pull-right header"> มีเหตุ</li>
          </ul>
          <div class="tab-content">
          <div class="tab-pane active" id="tab_1-1">
              <div class="row">
                <div class="col-md-12">
                  <h4><b>เลือกสถานะภาพไม่ปกติ</b></h4>
                  <div class="row">

                    <div class="col-md-10">
                      <select type="text" class="form-control selectpicker" id="input_status">
                      </select>
                    </div>
                    <div class="col-md-1">
                      <button type="button" id="button_status" class="btn btn-primary"><i class="fa fa-search"></i> ค้นหา</button>
                    </div>
                  </div>
                  <hr />
                  <div class="row" >
                  <style>
                  headcol{
                    width:200px;
                    overflow-x:scroll;
                    margin-left: 15em;
                  }
                  .col{
                    position: absolute;
                    left: 0em;
                    top:auto;
                  }
                  </style>
                  <div class="col-md-12"  style="height: 400px; overflow-x: auto;overflow-y: auto;">
                  <!-- <div class="col-md-12" > -->
                    <table id="select_refnum" class="table">
                      <thead>
                        <tr>
                          <th>ชื่อ-สกุล</th>
                          <th>หน่วยลง</th>
                          <th>ทะเบียน</th>
                          <th>สังกัด</th>
                          <th>ผลัด</th>
                          <th>หน่วยคัดเลือก</th>
                          <th>ร้องขอ</th>
                          <th>สมัครใจ1</th>
                          <th>สมัครใจ2</th>
                          <th>คะแนน</th>
                          <th>วุฒิ</th>
                          <th>สถานภาพ</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>

                  </div>

                                      <div id="loading_search"></div>
                </div>
              </div>
              <!-- <div class="col-md-3">
                <br>

                <div class="row">
                  <div class="col-md-5"><b>ชื่อ</b></div>
                  <div class="col-md-7" id="detail_name_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>ผลัดที่ฝึก</b></div>
                  <div class="col-md-7" id="detail_yearin_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>ทะเบียน</b></div>
                  <div class="col-md-7" id="detail_id8_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>สังกัด</b></div>
                  <div class="col-md-7" id="detail_belong_to_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>สถานะภาพ</b></div>
                  <div class="col-md-7" id="detail_statustab_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>ยาเสพติด</b></div>
                  <div class="col-md-7" id="patient_status_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>ศาสนา</b></div>
                  <div class="col-md-7" id="detail_religion_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>สูง</b></div>
                  <div class="col-md-7" id="detail_height_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>ร้องขอ</b></div>
                  <div class="col-md-7" id="detail_is_request_manual">-</div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-5"><b>วุฒิ</b></div>
                  <div class="col-md-7" id="detail_eductab_manual">-</div>
                </div>

                <div class="row">
                  <div class="col-md-5"><b>ความรู้พิเศษ</b></div>
                  <div class="col-md-7" id="detail_skill_manual">-</div>
                </div>
                <div class="row">
                  <div class="col-md-5"><b>คะแนนสอบ</b></div>
                  <div class="col-md-7" id="detail_percent_manual">-</div>
                </div>

                <div class="row">
                  <div class="col-md-5"><b>สมัครใจ1</b></div>
                  <div class="col-md-7"id="detail_uint1_manual">-</div>
                </div>
                <div class="row">

                  <div class="col-md-5"><b>สมัครใจ2</b></div>
                  <div class="col-md-7"id="detail_uint2_manual">-</div>
                </div>
                <hr />

                <div class="row">
                  <div class="col-md-5"><b>หน่วยคัดเลือก</b></div>
                  <div class="col-md-7" id="detail_uint4_manual">-</div>
                </div>
                <hr />
                <div id="detail_request"></div>
              </div> -->
            </div>

          </div>
          <div class="tab-pane" id="tab_1-2">
            <div class="row">
              <div class="col-md-offset-3 col-md-6">
                <label>ระบุหน่วย</label>
                <select id="report_unit3" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
                </select>
              </div>

            </div>
            <div class="row">
              <div class="col-md-offset-3 col-md-6">
                <label>ระบุสถานภาพผิดปกติ</label>
                <select id="report_status" class="form-control selectpicker" data-live-search="true" data-show-subtext="true">

                </select>

              </div>
            </div>
            <div class="row">
              <div class="col-md-offset-3 col-md-6" >
                <label>ตรวจสอบข้อมูลซ้ำ</label>
                <select id="report_duplicate" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
                  <option value="1">
                    ทั้งหมด
                  </option>
                  <option value="2">
                    ไม่ซ้ำ
                  </option>
                  <option value="3">
                    มีเหตุซ้ำกับคัดเลือกหน่วย
                  </option>
                  <option value="4">
                    มีเหตุซ้ำกับร้องขอ
                  </option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-offset-3 col-md-6" >
                <label>เรียงตาม</label>
                <select id="report_sortby" class="form-control selectpicker" data-live-search="true" data-show-subtext="true" >
                  <option value="1">
                    หน่วย
                  </option>
                  <option value="2">
                    สถานะภาพ
                  </option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-offset-3 col-md-6" >
                <br />
                <button type="button" class="btn btn-primary btn-block" onclick="print();"><i class="fa fa-print"></i> ออกรายงาน</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="box box-primary">
        <div class="box box-body">
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-5"><b>ชื่อ</b></div>
              <div class="col-md-7" id="detail_name_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>ผลัดที่ฝึก</b></div>
              <div class="col-md-7" id="detail_yearin_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>ทะเบียน</b></div>
              <div class="col-md-7" id="detail_id8_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>สังกัด</b></div>
              <div class="col-md-7" id="detail_belong_to_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>สถานะภาพ</b></div>
              <div class="col-md-7" id="detail_statustab_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>ยาเสพติด</b></div>
              <div class="col-md-7" id="patient_status_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>ศาสนา</b></div>
              <div class="col-md-7" id="detail_religion_manual">-</div>
            </div>

          </div>
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-5"><b>สูง</b></div>
              <div class="col-md-7" id="detail_height_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>ร้องขอ</b></div>
              <div class="col-md-7" id="detail_is_request_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>วุฒิ</b></div>
              <div class="col-md-7" id="detail_eductab_manual">-</div>
            </div>

            <div class="row">
              <div class="col-md-5"><b>ความรู้พิเศษ</b></div>
              <div class="col-md-7" id="detail_skill_manual">-</div>
            </div>
            <div class="row">
              <div class="col-md-5"><b>คะแนนสอบ</b></div>
              <div class="col-md-7" id="detail_percent_manual">-</div>
            </div>

            <div class="row">
              <div class="col-md-5"><b>สมัครใจ1</b></div>
              <div class="col-md-7"id="detail_uint1_manual">-</div>
            </div>
            <div class="row">

              <div class="col-md-5"><b>สมัครใจ2</b></div>
              <div class="col-md-7"id="detail_uint2_manual">-</div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-5"><b>หน่วยคัดเลือก</b></div>
              <div class="col-md-7" id="detail_uint4_manual">-</div>
            </div>
          </div>
          <div class="col-md-3">
            <div id="detail_request"></div>
          </div>
        </div>
      </div>

    </div>
    <div class="col-md-3"  id="fadetab2"  style="display:none;">
      <div class="box box-info">
        <div class="box-body">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab2default" data-toggle="tab">ยอดมีเหตุ</a></li>
            <li><a href="#tab1default" data-toggle="tab">ยอดรวม</a></li>
            <li><a href="#tab3default" data-toggle="tab">ตั้งค่า</a></li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="panel with-nav-tabs skin-blue">
              <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane fade" id="tab1default">
                    <div class="row" id="countall">
                    </div>
                  </div>
                  <div class="tab-pane fade in active" id="tab2default">
                    <div class="row" id="count">
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab3default">
                    <button type="button" onclick="saveamount();" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                    <div class="form-group" id="amount">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br/>
</section>
</div>
