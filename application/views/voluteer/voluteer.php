<div class="content-wrapper">
    <style type="text/css">

        .panel.with-nav-tabs .panel-heading{
            padding: 5px 5px 0 5px;
        }
        .panel.with-nav-tabs .nav-tabs{
            border-bottom: none;
        }
        .panel.with-nav-tabs .nav-justified{
            margin-bottom: -1px;
        }
        /********************************************************************/
        /*** PANEL DEFAULT ***/
        .with-nav-tabs.panel-default .nav-tabs > li > a,
        .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
        }
        .with-nav-tabs.panel-default .nav-tabs > .open > a,
        .with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
            background-color: #ddd;
            border-color: transparent;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.active > a,
        .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
            color: #555;
            background-color: #fff;
            border-color: #ddd;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f5f5f5;
            border-color: #ddd;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #777;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #ddd;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #555;
        }
        /********************************************************************/
        /*** PANEL PRIMARY ***/
        .with-nav-tabs.panel-primary .nav-tabs > li > a,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
        }
        .with-nav-tabs.panel-primary .nav-tabs > .open > a,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
            background-color: #3071a9;
            border-color: transparent;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
            color: #428bca;
            background-color: #fff;
            border-color: #428bca;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #428bca;
            border-color: #3071a9;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #fff;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #3071a9;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            background-color: #4a9fe9;
        }
        /********************************************************************/
        /*** PANEL SUCCESS ***/
        .with-nav-tabs.panel-success .nav-tabs > li > a,
        .with-nav-tabs.panel-success .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li > a:focus {
            color: #3c763d;
        }
        .with-nav-tabs.panel-success .nav-tabs > .open > a,
        .with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-success .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li > a:focus {
            color: #3c763d;
            background-color: #d6e9c6;
            border-color: transparent;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.active > a,
        .with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
            color: #3c763d;
            background-color: #fff;
            border-color: #d6e9c6;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #3c763d;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #d6e9c6;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #3c763d;
        }
        /********************************************************************/
        /*** PANEL INFO ***/
        .with-nav-tabs.panel-info .nav-tabs > li > a,
        .with-nav-tabs.panel-info .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
            color: #31708f;
        }
        .with-nav-tabs.panel-info .nav-tabs > .open > a,
        .with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-info .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
            color: #31708f;
            background-color: #bce8f1;
            border-color: transparent;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.active > a,
        .with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
            color: #31708f;
            background-color: #fff;
            border-color: #bce8f1;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #31708f;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #bce8f1;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #31708f;
        }
        /********************************************************************/
        /*** PANEL WARNING ***/
        .with-nav-tabs.panel-warning .nav-tabs > li > a,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
            color: #8a6d3b;
        }
        .with-nav-tabs.panel-warning .nav-tabs > .open > a,
        .with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
            color: #8a6d3b;
            background-color: #faebcc;
            border-color: transparent;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a,
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
            color: #8a6d3b;
            background-color: #fff;
            border-color: #faebcc;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #fcf8e3;
            border-color: #faebcc;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #8a6d3b;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #faebcc;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #8a6d3b;
        }
        /********************************************************************/
        /*** PANEL DANGER ***/
        .with-nav-tabs.panel-danger .nav-tabs > li > a,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
            color: #a94442;
        }
        .with-nav-tabs.panel-danger .nav-tabs > .open > a,
        .with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
            color: #a94442;
            background-color: #ebccd1;
            border-color: transparent;
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a,
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
            color: #a94442;
            background-color: #fff;
            border-color: #ebccd1;
            border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f2dede; /* bg color */
            border-color: #ebccd1; /* border color */
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #a94442; /* normal text color */
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #ebccd1; /* hover bg color */
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff; /* active text color */
            background-color: #a94442; /* active bg color */
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="col-xs-8"  id="fadetab1"  style="display:none;">
                <div class="box box-info">
                    <div class="box-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabvoluteer_select" data-toggle="tab">คัดสรรค</a></li>
                            <li><a href="#tabfilterunit" data-toggle="tab">ดูตามหน่วย</a></li>
                            <p class="pull-right">ความสมัครใจ</p>
                        </ul>
                        <!--</div>
                            <div class="box-body">-->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="panel with-nav-tabs skin-blue">

                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade" id="tabfilterunit">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-xs-3">
                                                            <label for="input_unittab_filter" >คัดหน่วย</label>
                                                        </div>
                                                        <div class="col-xs-9">
                                                            <input type="text" class="form-control"  onClick="SelectAll('input_unittab_filter');" id="input_unittab_filter" list="datalist_unittab"/>
                                                            <datalist id="datalist_unittab"></datalist>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row" >
                                                    <div class="col-xs-7" style="height: 490px; overflow-x: auto;overflow-y: auto;">
                                                        <table id="table_filter" class="table" >
                                                            <thead>
                                                                <tr>
                                                                    <th  class="text-center">ชื่อ-สกุล</th>
                                                                    <th  class="text-center">หน่วยลง</th>
                                                                    <th  class="text-center">สมัครใจ1</th>
                                                                    <th  class="text-center">สมัครใจ2</th>
                                                                    <th  class="text-center">คะแนนสอบ</th>
                                                                    <th  class="text-center">สถานภาพ</th>


                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>-</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-xs-5">
                                                            <!--<p><b>รายละเอียด</b></p>
                                                            <hr>-->
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ชื่อ</b></div>
                                                            <div class="col-xs-7" id="detail_name_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ผลัดที่ฝึก</b></div>
                                                            <div class="col-xs-7" id="detail_yearin_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ทะเบียน</b></div>
                                                            <div class="col-xs-7" id="detail_id8_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>สังกัด</b></div>
                                                            <div class="col-xs-7" id="detail_belong_to_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>สถานะภาพ</b></div>
                                                            <div class="col-xs-7" id="detail_statustab_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ยาเสพติด</b></div>
                                                            <div class="col-xs-7" id="patient_status_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-5"><b>ศาสนา</b></div>
                                                            <div class="col-md-7" id="detail_religion_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>สูง</b></div>
                                                            <div class="col-xs-7" id="detail_height_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ร้องขอ</b></div>
                                                            <div class="col-xs-7" id="detail_is_request_manual">-</div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-xs-5"><label>เลือกหน่วย</label></div>
                                                            <div class="col-xs-7">
                                                                <input type="text" class="form-control"  onClick="SelectAll('input_unittab3');" id="input_unittab3" list="datalist_unittab"/>
                                                                <datalist id="datalist_unittab"></datalist>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-5"><b>หน่วยคัดเลือก</b></div>
                                                            <div class="col-xs-7" id="detail_uint4_manual">-</div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>วุฒิการศึกษา</b></div>
                                                            <div class="col-xs-7" id="detail_eductab_manual">-</div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-5"><b>ความรู้พิเศษ</b></div>
                                                            <div class="col-xs-7" id="detail_skill_manual">-</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-5"><b>คะแนนสอบ</b></div>
                                                            <div class="col-xs-7" id="detail_percent_manual">-</div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-5"><b>หน่วยสมัครใจ1</b></div>
                                                            <div class="col-xs-7"id="detail_uint1_manual">-</div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-xs-5"><b>หน่วยสมัครใจ2</b></div>
                                                            <div class="col-xs-7"id="detail_uint2_manual">-</div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade in active" id="tabvoluteer_select">
                                                <div class="row">
                                                    <div class="col-xs-5"><label>#</label></div>
                                                    <div class="col-xs-7">
                                                        <div class="form-group">
                                                            <select class="form-control" id="voluteer_select">
                                                                <option selected value="1">สมัครใจ1</option>
                                                                <option value="2">สมัครใจ2</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-xs-12">
                                                        <lable class="pull-right form-group">
                                                            <button type="button" onclick="process();" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>

                                                            <input type="checkbox" checked onchange="checkall();" id="checkall">
                                                            เลือกทั้งหมด
                                                        </lable>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-xs-12">
                                                        <div class="form-group" id="voluteer">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="panelRequest4">
                                                <div class="row">
                                                    <div class="col-xs-5"><label>เลือกหน่วย</label></div>
                                                    <div class="col-xs-7">
                                                        <input type="text" class="form-control"  onClick="SelectAll('input_unitname4');" id="input_unitname4" list="datalist_unitname"/>
                                                        <datalist id="datalist_unitname"></datalist>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab4default">Default 4</div>
                                            <div class="tab-pane fade" id="tab5default">Default 5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4"  id="fadetab2"  style="display:none;">
                <div class="box box-info">
                    <div class="box-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabcount" data-toggle="tab">ยอดสมัครใจ</a></li>
                            <li><a href="#tabcountall" data-toggle="tab">ยอดรวม</a></li>
                            <li><a href="#tabsetting" data-toggle="tab">ตั้งค่า</a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel with-nav-tabs skin-blue">
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tabcount">
                                            <div class="row" id="count">
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tabcountall">
                                            <div class="row" id="countall">
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" id="tabsetting">
                                            <div class="form-group" id="amount">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/>
    </section>
</div>
