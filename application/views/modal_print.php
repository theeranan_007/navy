<div class="modal fade modal-wide" id="detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><b>รายละเอียด</b>                </h4>
            </div>
            <div class="modal-body" >
                <div id="printContentall">
                    <div class="row">
                        <div class="col-xs-12"><div class="text-center"><font size="6"><b>ประวัติ</b></font></div></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row" id="printContent">
                                <div class="col-xs-2"  id="printContent1">
                                    <img id="imgprofile" style="cursor:zoom-in;"class=""/>
                                </div>
                                <div class="col-xs-5"  id="printContent2">
                                    <p><strong>พลทหาร <strong id='detail_name'></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong> เครื่องหมาย <strong id='detail_id8'></strong></p>
                                    <p>เลขประจำตัวประชาชน <strong id='detail_id13'></strong> เลขประจำตัว <strong  id='detail_id'></strong></p>
                                    <p>ตำหนิแผลเป็น <strong id='detail_mark'></strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; สูง <strong id='detail_height'></strong> ซ.ม.</p>
                                    <p>บ้านเลขที่ <strong id='detail_address'></strong>&nbsp;&nbsp;&nbsp;&nbsp; ซอย <strong id='detail_address_soil'></strong> ถนน<strong id='detail_address_road'></strong>ตำบล <strong id='detail_town3'></strong></p>
                                    <p>อำเภอ <strong id='detail_town2'></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; จังหวัด <strong id='detail_town1'></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; รหัสไปรษณีย์ <strong id='detail_zipcode'></strong></p>
                                </div>
                                <div class="col-xs-5"  id="printContent3">
                                    <p>ขึ้นทะเบียนกองประจำการ <strong id="detail_regdate"></strong>&nbsp;&nbsp;&nbsp;เช้าประจำการในผลัด <strong id="detail_yearin"></strong></p>
                                    <p>รายงานตัวเข้ากองประจำการ <strong id="detail_repdate"></strong>&nbsp;&nbsp;&nbsp;&nbsp;รหัสโค้ด <strong id="detail_runcode"></strong></p>
                                    <p>ปัจจุบันสังกัด ร้อย <strong  id='detail_company'></strong> พัน <strong  id='detail_batt'></strong> หมวด <strong  id='detail_platoon'></strong> ลำดับ <strong  id='detail_pseq'></strong></p>
                                    <p>สอบผ่านคัดเลือก<strong> &ndash;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; หน่วยที่ตก <strong id="detail_unit3"></strong>. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p> ย้ายหน่วย <strong id="detail_movedate"></strong></p>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-xs-7" id="printContent4">
                                    <p>เกิดเมื่อ <strong id='detail_birthdate'></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สมัครใจ1 <strong id='detail_unit1'></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สมัครใจ2 <strong id='detail_unit2'></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; หมู่โลหิต <strong id='detail_blood'> </strong>&nbsp;&nbsp;&nbsp; อาชีพ &nbsp;&nbsp;&nbsp;&nbsp; <strong id='detail_occname'></strong>&nbsp;&nbsp;</p>
                                    <p>ศาสนา <strong  id='detail_religion'></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;บิดาชื่อ <strong  id='detail_father'></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; มารดาชื่อ <strong  id='detail_mother'></strong></p>
                                    <p>การศึกษา <strong  id='detail_educname'></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ความรู้พิเศษ <strong  id='detail_skillname'></strong></p>
                                    <p>สถานศึกษา&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong id="detail_schoolname" ></strong>&nbsp;&nbsp; ระยะว่ายน้ำ&nbsp;&nbsp;&nbsp;&nbsp;<strong  id='detail_swimcode'></strong>&nbsp;&nbsp;&nbsp;&nbsp; เมตร&nbsp; คะแนนสอบ&nbsp;&nbsp;&nbsp;<strong  id='detail_percent'></strong></p>
                                    <p><strong id="is_request"></strong> ร้องขอเข้ากองประจำการ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สถานภาพ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="btn_statuscode" class="btn btn-success" style="width: 250px;height: 30px"><p class="text-center" id="statuscode">ปกติ</p></button></p>
                                    <p><strong id="addressmove"></strong> ย้ายทะเบียนบ้าน&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ยาเสพติด&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="btn_patient_status" class="btn btn-success" style="width: 250px;height: 30px"><p class="text-center" id="patient_status">ปกติ</p></button></p>
                                </div>
                               <div class="col-xs-5"  id="printContent5">
                                    <p>&nbsp;</p>
                                    <p>บรรจุหน่วย <strong  id='detail_unit3'></strong></p>
                                    <p>สังกัด <strong  id='detail_mcp'></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; แผนก <strong id='detail_mc'></strong></P>
                                    <p> หน่วยย่อย <strong id='detail_mc5'></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; หมวด</p>
                                    <p>เหล่า <strong id='detail_groupname'></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; วันบรรจุ <strong id='detail_start'></strong></p>
                                    <p>หมายเหตุ:  <strong id='detail_mc4'></strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a onclick="printWindow();"> <button TYPE="button" class="btn btn-info pull-right" ><i class="fa fa-print"> พิมพ์</i></button>
                </a>
                 <a onclick="TranscriptionFrom();"> <button TYPE="button" class="btn btn-default pull-right" > บัตรประจำตัวประชาชน</button>
                </a></div>
        </div><!-- /.modal-content -->
    </div>
</div>
