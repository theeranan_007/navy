<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $CI;
	public function __construct(){
		parent:: __construct();	
		$this->CI =& get_instance();
	}
	public function get_data(){
		
		$data['method'] = $this->CI->router->method;
		$data['controller'] = $this->CI->router->class;

		
		//$data['lastname']= $this->CI->session->userdata('lastname');
		//$data['rs']=$this->query();

		//$data['is_logged_in']= $this->CI->session->userdata('is_logged_in');
		return $data;
	}
	public function index()
	{	$data=$this->get_data();
		$this->load->view('login',$data);
	}
	public function logout(){
		$this->load->model('loginModel');
		$query=$this->loginModel->un_validate();
		redirect('login');
	}
	public function validate()
	{
		$this->load->model('loginModel');
		$query = $this->loginModel->validate();
		if($query)
			{
			
			redirect('main');
		}
		else
		{
			redirect('login');
		}
	}
}
