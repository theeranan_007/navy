<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class cansusRegistration extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $CI;

    public function __construct() {
    	parent:: __construct();
    	$this->CI = & get_instance();
    }

    public function get_data() {
    	$data['method'] = $this->CI->router->method;
    	$data['controller'] = $this->CI->router->class;
    	$data['email'] = $this->CI->session->userdata('email');
    	$data['username'] = $this->CI->session->userdata('username');
    	$data['firstname'] = $this->CI->session->userdata('firstname');
    	$data['lastname'] = $this->CI->session->userdata('lastname');
    	$data['img']= $this->CI->session->userdata('img');
    	return $data;
    }

    public function index() {
    	$data = $this->get_data();
    	$this->load->view('header', $data);
    	$this->load->view('cansusRegistration/cansusRegistration');
    	$this->load->view('footer');
    	$this->load->view('cansusRegistration/script1');
    	$this->load->view('close_page');

    }
    public function query()
    {
    	$this->load->model('cansusRegistration_model');
    	$result = $this->cansusRegistration_model->search();
    	$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function unittab()
    {
    	$this->load->model('cansusRegistration_model');
    	$result = $this->cansusRegistration_model->unittab();
    	$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function towncode1()
    {
    	$this->load->model('cansusRegistration_model');
    	$result = $this->cansusRegistration_model->towncode1();
    	$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function update_request()
    {
    	$this->load->model('separation_unit_model');
    	$this->separation_unit_model->request_update();
    }
    public function updata_request()
    {
    	$this->load->model('separation_unit_model');
    	$this->separation_unit_model->request_updata();
                //$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function insert_request()
    {
    	$this->load->model('separation_unit_model');
    	$this->separation_unit_model->request_insert();
    }
    public function delete_request()
    {
    	$this->load->model('separation_unit_model');
    	$this->separation_unit_model->request_delete();
    }
    public function from_request()
    {
    	$this->load->model('separation_unit_model');
    	$result = $this->separation_unit_model->request_from();
    	$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function towncode()
    {
    	$this->load->model('search_model');
    	$result = $this->search_model->towncode();
    	$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function add_request()
    {
    	$this->load->model('separation_unit_model');
    	$result = $this->separation_unit_model->request_add();
    	$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function save_request()
    {
    	$this->load->model('separation_unit_model');
    	$result = $this->separation_unit_model->request_save();
    	$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function test(){
    	$this->load->view('datatable');
    }

}
