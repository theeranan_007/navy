<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $CI;
	public function __construct(){
		parent:: __construct();
		$this->CI =& get_instance();
	}
	public function get_data(){
		$data['method'] = $this->CI->router->method;
		$data['controller'] = $this->CI->router->class;
		$data['email']= $this->CI->session->userdata('email');
		$data['username']= $this->CI->session->userdata('username');
		$data['firstname']= $this->CI->session->userdata('firstname');
		$data['lastname']= $this->CI->session->userdata('lastname');
                $data['img']= $this->CI->session->userdata('img');

		//$data['lastname']= $this->CI->session->userdata('lastname');
		//$data['rs']=$this->query();

		//$data['is_logged_in']= $this->CI->session->userdata('is_logged_in');
		return $data;
	}
	public function index()
	{
		$percent = $this->percentage();
    $data=$this->get_data();
		$this->load->view('header',$data);
		$this->load->view('dashboard',$percent);
		$this->load->view('footer');
    $this->load->view('script_dashboard');
		$this->load->view('close_page');
		//$this->datageneral();
	}
	public function search1()
	{
		$data=$this->get_data();
		$this->load->view('header',$data);
		$this->load->view('search1');
		$this->load->view('footer');
	}
	public function search2()
	{
		$data=$this->get_data();
		$this->load->view('header',$data);
		$this->load->view('search2');
		$this->load->view('footer');
	}
	public function test()
	{
		$data=$this->get_data();
		$this->load->view('header',$data);
		$this->load->view('test');
		$this->load->view('footer');
		$this->load->view('testscript');
	}
	public function query_data()
	{
		$data=$this->get_data();
		$data['table']= $this->get_table();
		$this->load->view('header',$data);
		$this->load->view('from_query',$data);
		$this->load->view('footer');
	}
	public function percentage()
{
$this->load->model('main_model');
	$result = $this->main_model->percentage();
	//$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
	return $result;
}
public function percentage2()
{
$this->load->model('main_model');
$result = $this->main_model->percentage2();
$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
}
public function setdatabase()
	{
            $this->CI->session->set_userdata('database',$this->input->get('database'));
	}
	public function get_table(){
		$this->load->model('database');
		$data=$this->database->query_tables();
		return $data;
	}
	public function get_fields($get=null){
		$this->load->model('database');
		$data['fields']=$this->database->query_fields($get);
		$this->output->set_output(json_encode($data));
		//return $data;
	}
	public function get_datas($get=null,$sel=null){
		$this->load->model('database');
		$data=$this->database->query_data($get,$sel);
		$this->output->set_output(json_encode($data));
		//return $data;
	}
	  public function person() {
        $this->load->model('request_model');
        $result = $this->request_model->request_person();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
}
