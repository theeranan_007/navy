<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class voluteer extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $CI;

    public function __construct() {
        parent:: __construct();
        $this->CI = & get_instance();
    }
    
    public function get_data() {
        $data['method'] = $this->CI->router->method;
        $data['controller'] = $this->CI->router->class;
        $data['email'] = $this->CI->session->userdata('email');
        $data['username'] = $this->CI->session->userdata('username');
        $data['firstname'] = $this->CI->session->userdata('firstname');
        $data['lastname'] = $this->CI->session->userdata('lastname');
        $data['img'] = $this->CI->session->userdata('img');
        return $data;
    }
    public function index() {
        $data = $this->get_data();
        $this->load->view('header', $data);
        $this->load->view('voluteer/voluteer');
        $this->load->view('footer');
        $this->load->view('voluteer/script1');
        $this->load->view('close_page');
    }
    ////////////////////////////////// query unittab //////////////////////////////
    public function unittab(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->unittab();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    //////////////////////////////// query จำนวนที่ต้องการของแต่ละหน่วย //////////////////
    public function countall(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->countall();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    ////////////////////////////////// query unit1 //////////////////////////////
    public function countunit1(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->countunit1();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    ////////////////////////////////// query unit2 //////////////////////////////
    public function countunit2(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->countunit2();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    ////////////////////////////////// query unit3 //////////////////////////////
    public function countunit3(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->countunit3();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    ////////////////////////////////// query unit3 //////////////////////////////
    public function filter_unit(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->filter_unit();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
////////////////////////////////////////////////////call model reutrn json /////////////////////////////////////////
    public function count1(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->count1();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function count2(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->count2();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
     public function process(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->process();
        echo $result;
    	//$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    
    public function counttotal(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->counttotal();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function amount(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->amount();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function saveamount(){
    	$this->load->model('voluteer_model');
    	$result = $this->voluteer_model->saveamount();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function query() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->search_query();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function query_detail() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->detail();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function query_voluteer() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->voluteer();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function update_voluteer() {
        $this->load->model('voluteer_model');
        $this->voluteer_model->voluteer_update();
        //$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
    public function update_unit3() {
        $this->load->model('voluteer_model');
        $this->voluteer_model->unit3_update();
        //$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }

    public function insert_voluteer() {
        $this->load->model('voluteer_model');
        $this->voluteer_model->voluteer_insert();
    }

    public function delete_voluteer() {
        $this->load->model('voluteer_model');
        $this->voluteer_model->voluteer_delete();
    }

    public function from_voluteer() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->voluteer_from();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function search_voluteer() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->search_query();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function voluteer_table() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->table_voluteer();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function voluteer_print() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->print_voluteer();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function unittab_voluteer() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->voluteer_unittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function add_voluteer() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->voluteer_add();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function save_voluteer() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->voluteer_save();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function person() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->voluteer_person();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }


    public function pdf() {
        $this->load->library('Pdf');
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
        //do not show header or footer
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        // add a page - landscape style
        $pdf->AddFont("thsarabunpsk", "thsarabunpsk.php");

        // set font
        $pdf->SetMargins(2, 0, 2, true);
        $pdf->SetAutoPageBreak(TRUE, 0);
        $pdf->AddPage("L");
        $pdf->SetFont("thsarabunpsk", 'B', 14);
        $pdf->SetFillColor(224, 235, 255);
        $h = 6;
        $fill = 0;
        
        $pdf->SetFont("thsarabunpsk", "", 12);
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->print_voluteer_pdf();
        $pdf->Cell(0, 14, "รายชื่อขอทหาร เพื่อพิจารณาจัดแบ่ง ผลัด " . $result['select'][0]['yearin'], 0, 0, 'C', $fill, 0);
        $pdf->ln(10);
        //$pdf->SetFont($family, $style, $size)
        if(array_key_exists("status",$this->input->get()) && array_key_exists("skill",$this->input->get()) && array_key_exists("selectexam",$this->input->get())){
            $w = array(8, 5, 15, 20, 35, 38, 25, 31, 40, 25, 25, 25,25);
        //foreach($data as $row) {
        $pdf->Cell($w[0], $h, "ที่", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[1], $h, "#", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[2], $h, "ร/พ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[3], $h, "ทะเบียน", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[4], $h, "ชื่อ - นามสกุล", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[5], $h, "ผู้ขอ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[6], $h, "หน่วยที่ขอ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[7], $h, "ขอต่อ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[8], $h, "หมายเหตุ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[9], $h, "สถานภาพ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[10], $h, "ความรู้", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[11], $h, "คัดเลือกหน่วย", 'TB', 0, 'L', $fill, 0);
        $pdf->ln($h);
        $oldname = '';
        $count = 1;
        if ($result['select'] != null) {
            $r = $result['select'];

            foreach ($result['select'] as $key => $value) {
                if ($oldname == $value['name']) {
                    $value['belong'] = "";
                    $value['id8'] = "";
                    $value['name'] = "";
                    $value['sname'] = "";
                    $border = "";
                } else {

                    $border = "";
                    $oldname = $value['name'];
                }
                if ($key + 1 < sizeof($r)) {
                    if ($r[$key + 1]['name'] == $value['name']) {
                        $fill = 1;
                    } else {
                        $fill = 0;
                    }
                }

                $pdf->Cell($w[0], $h, ($key + 1), $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[1], $h, $value['selectcode'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[2], $h, $value['belong'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[3], $h, $value['id8'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[4], $h, $value['name'] . " " . $value['sname'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[5], $h, $value['askby'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[6], $h, $value['refnum3'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[7], $h, $value['remark2'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[8], $h, $value['remark'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[9], $h, $value['title'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[10], $h, $value['educname'] . ' ' . $value['skill'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[11], $h, $value['postname'] . " " . $value['refnum4'], $border, 0, 'L', $fill, 0);
                $pdf->ln($h);

                if ($count == 32) {
                    $fill = 0;
                    $pdf->SetFont("thsarabunpsk", 'B', 14);
                    $pdf->Cell(0, 14, "รายชื่อขอทหาร เพื่อพิจารณาจัดแบ่ง ผลัด" . $result['select'][0]['yearin'], 0, 0, 'C', $fill, 0);
                    $pdf->ln(10);
                    $pdf->SetFont("thsarabunpsk", "", 12);
                    $count = 0;
                    $pdf->Cell($w[0], $h, "ที่", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[1], $h, "#", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[2], $h, "ร/พ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[3], $h, "ทะเบียน", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[4], $h, "ชื่อ - นามสกุล", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[5], $h, "ผู้ขอ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[6], $h, "หน่วยที่ขอ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[7], $h, "ขอต่อ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[8], $h, "หมายเหตุ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[9], $h, "สถานภาพ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[10], $h, "ความรู้", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[11], $h, "คัดเลือกหน่วย", 'TB', 0, 'L', $fill, 0);
                    $pdf->ln($h);
                    $fill = !$fill;
                }
                $count++;
            }
        }
        }
        $pdf->Output('voluteer.pdf', 'I');
    }

    public function voluteer_table_new() {
        $this->load->model('voluteer_model');
        $result = $this->voluteer_model->print_voluteer_pdf();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    function utf8_to_tis620($str) {
        return iconv('UTF-8', 'TIS-620', $str);
    }

}
