<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class fromInsert extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $CI;

    public function __construct() {
        parent:: __construct();
        $this->CI = & get_instance();
    }
    
    public function get_data() {
        $data['method'] = $this->CI->router->method;
        $data['controller'] = $this->CI->router->class;
        $data['email'] = $this->CI->session->userdata('email');
        $data['username'] = $this->CI->session->userdata('username');
        $data['firstname'] = $this->CI->session->userdata('firstname');
        $data['lastname'] = $this->CI->session->userdata('lastname');
        $data['img'] = $this->CI->session->userdata('img');
        return $data;
    }
    public function index() {
        $data = $this->get_data();
        $this->load->view('header', $data);
        $this->load->view('FromInsert/fromInsert');
        $this->load->view('footer');
        $this->load->view('FromInsert/script');
        $this->load->view('close_page');
    }
    public function save(){
        $this->load->model('fromInsert_model');
        $this->fromInsert_model->save();
    }
    public function person() {
        $this->load->model('fromInsert_model');
        $result = $this->fromInsert_model->person();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function detail() {
        $this->load->model('fromInsert_model');
        $result = $this->fromInsert_model->detail();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function viewtable() {
        $this->load->model('fromInsert_model');
        $result = $this->fromInsert_model->viewtable();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
}
?>
