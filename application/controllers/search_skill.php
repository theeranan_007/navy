<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class search_skill extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $CI;

    public function __construct() {
        parent:: __construct();
        $this->CI = & get_instance();
    }

    public function get_data() {
        $data['method'] = $this->CI->router->method;
        $data['controller'] = $this->CI->router->class;
        $data['email'] = $this->CI->session->userdata('email');
        $data['username'] = $this->CI->session->userdata('username');
        $data['firstname'] = $this->CI->session->userdata('firstname');
        $data['lastname'] = $this->CI->session->userdata('lastname');
        $data['img'] = $this->CI->session->userdata('img');
        return $data;
    }

    public function index() {
        $data = $this->get_data();
        $this->load->view('header', $data);
        $this->load->view('search_skill/search_skill');
        $this->load->view('footer');
        $this->load->view('search_skill/script1');
        $this->load->view('close_page');
    }

    public function query() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->query();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function unittab() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->unittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function update_unit3() {
        $this->load->model('search_skill_model');
        $this->search_skill_model->unit3_update();
    }

    public function eductab() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->eductab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
     public function eductab2() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->eductab2();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function eductab_p2() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->eductab_p2();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
     public function eductab2_p2() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->eductab2_p2();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function viewtable() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->viewtable();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function skilltab() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->skilltab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function query_detail() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->detail();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function search_skill_table_new() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->search_skill_table_new();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function count(){
    	$this->load->model('search_skill_model');
    	$result = $this->search_skill_model->count();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function countall(){
    	$this->load->model('search_skill_model');
    	$result = $this->search_skill_model->countall();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function amount(){
    	$this->load->model('search_skill_model');
    	$result = $this->search_skill_model->amount();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function saveamount(){
    	$this->load->model('search_skill_model');
    	$result = $this->search_skill_model->saveamount();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function save(){
    	$this->load->model('search_skill_model');
    	$result = $this->search_skill_model->save();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
     public function remove_hisrequire(){
    	$this->load->model('search_skill_model');
    	$result = $this->search_skill_model->remove_hisrequire();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function edit_hisrequire(){
    	$this->load->model('search_skill_model');
    	$result = $this->search_skill_model->edit_hisrequire();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function count_hisrequire(){
    	$this->load->model('search_skill_model');
    	$result = $this->search_skill_model->count_hisrequire();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function person() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->search_skill_person();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function unittab_search_skill() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->search_skill_unittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function viewtable_search_skill() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->search_skill_viewtable();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function num() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->search_skill_num();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function insert() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->insert();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function remove() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->remove();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function position_search_skill() {
        $this->load->model('search_skill_model');
        $result = $this->search_skill_model->search_skill_position();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function test() {
        $this->load->view('datatable');
    }

}
