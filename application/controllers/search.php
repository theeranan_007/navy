<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class search extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $CI;

    public function __construct() {
        parent:: __construct();
        $this->CI = & get_instance();
    }

    public function get_data() {
        $data['method'] = $this->CI->router->method;
        $data['controller'] = $this->CI->router->class;
        $data['email'] = $this->CI->session->userdata('email');
        $data['username'] = $this->CI->session->userdata('username');
        $data['firstname'] = $this->CI->session->userdata('firstname');
        $data['lastname'] = $this->CI->session->userdata('lastname');
         $data['img']= $this->CI->session->userdata('img');
        return $data;
    }

    public function index() {
        $data = $this->get_data();
        $this->load->view('header', $data);
        $this->load->view('search/search');
        $this->load->view('footer');
        $this->load->view('search/script');
        $this->load->view('close_page');
    }
   	public function query()
	{
		$this->load->model('search_model');
		$result = $this->search_model->search_query();
		//$r = json_decode($result,JSON_UNESCAPED_UNICODE);
		$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
	}
	public function query_detail()
	{
		$this->load->model('search_model');
		$result = $this->search_model->detail();
		//$r = json_decode($result,JSON_UNESCAPED_UNICODE);
		$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
	}
        public function towncode()
	{
		$this->load->model('search_model');
		$result = $this->search_model->towncode();
                $this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
	}

}
