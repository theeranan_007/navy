<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class search_person_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('search_person_model','search_person');
    }
    public function search()
    {
    $this->load->model('search_person_model');
    $result = $this->search_person_model->search_query();
    //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
    $this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
}
