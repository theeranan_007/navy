<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class selectexam extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $CI;

    public function __construct() {
        parent:: __construct();
        $this->CI = & get_instance();
    }

    public function get_data() {
        $data['method'] = $this->CI->router->method;
        $data['controller'] = $this->CI->router->class;
        $data['email'] = $this->CI->session->userdata('email');
        $data['username'] = $this->CI->session->userdata('username');
        $data['firstname'] = $this->CI->session->userdata('firstname');
        $data['lastname'] = $this->CI->session->userdata('lastname');
        $data['img'] = $this->CI->session->userdata('img');
        return $data;
    }

    public function index() {
        $data = $this->get_data();
        $this->load->view('header', $data);
        $this->load->view('selectexam/selectexam');
        $this->load->view('footer');
        $this->load->view('selectexam/script1');
        $this->load->view('close_page');
    }

    public function query() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->query();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function unittab() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->unittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function checknum(){
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->checknum();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));

    }
    public function update_unit3() {
        $this->load->model('selectexam_model');
        $this->selectexam_model->unit3_update();
    }

    public function update_unit34() {
        $this->load->model('selectexam_model');
        $this->selectexam_model->unit34_update();
    }

    public function query_detail() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->detail();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function count_position() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->count_position();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function selectexam_table_new() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->selectexam_table_new();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function count(){
    	$this->load->model('selectexam_model');
    	$result = $this->selectexam_model->count();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function countall(){
    	$this->load->model('selectexam_model');
    	$result = $this->selectexam_model->countall();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function amount(){
    	$this->load->model('selectexam_model');
    	$result = $this->selectexam_model->amount();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function saveamount(){
    	$this->load->model('selectexam_model');
    	$result = $this->selectexam_model->saveamount();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function person() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->selectexam_person();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function unittab_selectexam() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->selectexam_unittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function viewtable_selectexam() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->selectexam_viewtable();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function num() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->selectexam_num();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function insert() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->insert();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function remove() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->remove();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function position_selectexam() {
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->selectexam_position();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function test() {
        $this->load->view('datatable');
    }
    public function pdf() {
        $this->load->library('Pdf');

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
        //do not show header or footer
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        // add a page - landscape style
        $pdf->AddFont("thsarabunpsk", "thsarabunpsk.php");

        // set font
        $pdf->SetMargins(2, 0, 2, true);
        $pdf->SetAutoPageBreak(TRUE, 0);
        $pdf->AddPage("L");
        $pdf->SetFont("thsarabunpsk", 'B', 14);
        $pdf->SetFillColor(224, 235, 255);
        $h = 6;
        $fill = 0;

        $pdf->SetFont("thsarabunpsk", "", 12);
        $this->load->model('selectexam_model');
        $result = $this->selectexam_model->print_pdf();
        // echo "<pre>";
        // print_r($result);
        // echo "<pre>";
        // exit();
        $pdf->Cell(0, 14, "รายชื่อทหารคัดเลือกหน่วย เพื่อพิจารณาจัดแบ่ง ผลัด " . $result[0]['yearin'], 0, 0, 'C', $fill, 0);
        $pdf->ln(10);
        //$pdf->SetFont($family, $style, $size)
        $w = array(8, 4, 8, 15, 32, 20, 32, 60, 24, 24, 20,30, 20);
        //foreach($data as $row) {
        $pdf->Cell($w[0], $h, "ที่", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[2], $h, "ร/พ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[3], $h, "ทะเบียน", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[4], $h, "ชื่อ - นามสกุล", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[5], $h, "หน่วยตก", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[6], $h, "หน่วยคัดเลือก", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[7], $h, "หน่วยร้องขอ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[8], $h, "หน่วยสมัครใจ1", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[9], $h, "หน่วยสมัครใจ1", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[10], $h, "สถานภาพ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[11], $h, "ยาเสพติด", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[12], $h, "หมายเหตุ", 'TB', 0, 'L', $fill, 0);
        $pdf->ln($h);
        $oldname = '';
        $count = 1;
        if ($result != null) {
            $r = $result;

            foreach ($result as $key => $value) {

                if ($oldname == $value['id8']) {
                    $value['belong'] = "";
                    $value['id8'] = "";
                    $value['name'] = "";
                    $value['sname'] = "";
                    $border = "";
                } else {

                    $border = "";
                    $oldname = $value['id8'];

                }

                $pdf->Cell($w[0], $h, ($key + 1), $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[2], $h, $value['belong'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[3], $h, $value['id8'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[4], $h, $value['name'] . " " . $value['sname'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[5], $h, $value['refnum3'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[6], $h, $value['refnum4'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[7], $h, $value['refnumur'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[8], $h, $value['refnum1'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[9], $h, $value['refnum2'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[10], $h, $value['status'] ,$border, 0, 'L', $fill, 0);
                $pdf->Cell($w[11], $h, $value['patient_status'] ,$border, 0, 'L', $fill, 0);
                $pdf->Cell($w[12], $h, $value['oldyearin'], $border, 0, 'L', $fill, 0);
                $pdf->ln($h);

                if ($count == 32) {
                    $fill = 0;
                    $pdf->SetFont("thsarabunpsk", 'B', 14);
                    $pdf->Cell(0, 14, "รายชื่อทหารมีสถานภาพผิดปกติ เพื่อพิจารณาจัดแบ่ง ผลัด " . $result[0]['yearin'], 0, 0, 'C', $fill, 0);
                    $pdf->ln(10);
                    $pdf->SetFont("thsarabunpsk", "", 12);
                    $count = 0;
                    $pdf->Cell($w[0], $h, "ที่", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[2], $h, "ร/พ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[3], $h, "ทะเบียน", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[4], $h, "ชื่อ - นามสกุล", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[5], $h, "หน่วยตก", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[6], $h, "หน่วยคัดเลือก", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[7], $h, "หน่วยร้องขอ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[8], $h, "หน่วยสมัครใจ1", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[9], $h, "หน่วยสมัครใจ1", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[10], $h, "สถานภาพ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[11], $h, "ยาเสพติด", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[12], $h, "หมายเหตุ", 'TB', 0, 'L', $fill, 0);
                    $pdf->ln($h);
                }
                $count++;
            }
        }
        $pdf->Output('request.pdf', 'I');
    }
}
