<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class request extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $CI;

    public function __construct() {
        parent:: __construct();
        $this->CI = & get_instance();
    }

    public function get_data() {
        $data['method'] = $this->CI->router->method;
        $data['controller'] = $this->CI->router->class;
        $data['email'] = $this->CI->session->userdata('email');
        $data['username'] = $this->CI->session->userdata('username');
        $data['firstname'] = $this->CI->session->userdata('firstname');
        $data['lastname'] = $this->CI->session->userdata('lastname');
        $data['img'] = $this->CI->session->userdata('img');
        return $data;
    }

    public function index() {
        $data = $this->get_data();
        $this->load->view('header', $data);
        $this->load->view('request/request');
        $this->load->view('footer');
        $this->load->view('request/script1');
        $this->load->view('close_page');
    }

////////////////////////////////////////////////////call model reutrn json /////////////////////////////////////////
    public function count() {
        $this->load->model('request_model');
        $result = $this->request_model->count();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function countunittab() {
        $this->load->model('request_model');
        $result = $this->request_model->countunittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function countall() {
        $this->load->model('request_model');
        $result = $this->request_model->countall();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function checknum() {
        $this->load->model('request_model');
        $result = $this->request_model->checknum();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function countamount() {
        $this->load->model('request_model');
        $result = $this->request_model->countamount();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function countrequest() {
        $this->load->model('request_model');
        $result = $this->request_model->countrequest();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function amount() {
        $this->load->model('request_model');
        $result = $this->request_model->amount();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function saveamount() {
        $this->load->model('request_model');
        $result = $this->request_model->saveamount();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function query() {
        $this->load->model('request_model');
        $result = $this->request_model->search_query();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function query_detail() {
        $this->load->model('request_model');
        $result = $this->request_model->detail();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function query_request() {
        $this->load->model('request_model');
        $result = $this->request_model->request();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function update_request() {
        $this->load->model('request_model');
        $this->request_model->request_update();
        //$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }
public function edit_request() {
        $this->load->model('request_model');
        $this->request_model->request_edit();
        //$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }

    public function update_unit3() {
        $this->load->model('request_model');
        $this->request_model->unit3_update();
        //$this->output->set_output(json_encode($result,JSON_UNESCAPED_UNICODE));
    }

    public function insert_request() {
        $this->load->model('request_model');
        $this->request_model->request_insert();
    }

    public function delete_request() {
        $this->load->model('request_model');
        $this->request_model->request_delete();
    }

    public function from_request() {
        $this->load->model('request_model');
        $result = $this->request_model->request_from();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function search_request() {
        $this->load->model('request_model');
        $result = $this->request_model->search_query();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function request_table() {
        $this->load->model('request_model');
        $result = $this->request_model->table_request();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function request_print() {
        $this->load->model('request_model');
        $result = $this->request_model->print_request();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function unittab_request() {
        $this->load->model('request_model');
        $result = $this->request_model->request_unittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function add_request() {
        $this->load->model('request_model');
        $result = $this->request_model->request_add();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function save_request() {
        $this->load->model('request_model');
        $result = $this->request_model->request_save();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function person() {
        $this->load->model('request_model');
        $result = $this->request_model->request_person();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function pdf() {
        $this->load->library('Pdf');

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
        //do not show header or footer
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        // add a page - landscape style
        $pdf->AddFont("thsarabunpsk", "thsarabunpsk.php");

        // set font
        $pdf->SetMargins(2, 0, 2, true);
        $pdf->SetAutoPageBreak(TRUE, 0);
        $pdf->AddPage("L");
        $pdf->SetFont("thsarabunpsk", 'B', 14);
        $pdf->SetFillColor(224, 235, 255);
        $h = 6;
        $fill = 0;

        $pdf->SetFont("thsarabunpsk", "", 12);
        $this->load->model('request_model');
        $result = $this->request_model->print_request_pdf();
        $pdf->Cell(0, 14, "รายชื่อขอทหาร เพื่อพิจารณาจัดแบ่ง ผลัด " . $result['select'][0]['yearin'], 0, 0, 'C', $fill, 0);
        $pdf->ln(10);
        //$pdf->SetFont($family, $style, $size)
        $w = array(8, 4, 6, 13, 32, 38, 25, 31, 40, 25, 36, 35);
        //foreach($data as $row) {
        $pdf->Cell($w[0], $h, "ที่", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[1], $h, "#", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[2], $h, "ร/พ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[3], $h, "ทะเบียน", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[4], $h, "ชื่อ - นามสกุล", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[5], $h, "ผู้ขอ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[6], $h, "หน่วยที่ขอ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[7], $h, "ขอต่อ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[8], $h, "หมายเหตุ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[9], $h, "สถานภาพ", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[10], $h, "ความรู้", 'TB', 0, 'L', $fill, 0);
        $pdf->Cell($w[11], $h, "คัดเลือกหน่วย", 'TB', 0, 'L', $fill, 0);
        $pdf->ln($h);
        $oldname = '';
        $count = 1;
        if ($result['select'] != null) {
            $r = $result['select'];

            foreach ($result['select'] as $key => $value) {

                if ($oldname == $value['id8']) {
                    $value['belong'] = "";
                    $value['id8'] = "";
                    $value['name'] = "";
                    $value['sname'] = "";
                    $border = "";
                } else {

                    $border = "";
                    $oldname = $value['id8'];

                }

                if ($key + 1 < sizeof($r)) {

                    if ($r[$key + 1]['name'] == $value['name'] || $value['name'] == "") {
                        $fill = 1;

                    } else{
                        $fill = 0;
                    }
                }
                else if($key + 1 == sizeof($r) && $value['name'] == ""){
                    $fill = 1;
                }
                else {
                    $fill = 0;
                }

                $pdf->Cell($w[0], $h, ($key + 1), $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[1], $h, $value['selectcode'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[2], $h, $value['belong'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[3], $h, $value['id8'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[4], $h, $value['name'] . " " . $value['sname'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[5], $h, $value['askby'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[6], $h, $value['refnum2'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[7], $h, $value['remark2'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[8], $h, $value['remark'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[9], $h, $value['title'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[10], $h, $value['educname'] . ' ' . $value['skill'], $border, 0, 'L', $fill, 0);
                $pdf->Cell($w[11], $h, $value['postname'] . " " . $value['refnum4'], $border, 0, 'L', $fill, 0);
                $pdf->ln($h);

                if ($count == 32) {
                    $fill = 0;
                    $pdf->SetFont("thsarabunpsk", 'B', 14);
                    $pdf->Cell(0, 14, "รายชื่อขอทหาร เพื่อพิจารณาจัดแบ่ง ผลัด" . $result['select'][0]['yearin'], 0, 0, 'C', $fill, 0);
                    $pdf->ln(10);
                    $pdf->SetFont("thsarabunpsk", "", 12);
                    $count = 0;
                    $pdf->Cell($w[0], $h, "ที่", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[1], $h, "#", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[2], $h, "ร/พ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[3], $h, "ทะเบียน", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[4], $h, "ชื่อ - นามสกุล", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[5], $h, "ผู้ขอ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[6], $h, "หน่วยที่ขอ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[7], $h, "ขอต่อ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[8], $h, "หมายเหตุ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[9], $h, "สถานภาพ", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[10], $h, "ความรู้", 'TB', 0, 'L', $fill, 0);
                    $pdf->Cell($w[11], $h, "คัดเลือกหน่วย", 'TB', 0, 'L', $fill, 0);
                    $pdf->ln($h);
                    $fill = !$fill;
                }
                $count++;
            }
        }
        $pdf->Output('request.pdf', 'I');
    }

    public function request_table_new() {
        $this->load->model('request_model');
        $result = $this->request_model->print_request_pdf();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    function utf8_to_tis620($str) {
        return iconv('UTF-8', 'TIS-620', $str);
    }

}
