<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class education extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $CI;

    public function __construct() {
        parent:: __construct();
        $this->CI = & get_instance();
    }

    public function get_data() {
        $data['method'] = $this->CI->router->method;
        $data['controller'] = $this->CI->router->class;
        $data['email'] = $this->CI->session->userdata('email');
        $data['username'] = $this->CI->session->userdata('username');
        $data['firstname'] = $this->CI->session->userdata('firstname');
        $data['lastname'] = $this->CI->session->userdata('lastname');
        $data['img'] = $this->CI->session->userdata('img');
        return $data;
    }

    public function index() {
        $data = $this->get_data();
        $this->load->view('header', $data);
        $this->load->view('education/education');
        $this->load->view('footer');
        $this->load->view('education/script1');
        $this->load->view('close_page');
    }

    public function query() {
        $this->load->model('education_model');
        $result = $this->education_model->query();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function unittab() {
        $this->load->model('education_model');
        $result = $this->education_model->unittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function update_unit3() {
        $this->load->model('education_model');
        $this->education_model->unit3_update();
    }

    public function eductab() {
        $this->load->model('education_model');
        $result = $this->education_model->eductab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
     public function eductab2() {
        $this->load->model('education_model');
        $result = $this->education_model->eductab2();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function eductab_p2() {
        $this->load->model('education_model');
        $result = $this->education_model->eductab_p2();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
     public function eductab2_p2() {
        $this->load->model('education_model');
        $result = $this->education_model->eductab2_p2();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function viewtable() {
        $this->load->model('education_model');
        $result = $this->education_model->viewtable();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function skilltab() {
        $this->load->model('education_model');
        $result = $this->education_model->skilltab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function skilltab2() {
        $this->load->model('education_model');
        $result = $this->education_model->skilltab2();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function query_detail() {
        $this->load->model('education_model');
        $result = $this->education_model->detail();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function education_table_new() {
        $this->load->model('education_model');
        $result = $this->education_model->education_table_new();
        //$r = json_decode($result,JSON_UNESCAPED_UNICODE);
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function count(){
    	$this->load->model('education_model');
    	$result = $this->education_model->count();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function countall(){
    	$this->load->model('education_model');
    	$result = $this->education_model->countall();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function amount(){
    	$this->load->model('education_model');
    	$result = $this->education_model->amount();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function saveamount(){
    	$this->load->model('education_model');
    	$result = $this->education_model->saveamount();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function save(){
    	$this->load->model('education_model');
    	$result = $this->education_model->save();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
     public function remove_hisrequire(){
    	$this->load->model('education_model');
    	$result = $this->education_model->remove_hisrequire();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function edit_hisrequire(){
    	$this->load->model('education_model');
    	$result = $this->education_model->edit_hisrequire();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
    public function count_hisrequire(){
    	$this->load->model('education_model');
    	$result = $this->education_model->count_hisrequire();
    	$this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function person() {
        $this->load->model('education_model');
        $result = $this->education_model->education_person();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function unittab_education() {
        $this->load->model('education_model');
        $result = $this->education_model->education_unittab();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function viewtable_education() {
        $this->load->model('education_model');
        $result = $this->education_model->education_viewtable();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function num() {
        $this->load->model('education_model');
        $result = $this->education_model->education_num();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function insert() {
        $this->load->model('education_model');
        $result = $this->education_model->insert();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function remove() {
        $this->load->model('education_model');
        $result = $this->education_model->remove();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function position_education() {
        $this->load->model('education_model');
        $result = $this->education_model->education_position();
        $this->output->set_output(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function test() {
        $this->load->view('datatable');
    }

}
