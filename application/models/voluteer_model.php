<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class voluteer_model extends CI_Model {

    private $ci;

    ///////////////////////////////// switch database //////////////////////////
    public function selectdb() {
         if($this->ci->session->userdata('database')!=null){
             return $this->ci->session->userdata('database');
         }
         else{
             return 'navdb';
         }
    }

    ////////////////////////////////ends switch database ///////////////////////////////

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }
       public function person() {
        $this->load->database('test');
        $this->db->select('CONCAT (person.name ," " ,person.sname ," " ,person.address," " , person.id8 ," - ",person.navyid) AS name')
                ->from('person')->limit('4')
                ->like("person.name", $this->input->get('name'), 'after')
                ->like("person.sname", $this->input->get('sname'), 'after')
                ->like("person.id8", $this->input->get('id8'), 'after');
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }
       public function detail() {
        $this->load->database('test');
        $this->db->select('p.yearin,p.name,p.sname,p.address,p.id8,p.navyid,p.unit3,u.unitname')
                ->from('test.person p')
                ->join('navdb.unittab u','u.refnum = p.unit3','LEFT')
                ->where('p.navyid', $this->input->get('navyid'));
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }
    public function viewtable(){
        
    }

    public function save(){
        $this->load->database('test');
        
        $array = array(
            'yearin' => $this->input->get('yearin'),
            'name' => $this->input->get('name'),
            'sname' => $this->input->get('sname'),
            'address' => $this->input->get('address'),
            'id8' => $this->input->get('id8'),
            'unit3' => $this->input->get('unit3'),
            );
        if($this->input->get('navyid')==''){
            $this->db->set($array)
            ->insert('person');
        }else{
        $this->db->set($array)
                ->where('navyid', $this->input->get('navyid'))
            ->update('person');
        }
        $this->db->close();
    }
  

    public function amount() {
        $this->load->database($this->selectdb());
        $query_new = $this->db->select('unittab.unitname,unittab.refnum,amount.unit,amount.count,amount.yearin')->from('amount')
                        ->join('unittab', 'unittab.refnum = amount.unit')
                        ->get()->result();
        return $query_new;
    }

   
    public function counttotal() {
        $this->load->database($this->selectdb());
        $this->db->select('unittab.refnum,unittab.unitname as unit,count(person.unit3) as count,amount.count as all,amount.count-count(person.unit3) as total')
                ->from('person')->where('unittab.movestat', 1)
                ->join('unittab', 'unittab.refnum = person.unit3', 'RIGHT')
                ->join('amount', 'amount.unit = unittab.refnum', 'LEFT')
                ->group_by('unittab.refnum')
                ->order_by('unittab.refnum', 'asc');
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }

    public function saveamount() {
        $array = $this->input->get();
        $this->load->database($this->selectdb());
        for ($i = 1; $i <= 30; $i++) {
            $this->db->set('count', $array[$i])->where('unit', $i)->update('amount');
        }
        $this->db->close();
    }

    public function process() {
        $this->load->database($this->selectdb());
        $query = $this->db->select('person.navyid')
                        ->from('person')->where($this->input->get('unit'))->where('(person.unit3 = 0 or person.unit3 = "")')
                        ->order_by('person.percent DESC')
                        ->limit($this->input->get('limit'))
                        ->get()->result_array();
        if (sizeof($query) != 0) {

            foreach ($query as $key => $result) {
                $this->db->set('person.unit3', $this->input->get('unit3'))
                        ->where('person.navyid', $result['navyid'])
                        ->update('person');
            }
        }
        $this->db->close();
    }

    /////////////////////////////////////// query unittab //////////////////////////////
    public function unittab() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('unitname,refnum')->from('unittab')->where('movestat',1);
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query all //////////////////////////////
    public function countall() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('count,unit')->from('amount');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query unit1 //////////////////////////////
    public function countunit1() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('unit1 as unit,count(unit1) as count')->from('person')->where('unit3 = 0')
                    ->group_by('unit1')->order_by('unit1 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query unit2 //////////////////////////////
    public function countunit2() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('unit2 as unit,count(unit2) as count')->from('person')->where('unit3 = 0')
                    ->group_by('unit2')->order_by('unit2 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query unit3 //////////////////////////////
    public function countunit3() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('unit3,count(unit3) as count')->from('person')
                    ->group_by('unit3')->order_by('unit3 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query filter_unit3 //////////////////////////////
    public function filter_unit() {
        $respond['query']='';
        $respond['error']='';
        $str = 'person.navyid,person.name,person.sname,person.percent,u1.unitname as unit1,u2.unitname as unit2,u3.unitname as unit3,statustab.title';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('person')
                    ->join('unittab as u1','u1.refnum = person.unit1','LEFT')
                    ->join('unittab as u2','u2.refnum = person.unit2','LEFT')
                    ->join('unittab as u3','u3.refnum = person.unit3','LEFT')
                    ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
                    ->where('unit3',$this->input->get('unit3'))
                    ->order_by('person.percent desc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    public function unit3_update(){
        $this->load->database($this->selectdb());
        $this->db->set('unit3',$this->input->get('unit3'))
                ->where('navyid',$this->input->get('navyid'))
                ->update('person');
        $this->db->close();
    }
}
