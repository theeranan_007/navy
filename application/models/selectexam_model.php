<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class selectexam_model extends CI_Model {

   private $ci;

   public function __construct() {
      // Assign the CodeIgniter super-object
      $this->ci = & get_instance();
   }
   public function selectdb() {
      if($this->ci->session->userdata('database')!=null){
         return $this->ci->session->userdata('database');
      }
      else{
         return 'navdb';
      }
   }
   public function query() {
      $select = "person.navyid,person.name,person.sname,CONCAT(statustab.title,'(',person.statuscode,')') as status,CONCAT_WS('/',person.company,person.batt) as belong,unittab.refnum,unittab.unitname,person.id8,person.oldyearin";
      $this->load->database($this->selectdb());
      $query_new['count'] = $this->db->select("COUNT(navyid) AS count")->from("person")->get()->result(); //
      $query_new['select'] = $this->db->select($select)->from('person')->limit($this->input->get('limit'), $this->input->get('row'))
      ->join('statustab', 'statustab.statuscode = person.statuscode', 'LEFT')
      ->join('unittab', 'unittab.refnum = person.unit3', 'LEFT')
      ->where("person.statuscode !=", "AA")
      ->order_by('person.statuscode', 'asc')
      ->order_by('person.batt', 'asc')
      ->order_by('person.company', 'asc')
      ->order_by('person.id8', 'asc')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }
   public function print_pdf() {
      $this->load->database($this->selectdb());
      $select="person.navyid,patient.title as patient_status,person.batt,concat(person.company,'/',person.batt) as belong,person.yearin,person.name,person.sname,person.id8,u1.unitname as refnum1,u2.unitname as refnum2,u3.unitname as refnum3,
      concat(ur.unitname,' ',request.askby,'(',request.num,')') as refnumur,concat(positiontab.postname,' ',
      u4.unitname,'(',selectexam.item,')') as refnum4,person.oldyearin,statustab.title as status";
      $this->db->select($select)->from('person')
      ->join('request','request.navyid = person.navyid and request.selectcode=1','LEFT')
      ->join('selectexam','selectexam.navyid = person.navyid','LEFT')
      ->join('unittab as ur','ur.refnum = request.unit','LEFT')
      ->join('positiontab','positiontab.postcode = selectexam.postcode','LEFT')
      ->join('unittab as u1','u1.refnum = person.unit1','LEFT')
      ->join('unittab as u2','u2.refnum = person.unit2','LEFT')
      ->join('unittab as u3','u3.refnum = person.unit3','LEFT')
      ->join('unittab as u4','u4.refnum = selectexam.unit4','LEFT')
      ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
      ->join('statustab patient','patient.statuscode = person.patient_status','LEFT');
      if($this->input->get('where')!=""){
         $this->db->where($this->input->get('where'));
      }
      if($this->input->get('orderby')!=""){
         $this->db->order_by($this->input->get('orderby'));
      }
      $query = $this->db->get()->result_array();
      $this->db->close();
      return $query;
   }
   public function selectexam_table_new() {
      $this->load->database($this->selectdb());
      $str = 'selectexam.navyid,person.name,person.sname,u4.unitname as refnum4,positiontab.postname,selectexam.item,statustab.title,u3.unitname as refnum3';
      $this->db->select($str)->from('selectexam')
      ->join('person', 'person.navyid = selectexam.navyid', 'LEFT')
      ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
      ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
      ->join('positiontab', 'selectexam.postcode = positiontab.postcode', 'LEFT')
      ->join('statustab', 'statustab.statuscode = person.statuscode', 'LEFT')
      ->where($this->input->get('sql'))
      ->order_by('selectexam.postcode asc,selectexam.item asc');
      $query = $this->db->get()->result();
      return $query;
   }
   public function print_selectexam_pdf() {

      $select = 'p.yearin,p.name,p.sname,p.id8,concat(company,"/",batt) as belong,positiontab.postname,'
      . 'u4.unitname as unit4,eductab.educname,statustab.title,skilltab.skill,selectexam.item,'
      . 'u3.unitname as unit3';
      $this->load->database($this->selectdb());
      $this->db->select('selectexam.navyid')->from('selectexam');
      if($this->input->get('sql')!="")
      $this->db->where($this->input->get('sql'))->order_by('selectexam.unit4 asc,selectexam.num asc');
      /* if($this->input->get('askby')!="")
      $this->db->like("askby", $this->input->get('askby'), "after");
      if($this->input->get('unit')!="")
      $this->db->where("unit", $this->input->get('unit')); */
      $query1 = $this->db->group_by('selectexam.navyid')->get()->result_array();
      $query_new['select'] = null;
      if ($query1 != null) {
         foreach ($query1 as $key => $result) {
            //echo $result['navyid'];
            if ($query_new['select'] == null) {
               $this->db->select($select)->from('selectexam')->limit('10')
               ->join('person as p', 'selectexam.navyid = p.navyid', "LEFT")
               ->join('statustab','p.statuscode = statustab.statuscode','LEFT')
               ->join('skilltab', 'p.skillcode = skilltab.skillcode', 'LEFT')
               ->join('eductab', 'p.educode1 = eductab.ecode1 and p.educode2 = eductab.ecode2', 'LEFT')
               ->join('positiontab','positiontab.postcode = selectexam.postcode','LEFT')
               ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
               ->join('unittab as u3', 'u3.refnum = p.unit3', 'LEFT')
               ->where("selectexam.navyid ", $result['navyid']);
               if ($this->input->get('orderby') != "")
               $this->db->order_by($this->input->get('orderby'));
               $query_new['select'] = $this->db->get()->result_array();
            }
            else {
               $ $this->db->select($select)->from('selectexam')->limit('10')
               ->join('person as p', 'selectexam.navyid = p.navyid', "LEFT")
               ->join('statustab','p.statuscode = statustab.statuscode','LEFT')
               ->join('skilltab', 'p.skillcode = skilltab.skillcode', 'LEFT')
               ->join('eductab', 'p.educode1 = eductab.ecode1 and p.educode2 = eductab.ecode2', 'LEFT')
               ->join('positiontab','positiontab.postcode = selectexam.postcode','LEFT')
               ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
               ->where("selectexam.navyid ", $result['navyid']);
               if ($this->input->get('orderby') != "")
               $this->db->order_by($this->input->get('orderby'));
               $query2 = $this->db->get()->result_array();
               $query_new['select'] = array_merge($query_new['select'], $query2);
            }
         }
      }
      $query_new['count'] = $this->db->select("COUNT(navyid) AS count,")->from('selectexam')
      ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }
   public function selectexam_person() {
      $this->load->database($this->selectdb());
      $this->db->select('CONCAT (person.name ," " ,person.sname ," " ,concat(company,"/",batt)," " , person.id8 ," - ",person.navyid) AS name')
      ->from('person')->limit('4')
      ->like("person.name", $this->input->get('name'), 'after')
      ->like("person.sname", $this->input->get('sname'), 'after')
      ->like("person.id8", $this->input->get('id8'), 'after');
      $query = $this->db->get()->result();
      $this->db->close();
      return $query;
   }

   public function unittab() {
      $this->load->database($this->selectdb());
      $query_new['unit'] = $this->db->select("refnum,unitname")->from('unittab')
      ->where('movestat', '1')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }
   public function checknum(){
      $this->load->database($this->selectdb());
      $query_new['unit'] = $this->db->select("person.name,person.sname")->from('selectexam')
      ->join('person','person.navyid = selectexam.navyid','right')
      ->where(array('selectexam.postcode'=>$this->input->get('position'),
      'selectexam.unit4'=>$this->input->get('unit4'),
      'selectexam.item'=>$this->input->get('num')))
      ->get()->result();
      $this->db->close();
      return $query_new;
   }
   public function unit3_update() {
      $this->load->database($this->selectdb());
      try{
         $query = $this->db->select('selectexam.navyid,selectexam.unit4')->from('person')
         ->join('selectexam','selectexam.navyid = person.navyid')
         ->where($this->input->get('where'))
         ->order_by('selectexam.item asc')
         ->limit($this->input->get('limit'))
         ->get()->result_array();
         foreach ($query as $key => $value) {
            $this->db->set('person.unit3',$value['unit4'])->where('person.navyid',$value['navyid'])->update('person');
            echo "<pre>";
            print_r($value);
            echo "<pre>";
         }
      }
      catch (Exception $error) {
         $this->db->close();
         return $error;
      }



   }

   public function unit34_update() {
      $this->load->database($this->selectdb());
      $this->db->set('unit3', "unit4", FALSE)
      ->where('unit4', "19")
      ->where('postcode', "D")
      ->where('unit3', '0')
      ->where('statuscode', 'AA')
      ->order_by(item, "asc")
      ->limit(87)
      ->update('person');
      $this->db->close();
   }

   public function detail() {
      $select = 'person.name,person.is_request,person.sname,person.yearin,person.height,person.batt,'
      . 'person.company,person.platoon,person.pseq,person.id8,person.oldyearin,'
      . 'statustab.statuscode,statustab.title,statustab.stitle,eductab.educname,'
      . 'skilltab.skill,person.percent,u3.unitname as refnum3,selectexam.unit4,'
      . 'selectexam.postcode,selectexam.item,u1.unitname as refnum1,u2.'
      . 'unitname as refnum2,s.title as patient_status,religion.religion,'
      . 'u4.unitname as refnum4,positiontab.postname';
      $this->load->database($this->selectdb());
      $query_new = $this->db->select($select)->from('person')->where('person.navyid', $this->input->get('navyid'))
      ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
      ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
      ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
      ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
      ->join('selectexam', 'selectexam.navyid = person.navyid', 'LEFT')
      ->join('positiontab', 'selectexam.postcode = positiontab.postcode', 'LEFT')
      ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
      ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
      ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
      ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
      ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function selectexam_unittab() {
      $this->load->database($this->selectdb());
      $query_new = $this->db->select('concat(refnum," ",unitname)as name')->from('unittab')
      ->where('movestat', '1')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function selectexam_position() {
      $this->load->database($this->selectdb());
      $query_new = $this->db->select('concat(postname," ",postcode)as name,postcode as code')->from('positiontab')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function selectexam_num() {
      $this->load->database($this->selectdb());
      $select = 'IFNULL(MAX(item),0) as maxNUM';
      $query_new['num'] = $this->db->select($select)->from('selectexam')
      ->where('unit4', $this->input->get('unit4'))
      ->where('postcode', $this->input->get('postcode'))
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function selectexam_viewtable() {
      $this->load->database($this->selectdb());
      if ($this->input->get('unit4') != "") {
         $query = $this->db->select('person.navyid,concat(person.company,"/",person.batt) as belong,person.name,person.sname,person.id8,person.yearin,person.oldyearin,selectexam.item,unittab.unitname')
         ->from('selectexam')
         ->where('selectexam.postcode', $this->input->get('postcode'))
         ->where('selectexam.unit4', $this->input->get('unit4'))
         ->join('person', 'person.navyid = selectexam.navyid')
         ->join('unittab', 'unittab.refnum = selectexam.unit4')
         ->get()->result();
      } else {
         $query = $this->db->select('person.navyid,concat(person.company,"/",person.batt) as belong,person.name,person.sname,person.id8,person.yearin,person.oldyearin,selectexam.item,unittab.unitname')
         ->from('selectexam')
         ->where('selectexam.postcode', $this->input->get('postcode'))
         ->join('person', 'person.navyid = selectexam.navyid')
         ->join('unittab', 'unittab.refnum = selectexam.unit4')
         ->get()->result();
      }
      $this->db->close();
      return $query;
   }

   public function remove() {
      $this->load->database($this->selectdb());
      $this->db->where('navyid', $this->input->get('navyid'));
      $this->db->delete('selectexam');
      $this->db->close();
   }

   public function insert() {
      $this->load->database($this->selectdb());
      $query = $this->db->select('navyid')->from('selectexam')
      ->where('navyid', $this->input->get('navyid'))
      ->get()->result_array();
      $data = array('navyid' => $this->input->get('navyid'),
      'yearin' => $this->input->get('yearin')
      ,
      'unit4' => $this->input->get('unit4'),
      'postcode' => $this->input->get('postcode'),
      'item' => $this->input->get('item'),
      'flag' => 'F');

      if (sizeof($query) == 0) {
         $this->db->insert('selectexam', $data);
      } else {
         $this->db->where('navyid', $this->input->get('navyid'))
         ->update('selectexam', array('yearin' => $this->input->get('yearin'),
         'unit4' => $this->input->get('unit4'),
         'postcode' => $this->input->get('postcode'),
         'item' => $this->input->get('item'),
         'flag' => 'F'));
      }
      $this->db->close();
   }
   public function count_position() {
      $this->load->database($this->selectdb());
      $this->db->select('count(navyid) as countposition')
      ->from('selectexam')->where(array('unit4'=>$this->input->get('unit4'),'postcode'=>$this->input->get('postcode')));
      $query = $this->db->get()->result();
      $this->db->close();
      return $query;
   }
   public function count() {
      $this->load->database($this->selectdb());
      $this->db->select('unittab.refnum,unittab.unitname as unit,count(selectexam.unit4) as count,amount.count as all,count(person.unit3) as count2')
      ->from('selectexam')->where('unittab.movestat',1)
      ->join('unittab', 'unittab.refnum = selectexam.unit4', 'RIGHT')
      ->join('amount', 'amount.unit = unittab.refnum', 'LEFT')
      ->join('person', 'selectexam.navyid = person.navyid  and selectexam.unit4 = person.unit3', 'LEFT')
      ->group_by('unittab.refnum')
      ->order_by('unittab.refnum', 'asc');
      $query = $this->db->get()->result();
      $this->db->close();
      return $query;
   }

   public function countall() {
      $this->load->database($this->selectdb());
      $this->db->select('unittab.refnum,unittab.unitname as unit,count(person.unit3) as count,amount.count as all')
      ->from('person')->where('unittab.movestat', 1)
      ->join('unittab', 'unittab.refnum = person.unit3', 'RIGHT')
      ->join('amount', 'amount.unit = unittab.refnum', 'LEFT')
      ->group_by('unittab.refnum')
      ->order_by('unittab.refnum', 'asc');
      $query = $this->db->get()->result();
      $this->db->close();
      return $query;
   }

   public function amount() {
      $this->load->database($this->selectdb());
      $query_new = $this->db->select('unittab.unitname,unittab.refnum,amount.unit,amount.count,amount.yearin')->from('amount')
      ->join('unittab', 'unittab.refnum = amount.unit')
      ->get()->result();
      return $query_new;
   }

   public function saveamount() {
      $array = $this->input->get();
      $this->load->database($this->selectdb());
      for ($i = 1; $i <= 30; $i++) {
         $this->db->set('count', $array[$i])->where('unit', $i)->update('amount');
      }
      $this->db->close();
   }

}
