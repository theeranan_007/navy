<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class loginModel extends CI_Model{
	private $sess;
	public function __construct()
        {
                // Assign the CodeIgniter super-object
                $this->sess =& get_instance();
                $this->load->database('webservertest');
		
        }
	public function validate(){
			$user=$this->input->post('username');
			$pass=$this->input->post('password');
		    //echo $user;
		    //echo $pass;
		    
 			$this->db->where('username',$user);
			$this->db->where('password',$pass);
        	$query = $this->db->get('person');
        	$this->db->close();
        	//print_r( $query);

		if($query->num_rows() >0){
			 foreach ($query->result() as $row)
        		{
                $newdata = array(
                	'id' => $row->id,
                	'email'  => $row->username,
					'username'  => $row->username,
					'firstname' => $row->firstname,
					'lastname' => $row->lastname,
                                        'img' => $row->img,
                    'database' => 'navdb',
               		'is_logged_in' => 1
			);
        		}
			
			$this->session->set_userdata($newdata);
			return true;
	}
		else{
			return false;
		}
	}
	public function un_validate(){
			$this->session->sess_destroy();
	}
}