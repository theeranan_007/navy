<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class incident_model extends CI_Model {
  private $ci;
  public function __construct() {
    // Assign the CodeIgniter super-object
    $this->ci = & get_instance();
  }
  public function selectdb() {
    if($this->ci->session->userdata('database')!=null){
      return $this->ci->session->userdata('database');
    }
    else{
      return 'navdb';
    }
  }
  public function query() {
    $select = "person.statuscode,person.navyid,person.name,person.sname,CONCAT(statustab.title,'(',person.statuscode,')') as status,CONCAT_WS('/',person.company,person.batt) as belong,unittab.refnum,unittab.unitname,person.id8,person.oldyearin";
    $this->load->database($this->selectdb());
    $query_new['count']=$this->db->select("COUNT(navyid) AS count")->from("person") ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
    ->join('unittab','unittab.refnum = person.unit3','LEFT')
    ->where("person.statuscode !=","AA")
    ->order_by('person.statuscode','asc')
    ->order_by('person.batt','asc')
    ->order_by('person.company','asc')
    ->order_by('person.id8','asc')->get()->result();//
    $query_new['select']= $this->db->select($select)->from('person')->limit($this->input->get('limit'),$this->input->get('row'))
    ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
    ->join('unittab','unittab.refnum = person.unit3','LEFT')
    ->where("person.statuscode !=","AA")
    ->order_by('person.statuscode','asc')
    ->order_by('person.batt','asc')
    ->order_by('person.company','asc')
    ->order_by('person.id8','asc')

    ->get()->result();
    $this->db->close();
    return $query_new;
  }
  public function statustab(){
    $this->load->database($this->selectdb());
    $this->db->select('statustab.statuscode,statustab.title')->from('statustab')
    ->join('person','person.statuscode=statustab.statuscode','FULL')
    ->where('person.statuscode!="AA"')
    ->group_by('statustab.statuscode');
    $query = $this->db->get()->result();
    $this->db->close();
    return $query;
  }
  public function print_pdf() {
    $this->load->database($this->selectdb());
    $select="person.navyid,patient.title as patient_status,concat(person.company,'/',person.batt) as belong
    ,person.yearin,person.name,person.sname,person.id8,u1.unitname as refnum1,u2.unitname as refnum2
    ,u3.unitname as refnum3,person.percent,
    concat(ur.unitname,' ',request.askby,'(',request.num,')') as refnumur,concat(positiontab.postname,' ',
    u4.unitname,'(',selectexam.item,')') as refnum4,person.oldyearin,statustab.title as status";
    $this->db->select($select)->from('person')
    ->join('request','request.navyid = person.navyid and request.selectcode=1','LEFT')
    ->join('selectexam','selectexam.navyid = person.navyid','LEFT')
    ->join('unittab as ur','ur.refnum = request.unit','LEFT')
    ->join('positiontab','positiontab.postcode = selectexam.postcode','LEFT')
    ->join('unittab as u1','u1.refnum = person.unit1','LEFT')
    ->join('unittab as u2','u2.refnum = person.unit2','LEFT')
    ->join('unittab as u3','u3.refnum = person.unit3','LEFT')
    ->join('unittab as u4','u4.refnum = selectexam.unit4','LEFT')
    ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
    ->join('statustab patient','patient.statuscode = person.patient_status','LEFT');
    if($this->input->get('where')!=""){
      $this->db->where($this->input->get('where'));
    }
    if($this->input->get('orderby')!=""){
      $this->db->order_by($this->input->get('orderby'));
    }
    $query = $this->db->get()->result_array();
     $this->db->close();
     return $query;
  }
  public function detail() {
    $select = 'person.name,person.is_request,person.sname,person.yearin,person.height'
    . ',person.unit3,person.batt,'
    . 'person.company,person.platoon,person.pseq,person.id8,person.oldyearin,'
    . 'statustab.statuscode,statustab.title,statustab.stitle,eductab.educname,'
    . 'skilltab.skill,person.percent,u3.refnum as refnum3,u3.unitname as unit3,selectexam.unit4,'
    . 'selectexam.postcode,selectexam.item,u1.unitname as refnum1,u2.'
    . 'unitname as refnum2,s.title as patient_status,religion.religion,'
    . 'u4.unitname as refnum4,positiontab.postname';
    $this->load->database($this->selectdb());
    $query_new = $this->db->select($select)->from('person')
    ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
    ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
    ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
    ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
    ->join('selectexam', 'person.navyid = selectexam.navyid', 'LEFT')
    ->join('positiontab', 'selectexam.postcode = positiontab.postcode', 'LEFT')
    ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
    ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
    ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
    ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
    ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
    ->where('person.navyid', $this->input->get('navyid'))
    ->get()->result();
    $this->db->close();
    return $query_new;
  }
  public function unittab() {
    $this->load->database($this->selectdb());
    $query_new['unit'] = $this->db->select("refnum,unitname")->from('unittab')
    ->where('movestat', '1')
    ->get()->result();
    $this->db->close();
    return $query_new;
  }
  public function incident_table() {
    $this->load->database($this->selectdb());
    $select="person.navyid,patient.title as patient_status,person.batt,concat(person.company,'/',person.batt) as belong
    ,person.yearin,person.name,person.sname,person.id8
    ,u1.unitname as refnum1,u2.unitname as refnum2
    ,u3.unitname as refnum3,u3.refnum as unit3,person.percent,
    concat(ur.unitname,' ',request.askby,'(',request.num,')') as refnumur,
    if(person.EDUCODE1<=3,e1.EDUCNAME,e2.EDUCNAME) as eductab,
    concat(positiontab.postname,' ',
    u4.unitname,'(',selectexam.item,')') as refnum4,person.oldyearin,statustab.title as status";
    $this->db->select($select)->from('person')
    ->join('request','request.navyid = person.navyid and request.selectcode=1','LEFT')
    ->join('selectexam','selectexam.navyid = person.navyid','LEFT')
    ->join('unittab as ur','ur.refnum = request.unit','LEFT')
    ->join('positiontab','positiontab.postcode = selectexam.postcode','LEFT')
    ->join('unittab as u1','u1.refnum = person.unit1','LEFT')
    ->join('unittab as u2','u2.refnum = person.unit2','LEFT')
    ->join('unittab as u3','u3.refnum = person.unit3','LEFT')
    ->join('unittab as u4','u4.refnum = selectexam.unit4','LEFT')
    ->join('eductab as e1','e1.ecode1 = person.educode1 and e1.ecode2 = person.educode2','LEFT')
    ->join('eductab as e2','e2.ecode1 = person.educode1 and e2.ecode2 = "001"','LEFT')
    ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
    ->join('statustab patient','patient.statuscode = person.patient_status','LEFT');
    if($this->input->get('status')=="รวม")
    $this->db->where('person.statuscode != "AA"');
    else
    $this->db->where('person.statuscode',$this->input->get('status'));
    $query = $this->db->get()->result();
    $this->db->close();
    return $query;
  }
  public function unit3_update(){
    $this->load->database($this->selectdb());
    $this->db->set('unit3', $this->input->get('unit3'), FALSE)
    ->where('navyid',$this->input->get('navyid'))
    ->update('person');
    $this->db->close();
  }
  public function status(){
    $this->load->database($this->selectdb());
    $this->db->select('concat(statustab.title," (",count(person.statuscode),")")as status,statustab.statuscode')
    ->from('person')
    ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
    ->group_by('person.statuscode')
    ->where('person.statuscode != "AA"');
    $query = $this->db->get()->result();
    $this->db->select('concat("รวม"," (",count(person.statuscode),")")as status,concat("") as statuscode')
    ->from('person')
    ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
    ->where('person.statuscode != "AA"');
    $query2 = $this->db->get()->result();
    $query = array_merge($query,$query2);
    $this->db->close();
    return $query;
  }
  public function unit34_update(){
    $this->load->database($this->selectdb());
    $array = array(
      'unit4' => 19,
      'poscode' => 'D',
      'unit3' => null,
      'statuscode' => 'AA'
    );
    $this->db->set('unit3', 'unit4', FALSE)
    ->where($array)
    ->update('person');
    $this->db->close();
  }
  public function amount() {
    $this->load->database($this->selectdb());
    $query_new = $this->db->select('unittab.unitname,unittab.refnum,amount.unit,amount.count,amount.yearin')->from('amount')
    ->join('unittab','unittab.refnum = amount.unit')
    ->get()->result();
    return $query_new;
  }

  public function count(){
    $this->load->database($this->selectdb());
    $this->db->select('unit3 as unit,amount.count as all,IF(person.statuscode != "AA",count(person.unit3),0) as count')
    ->from('person')
    ->join('unittab','unittab.refnum = person.unit3 and unittab.movestat = 1','LEFT')
    ->join('amount','amount.unit = unittab.refnum','LEFT')
    ->where('person.statuscode != "AA"')
    ->group_by('unittab.refnum')
    ->order_by('unittab.refnum','asc');
    $query = $this->db->get()->result();
    $this->db->close();
    return $query;
  }
  public function countall(){
    $this->load->database($this->selectdb());
    $this->db->select('unittab.unitname as unit,count(person.unit3) as count,amount.count as all')
    ->from('person')->where('unittab.movestat',1)
    ->join('unittab','unittab.refnum = person.unit3','RIGHT')
    ->join('amount','amount.unit = unittab.refnum','LEFT')
    ->group_by('unittab.refnum')
    ->order_by('unittab.refnum','asc');
    $query = $this->db->get()->result();
    $this->db->close();
    return $query;
  }
  public function saveamount(){
    $array=$this->input->get();
    $this->load->database($this->selectdb());
    for($i=1;$i<=30;$i++){
      $this->db->set('count',$array[$i])->where('unit',$i)->update('amount');
    }
    $this->db->close();
  }
}
