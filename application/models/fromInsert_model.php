<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class fromInsert_model extends CI_Model {

    private $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();

    }
    public function person() {
        $this->load->database('test');
        $this->db->select('CONCAT (person.name ," " ,person.sname ," " ,person.address," " , person.id8 ," - ",person.navyid) AS name')
                ->from('person')->limit('4')
                ->like("person.name", $this->input->get('name'), 'after')
                ->like("person.sname", $this->input->get('sname'), 'after')
                ->like("person.id8", $this->input->get('id8'), 'after');
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }
       public function detail() {
        $this->load->database('test');
        $this->db->select('p.yearin,p.name,p.sname,p.address,p.id8,p.navyid,p.unit3,u.unitname')
                ->from('test.person p')
                ->join('navdb.unittab u','u.refnum = p.unit3','LEFT')
                ->where('p.navyid', $this->input->get('navyid'));
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }
    public function viewtable(){
        $this->load->database('test');
        $this->db->select('p.yearin,p.name,p.sname,p.address,p.id8,p.navyid,p.unit3,u.unitname')
                ->from('test.person p')
                ->join('navdb.unittab u','u.refnum = p.unit3','LEFT')
                ->like('p.yearin', $this->input->get('yearin'),'after');
                
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }
 public function save(){
        $this->load->database('test');
        
        $array = array(
            'yearin' => $this->input->get('yearin'),
            'name' => $this->input->get('name'),
            'sname' => $this->input->get('sname'),
            'address' => $this->input->get('address'),
            'id8' => $this->input->get('id8'),
            'unit3' => $this->input->get('unit3'),
            );
        if($this->input->get('navyid')==''){
            $this->db->set($array)
            ->insert('person');
        }else{
        $this->db->set($array)
                ->where('navyid', $this->input->get('navyid'))
            ->update('person');
        }
        $this->db->close();
    }
}
?>