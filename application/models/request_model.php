<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class request_model extends CI_Model {

   private $ci;

   public function __construct() {
      // Assign the CodeIgniter super-object
      $this->ci = & get_instance();

   }
   public function selectdb() {

      return 'navdb';

   }
   public function search_query() {
      $this->load->database($this->selectdb());
      $this->db->select('navyid,name,sname,batt,company,id8,oldyearin,yearin')
      ->from('person')
      ->like('name', $this->input->get('name'), 'after')
      ->like('sname', $this->input->get('sname'), 'after')
      ->like('id8', $this->input->get('id8'), 'after')
      ->like('yearin', $this->input->get('yearin'), 'after');
      $query = $this->db->get()->result();
      $this->db->close();
      return $query;
   }
 public function checknum() {
      $this->load->database($this->selectdb());
      $this->db->select('person.name,person.sname')
      ->from('person')
      ->join('request','request.navyid=person.navyid','right')
      ->like('request.askby',$this->input->get('askby'),'after')
      ->where('request.num',$this->input->get('num'));
      $query = $this->db->get()->result();
      $this->db->close();
      return $query;
   }
   public function request_person() {
      $this->load->database($this->selectdb());
      $this->db->select('person.name ,person.sname , person.id8 ,person.navyid')
      ->from('person')->limit('10')
      ->like("person.name",$this->input->get('name'),'after')
      ->like("person.sname",$this->input->get('sname'),'after')
      ->like("person.id8",$this->input->get('id8'),'after');
      $query = $this->db->get()->result();
      $this->db->close();
      return $query;
   }

   public function print_request() {
      $select = 'if(request.unit = p.unit3,1,0) as up,request.navyid,request.yearin,request.name,request.sname,request.askby,request.unit,request.num,request.remark,request.remark2,request.selectcode,request.selectid,u3.unitname as refnum3';
      $this->load->database($this->selectdb());
      $query_new['select'] = $this->db->select($select)->from('request')
      ->join('unittab as u3', 'u3.refnum = request.unit', 'LEFT')
      ->join('person as p', 'request.navyid = p.navyid', "LEFT")
      ->order_by('request.name,request.sname,request.selectid,request.askby')
      ->get()->result();
      $query_new['count'] = $this->db->select("COUNT(navyid) AS count")->from('request')
      ->join('unittab as u3', 'u3.refnum = request.unit', 'LEFT')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function print_request_pdf() {
        $query3 = null;
        $query1 = array();
      $select = 'request.navyid,'
      . 'request.yearin,request.name,request.sname,request.askby,p.percent,'
      . 'concat(p.company,"/",p.batt) as belong,request.unit,request.num'
      . ',request.remark,request.remark2,request.selectcode'
      . ',u3.unitname as refnum3,eductab.educname,selectexam.unit4,p.unit3,'
      . 'selectexam.postcode,selectexam.item,request.selectid,statustab.title'
      . ',if(skilltab.skill="ไม่กำหนด","",skilltab.skill) as skill,'
      . 'u1.unitname as refnum1,u2.unitname as refnum2,u3.unitname as refnum3,ur.unitname as refnumur,u4.unitname as refnum4,positiontab.postname,p.id8,p.yearin';
      $this->load->database($this->selectdb());
      $this->db->select('request.navyid')->from('request')->join('person','person.navyid = request.navyid',"LEFT");
      if($this->input->get('where')!="")
      $this->db->where($this->input->get('where'));
      if ($this->input->get('orderby') != "")
      $this->db->order_by($this->input->get('orderby'));

    if($this->input->get('having')!=""){
      $this->db->having($this->input->get('having'));
      $query3 = $this->db->group_by('request.navyid')->get()->result_array();
        if($query3 != null){
            foreach($query3 as $key=>$result){
                 $this->db->select('request.navyid')->from('request')->join('person','person.navyid = request.navyid',"LEFT");
                 $this->db->where('request.navyid',$result['navyid']);
                 $tmp = $this->db->get()->result_array();
                 if($tmp!=null)
                 array_push($query1,$tmp[0]);
            }
        }
    }
    else{
      $query3 = $this->db->group_by('request.navyid')->get()->result_array();
        $query1 = $query3;
    }
      /* if($this->input->get('askby')!="")
      $this->db->like("askby", $this->input->get('askby'), "after");
      if($this->input->get('unit')!="")
      $this->db->where("unit", $this->input->get('unit')); */

      $query_new['select'] = null;
      if ($query1 != null) {
         foreach ($query1 as $key => $result) {
            //echo $result['navyid'];
            if ($query_new['select'] == null) {
               $this->db->select($select)->from('request')->limit('10')
               ->join('unittab as ur', 'ur.refnum = request.unit', 'LEFT')
               ->join('person as p', 'request.navyid = p.navyid', "LEFT")
               ->join('unittab as u1', 'u1.refnum = p.unit1', 'LEFT')
               ->join('unittab as u2', 'u2.refnum = p.unit2', 'LEFT')
               ->join('unittab as u3', 'u3.refnum = p.unit3', 'LEFT')
               ->join('statustab','p.statuscode = statustab.statuscode','LEFT')
               ->join('skilltab', 'p.skillcode = skilltab.skillcode', 'LEFT')
               ->join('eductab', 'p.educode1 = eductab.ecode1 and p.educode2 = eductab.ecode2', 'LEFT')
               ->join('selectexam', 'selectexam.navyid = request.navyid', 'LEFT')
               ->join('positiontab','positiontab.postcode = selectexam.postcode','LEFT')
               ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
               ->where("request.navyid ", $result['navyid']);
               if ($this->input->get('orderby') != "")
               $this->db->order_by($this->input->get('orderby'));
               $query_new['select'] = $this->db->get()->result_array();
            }
            else {
               $this->db->select($select)->from('request')->limit('10')
               ->join('unittab as ur', 'ur.refnum = request.unit', 'LEFT')
               ->join('person as p', 'request.navyid = p.navyid', "LEFT")
               ->join('unittab as u1', 'u1.refnum = p.unit1', 'LEFT')
               ->join('unittab as u2', 'u2.refnum = p.unit2', 'LEFT')
               ->join('unittab as u3', 'u3.refnum = p.unit3', 'LEFT')
               ->join('statustab','p.statuscode = statustab.statuscode','LEFT')
               ->join('skilltab', 'p.skillcode = skilltab.skillcode', 'LEFT')
               ->join('eductab', 'p.educode1 = eductab.ecode1 and p.educode2 = eductab.ecode2', 'LEFT')
               ->join('selectexam', 'selectexam.unit4 = p.unit4  and selectexam.navyid = request.navyid', 'LEFT')
               ->join('positiontab','positiontab.postcode = selectexam.postcode','LEFT')
               ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
               ->where("request.navyid ", $result['navyid']);
               if ($this->input->get('orderby') != "")
               $this->db->order_by($this->input->get('orderby'));
               $query2 = $this->db->get()->result_array();
               $query_new['select'] = array_merge($query_new['select'], $query2);
            }
         }
      }
      $query_new['count'] = $this->db->select("COUNT(navyid) AS count,")->from('request')
      ->join('unittab as u3', 'u3.refnum = request.unit', 'LEFT')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function table_request() {
      $select = 'if(request.unit = p.unit3,1,0) as up,request.navyid,'
      . 'request.yearin,request.name,request.sname,request.askby,'
      . 'concat(p.company,"/",p.batt) as belong,request.unit,request.num'
      . ',request.remark,request.remark2,request.selectcode'
      . ',u3.unitname as refnum3,statustab.title,selectexam.unit4,'
      . 'selectexam.postcode,selectexam.item'
      . ',schooltab.schoolname,u4.unitname as refnum4,positiontab.postname,p.id8,p.yearin,request.selectid';
      $this->load->database($this->selectdb());
      $query_new['select'] = $this->db->select($select)->from('request')->limit($this->input->get('limit'), $this->input->get('row'))
      ->join('unittab as u3', 'u3.refnum = request.unit', 'LEFT')
      ->join('person as p', 'request.navyid = p.navyid', "LEFT")
      ->join('statustab', 'p.statuscode = statustab.statuscode', 'LEFT')
      ->join('schooltab', 'schooltab.campuscode = substring(p.schoolcode,7,8) and schooltab.ctown =  substring(p.schoolcode,1,6)', 'LEFT')
      ->join('selectexam', 'selectexam.navyid = request.navyid', 'LEFT')
      ->join('positiontab','positiontab.postcode = selectexam.postcode','LEFT')
      ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
      ->order_by('request.name,request.sname,request.selectid,request.askby')
      ->get()->result();
      $query_new['count'] = $this->db->select("COUNT(navyid) AS count")->from('request')
      ->join('unittab as u3', 'u3.refnum = request.unit', 'LEFT')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function detail() {
      $select = 'person.name,person.is_request,person.sname,person.yearin,person.height'
      . ',person.unit3,person.batt,'
      . 'person.company,person.platoon,person.pseq,person.id8,person.oldyearin,'
      . 'statustab.statuscode,statustab.title,statustab.stitle,eductab.educname,'
      . 'skilltab.skill,person.percent,u3.unitname as refnum3,selectexam.unit4,'
      . 'selectexam.postcode,selectexam.item,u1.unitname as refnum1,u2.'
      . 'unitname as refnum2,s.title as patient_status,religion.religion,'
      . 'u4.unitname as refnum4,positiontab.postname';
      $this->load->database($this->selectdb());
      $query_new = $this->db->select($select)->from('person')
      ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
      ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
      ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
      ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
      ->join('selectexam', 'person.navyid = selectexam.navyid', 'LEFT')
      ->join('positiontab', 'selectexam.postcode = positiontab.postcode', 'LEFT')
      ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
      ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
      ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
      ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
      ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
      ->where('person.navyid', $this->input->get('navyid'))
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function request() {
      $select = 'request.yearin,unittab.refnum,unittab.unitname,request.askby,request.num,request.remark,request.remark2,request.selectcode,request.selectid';
      $this->load->database($this->selectdb());
      $query_new = $this->db->select($select)->from('request')
      ->join('unittab', 'unittab.refnum = request.unit', 'LEFT')
      ->where('request.navyid', $this->input->get('navyid'))
      ->order_by('substr(askby,1,2) asc,num asc')
      ->get()->result();
      $this->db->close();
      return $query_new;
   }

   public function unit3_update(){
      $this->load->database($this->selectdb());
      $this->db->set('unit3',$this->input->get('unit3'))
      ->where('navyid',$this->input->get('navyid'))
      ->update('person');
      $this->db->close();
   }

   public function request_update() {
      $this->load->database($this->selectdb());
      $this->db->set('selectcode', 0, FALSE)
      ->where('request.navyid', $this->input->get('navyid'))
      ->update('request');
      $this->db->set('selectcode', 1, FALSE)
      ->where('request.navyid = ' . $this->input->get('navyid'))
      ->where('request.selectid', $this->input->get('selectid'))
      ->update('request');
      $select = 'unit';
      $array = array(
         'navyid' => $this->input->get('navyid'),
         'selectid' => $this->input->get('selectid')
      );
      $result = $this->db->select($select)->from('request')
      ->where($array)
      ->get()->result_array();
      $this->db->set(array('unit3' => $result[0]['unit']));
      $this->db->where(array('navyid' => $this->input->get('navyid')))->update('person');
      $this->db->close();
   }

   public function request_from() {
      $select = 'IFNULL(MAX(NUM),0) as maxNUM';
      $this->load->database($this->selectdb());
      $query_new['unit'] = $this->db->select("refnum,unitname")->from('unittab')
      ->where('movestat', '1')
      ->get()->result();

      $query_new['num'] = $this->db->select($select)->from('request')
      ->like('askby', $this->input->get('input_askby'), 'after')
      ->where('yearin', $this->input->get('yearin'))
      ->get()->result();
      $this->db->close();
      return $query_new;
   }
   public function countunittab() {
       $respond['query']='';
       $respond['error']='';
       try {
           $this->load->database($this->selectdb());
           $this->db->select('unitname,refnum')->from('unittab')->where('movestat',1);
           $respond['query'] = $this->db->get()->result();
       } catch (Exception $error) {
           $respond['error']=$error;
       }
       return $respond;
   }
   /////////////////////////////////////// query all //////////////////////////////
public function countamount() {
     $respond['query']='';
     $respond['error']='';
     try {
         $this->load->database($this->selectdb());
         $this->db->select('count,unit')->from('amount');
         $respond['query'] = $this->db->get()->result();
     } catch (Exception $error) {
         $respond['error']=$error;
     }
     return $respond;
}
/////////////////////////////////////// query unit3 //////////////////////////////
public function countrequest() {
 $respond['query']='';
 $respond['error']='';
 try {
      $this->load->database($this->selectdb());
      $this->db->select('unittab.REFNUM as unit,count(person.navyid) as count')->from('unittab')
               ->join('person','unittab.refnum = person.unit3','left')
              ->join('request','request.navyid = person.navyid and request.selectcode = 1 and request.unit = person.unit3','left')
              ->where('unittab.movestat = 1')
              ->group_by('unittab.refnum')->order_by('unittab.refnum asc');
      $respond['query'] = $this->db->get()->result();
 } catch (Exception $error) {
  $respond['error']=$error;
 }
 return $respond;
}
/////////////////////////////////// countrequest /////////////////////////
   public function request_unittab() {
      $this->load->database($this->selectdb());
      $query_new = $this->db->select('concat(refnum," ",unitname)as name,refnum,unitname')->from('unittab')
      ->where('movestat', '1')
      ->get()->result();
      return $query_new;
   }
   public function amount() {
      $this->load->database($this->selectdb());
      $query_new = $this->db->select('unittab.unitname,unittab.refnum,amount.unit,amount.count,amount.yearin')->from('amount')
      ->join('unittab','unittab.refnum = amount.unit')
      ->get()->result();
      return $query_new;
   }

   public function request_add() {
      $this->load->database($this->selectdb());
      $query = $this->db->select('navyid,selectid,unit')->from('request')->where('navyid', $this->input->get('navyid'))->order_by('substr(askby,1,2) asc,num asc')->get()->result_array();
      $select = 'IFNULL(MAX(selectid),0) as id';
      $selectid = $this->db->select($select)->from('request')->get()->result_array();
      if ($query != null) {
         $array = array(
            'navyid' => $this->input->get('navyid')
         );

         $count = $this->db->select('COUNT(navyid) as c')->from('request')->where($array)->get()->result_array();
         if ($count[0]['c'] >= 5)
         return 0;
         $this->db->where($array)
         ->update('request', array('selectcode' => "0"));
         $data = array(
            'yearin' => $this->input->get('yearin'),
            'navyid' => $this->input->get('navyid'),
            'id8' => $this->input->get('id8'),
            'name' => $this->input->get('name'),
            'sname' => $this->input->get('sname'),
            'unit' => $this->input->get('unit'),
            'askby' => $this->input->get('askby'),
            'num' => $this->input->get('num'),
            'remark' => $this->input->get('remark'),
            'remark2' => $this->input->get('remark2'),
            'upddate' => $this->input->get('update'),
            'updby' => $this->ci->session->userdata("firstname"),
            'updtime' => $this->input->get('updtime'),
            'updatecount' => '0',
            'selectcode' => '0',
            'selectid' => $selectid[0]['id'] + 1
         );
         $this->db->insert('request', $data);
         $query = $this->db->select('navyid,selectid,unit')->from('request')->where('navyid', $this->input->get('navyid'))->order_by('substr(askby,1,2) asc,num asc')->limit(1)->get()->result_array();
         if($query != null){
            $this->db->set('selectcode',0)->where(array(
                                       'navyid'=>$query[0]['navyid'])
                                       )->update('request');
         $this->db->set('selectcode',1)->where(array(
                                    'navyid'=>$query[0]['navyid'],
                                    'selectid'=>$query[0]['selectid'])
                                    )->update('request');
            $this->db->set(array('unit3' => $query[0]['unit']));
            $this->db->where(array('navyid' =>$query[0]['navyid']))->update('person');
         }else{
            $this->db->set(array('unit3' => '0'));
            $this->db->where(array('navyid' => $this->input->get('navyid')))->update('person');
         }
         } else {
            $data = array(
               'yearin' => $this->input->get('yearin'),
               'navyid' => $this->input->get('navyid'),
               'id8' => $this->input->get('id8'),
               'name' => $this->input->get('name'),
               'sname' => $this->input->get('sname'),
               'unit' => $this->input->get('unit'),
               'askby' => $this->input->get('askby'),
               'num' => $this->input->get('num'),
               'remark' => $this->input->get('remark'),
               'remark2' => $this->input->get('remark2'),
               'upddate' => $this->input->get('update'),
               'updby' => $this->ci->session->userdata("firstname"),
               'updtime' => $this->input->get('updtime'),
               'updatecount' => '0',
               'selectcode' => '1',
               'selectid' => $selectid[0]['id'] + 1
            );
            $this->db->insert('request', $data);
            $this->db->set(array('unit3' => $this->input->get('unit')));
            $this->db->where(array('navyid' => $this->input->get('navyid')))->update('person');
         }

         $this->db->close();

         // Produces: INSERT INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
      }
      public function request_edit() {
         $this->load->database($this->selectdb());
         $where = array('navyid'=>$this->input->get('navyid'),
         'selectid'=>$this->input->get('selectid'));
         $data = array(
            'unit' => $this->input->get('unit'),
            'askby' => $this->input->get('askby'),
            'num' => $this->input->get('num'),
            'remark' => $this->input->get('remark'),
            'remark2' => $this->input->get('remark2'),
            'upddate' => $this->input->get('update'),
            'updby' => $this->ci->session->userdata("firstname"),
            'updtime' => $this->input->get('updtime'),
            'updatecount' => '0',
            'selectcode' => '0'
         );
         $this->db->set($data)->where($where)->update('request');
         $query = $this->db->select('navyid,selectid,unit')->from('request')->where('navyid', $this->input->get('navyid'))->order_by('substr(askby,1,2) asc,num asc')->limit(1)->get()->result_array();
         if($query != null){
            $this->db->set('selectcode',0)->where(array(
                                       'navyid'=>$query[0]['navyid'])
                                       )->update('request');
         $this->db->set('selectcode',1)->where(array(
                                    'navyid'=>$query[0]['navyid'],
                                    'selectid'=>$query[0]['selectid'])
                                    )->update('request');
            $this->db->set(array('unit3' => $query[0]['unit']));
            $this->db->where(array('navyid' =>$query[0]['navyid']))->update('person');
         }
         $this->db->close();
      }
      public function request_delete() {

         $this->load->database($this->selectdb());
         $array = array(
            'navyid' => $this->input->get('navyid'),
            'selectid' => $this->input->get('selectid')
         );
         $this->db->where($array)
         ->delete('request');
         $query = $this->db->select('navyid,selectid,unit')->from('request')->where('navyid', $this->input->get('navyid'))->order_by('substr(askby,1,2) asc,num asc')->limit(1)->get()->result_array();
         if($query!=null){
         $this->db->set('selectcode',0)->where(array(
                                    'navyid'=>$query[0]['navyid'])
                                    )->update('request');
         $this->db->set('selectcode',1)
         ->where(array(
            'navyid'=>$query[0]['navyid'],
            'selectid'=>$query[0]['selectid'])
            )->update('request');

         $this->db->set(array('unit3' => $query[0]['unit']))->where(array('navyid' => $this->input->get('navyid')))->update('person');
      }
      else{
         $this->db->set(array('unit3' => 0))->where(array('navyid' => $this->input->get('navyid')))->update('person');

      }

         $this->db->close();
         // Produces: INSERT INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
      }

      public function request_save() {
         $this->load->database($this->selectdb());
         $data = array(
            'unit3' => $this->input->get('unit3')
         );
         $this->db->where("navyid", $this->input->get('navyid'))
         ->update('person', $data);
         $this->db->close();
      }

      public function test() {
         $this->load->database($this->selectdb());
         $query = $this->db->select('navyid')->from('request')
         ->like("unit", $this->input->get('unit'), "after")
         ->like("askby", $this->input->get('askby'), "after")
         ->get()->result_array();
         $a1 = 0;
         if ($query != null) {
            foreach ($query as $key => $result) {
               //echo $result['navyid'];
               if ($a1 == null) {

                  $this->db->select('')->from('request')
                  ->where("navyid ", $result['navyid']);
                  if ($this->input->get('orderby') != "")
                  $this->db->order_by($this->input->get('orderby'));
                  $a1 = $this->db->get()->result_array();
               }
               else {
                  $this->db->select('')->from('request')
                  ->where("navyid ", $result['navyid']);
                  if ($this->input->get('orderby') != "")
                  $this->db->order_by($this->input->get('orderby'));
                  $query2 = $this->db->get()->result_array();
                  $a1 = array_merge($a1, $query2);
               }
            }
         }

         $this->db->close();
         return $a1;
      }
      public function count(){
         $this->load->database($this->selectdb());
         $this->db->select('unittab.refnum,unittab.unitname as unit,count(person.navyid) as count,amount.count as all')
         ->from('request')->where('unittab.movestat',1)
         ->join('unittab','unittab.refnum = request.unit','RIGHT')
         ->join('amount','amount.unit = unittab.refnum','LEFT')
         ->join('person','request.unit = person.unit3 and request.navyid = person.navyid','LEFT')
         ->group_by('unittab.refnum')
         ->order_by('unittab.refnum','asc');
         $query1 = $this->db->get()->result_array();
         $this->db->select('unittab.refnum,count(person.unit3) as count2')
         ->from('person')->where('unittab.movestat',1)
         ->join('unittab','unittab.refnum = person.unit3','RIGHT')
         ->join('amount','amount.unit = unittab.refnum','LEFT')
         ->group_by('unittab.refnum')
         ->order_by('unittab.refnum','asc');
         $query2 = $this->db->get()->result_array();
         foreach ($query2 as $key => $value) {
            if($query1[$key]['refnum']=$value['refnum'])
            $query1[$key]['count2'] = $value['count2'];

         }
         $this->db->close();
         return (object)$query1;
      }
      public function countall(){
         $this->load->database($this->selectdb());
         $this->db->select('unittab.refnum,unittab.unitname as unit,count(person.unit3) as count,amount.count as all')
         ->from('person')->where('unittab.movestat',1)
         ->join('unittab','unittab.refnum = person.unit3','RIGHT')
         ->join('amount','amount.unit = unittab.refnum','LEFT')
         ->group_by('unittab.refnum')
         ->order_by('unittab.refnum','asc');
         $query = $this->db->get()->result();
         $this->db->close();
         return $query;
      }
      public function saveamount(){
         $array=$this->input->get();
         $this->load->database($this->selectdb());
         for($i=1;$i<=30;$i++){
            $this->db->set('count',$array[$i])->where('unit',$i)->update('amount');
         }
         $this->db->close();
      }

   }
