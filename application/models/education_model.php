<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class education_model extends CI_Model {

    private $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }

     public function selectdb() {
         if($this->ci->session->userdata('database')!=null){
             return $this->ci->session->userdata('database');
         }
         else{
             return 'navdb';
         }
    }

    public function query() {
        $select = "person.navyid,person.name,person.sname,CONCAT(statustab.title,'(',person.statuscode,')') as status,CONCAT_WS('/',person.company,person.batt) as belong,unittab.refnum,unittab.unitname,person.id8,person.oldyearin";
        $this->load->database($this->selectdb());
        $query_new['count'] = $this->db->select("COUNT(navyid) AS count")->from("person")->get()->result(); //
        $query_new['select'] = $this->db->select($select)->from('person')->limit($this->input->get('limit'), $this->input->get('row'))
                        ->join('statustab', 'statustab.statuscode = person.statuscode', 'LEFT')
                        ->join('unittab', 'unittab.refnum = person.unit3', 'LEFT')
                        ->where("person.statuscode !=", "AA")
                        ->order_by('person.statuscode', 'asc')
                        ->order_by('person.batt', 'asc')
                        ->order_by('person.company', 'asc')
                        ->order_by('person.id8', 'asc')
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

    public function eductab() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('eductab')
                    ->where('eductab.ecode2', '001')
                    ->where('eductab.ecode1>3')
                    ->order_by('eductab.ecode1 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
    public function save() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = array(
            'yearin'=>$this->input->get('yearin'),
            'unit'=> $this->input->get('unit'),
            'general'=> $this->input->get('general'),
            'ecode1'=> $this->input->get('ecode1'),
            'ecode2'=> $this->input->get('ecode2'),
            'skillcode'=> $this->input->get('skill'),
            'total' => $this->input->get('total'));
        try {
            $this->load->database($this->selectdb());
            $this->db->set($str);
            if($this->input->get('seq')=='')   {
                $this->db->insert('hisrequire');
            }else{
                $this->db->where('seq',$this->input->get('seq'))->update('hisrequire');
            }
            $respond['query'] = '';
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
     public function viewtable() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str='';
        if($this->input->get('gen')=='T'){
          $str='person.educode1<=3';
        }
        else if($this->input->get('skill')!=''){
          $str = array( 'person.skillcode'=>$this->input->get('skill'));
        }
        else{
          $str = array(
              'person.educode1'=>$this->input->get('ecode1'),
              'person.educode2'=> $this->input->get('ecode2'));
        }

        try {
            $this->load->database($this->selectdb());
            $this->db->select('person.navyid,person.percent,concat(person.name," ",person.sname) as name,u1.unitname unit1'
                    . ',u2.unitname unit2,u3.unitname unit3,u4.unitname unit4,u.unitname unit'
                    . ',statustab.title,eductab.educname,skilltab.skill')->from('person')
                    ->join('request r','r.navyid = person.navyid','left')
                    ->join('selectexam s','s.navyid = person.navyid','left')
                    ->join('statustab','statustab.statuscode = person.statuscode','left')
                    ->join('unittab u1','u1.refnum = person.unit1','left')
                    ->join('unittab u2','u2.refnum = person.unit2','left')
                    ->join('unittab u3','u3.refnum = person.unit3','left')
                    ->join('unittab u4','u4.refnum = s.unit4','left')
                    ->join('unittab u','u.refnum = r.unit','left')
                    ->join('eductab','eductab.ecode1 = person.educode1 and eductab.ecode2 = person.educode2','left')
                    ->join('skilltab','skilltab.skillcode = person.skillcode','left')
                    ->where($str);
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
public function remove_hisrequire() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->where('SEQ', $this->input->get('seq'))->delete('hisrequire');
            $respond['query'] = '';
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
    public function count_hisrequire() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('SUM(hisrequire.total) as count')->from('hisrequire')
                    ->where('hisrequire.unit', $this->input->get('unit'))
                    ->where('hisrequire.yearin', $this->input->get('yearin'));
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
    public function edit_hisrequire() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('hisrequire.*,e1.educname as eductab,e2.educname as eductab2,skilltab.skill'
                    . ',u.refnum,u.unitname')
                    ->from('hisrequire')
                    ->join('unittab u','u.refnum = hisrequire.unit')
                    ->join('eductab as e1','e1.ecode1 = hisrequire.ecode1 and e1.ecode2 = "001"','left')
                    ->join('eductab as e2','e2.ecode1 = hisrequire.ecode1 and e2.ecode2 = hisrequire.ecode2','left')
                    ->join('skilltab','skilltab.skillcode = hisrequire.skillcode','left')
                    ->where('SEQ', $this->input->get('seq'));
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }

    public function skilltab() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('skilltab')
            ->group_by('skilltab.skillcode');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
    public function skilltab2() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = 'concat(skilltab.skill,"(",count(p.skillcode),")") as skill,skilltab.skillcode';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('skilltab')
            ->join('person p','p.skillcode = skilltab.skillcode','left')
            ->group_by('p.skillcode');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }

    public function eductab2() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('eductab')
                    ->where('eductab.ecode2!=001 and eductab.ecode1=' . $this->input->get('ecode1'))
                    ->order_by('eductab.ecode2 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
public function eductab_p2() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('eductab')
                    ->where('eductab.ecode2', '001')
                    ->where('eductab.ecode1>3')
                    ->order_by('eductab.ecode1 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
    public function eductab2_p2() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = 'concat(e.educname,"(",count(p.navyid),")") as educname,e.ecode2';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('eductab e')
                    ->join('person p','p.educode1=e.ecode1 and p.educode2=e.ecode2','left')
                    ->where('e.ecode2!=001 and e.ecode1=' . $this->input->get('ecode1'))
                    ->group_by('e.educname')
                    ->order_by('e.ecode2 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
    public function education_table_new() {
        $this->load->database($this->selectdb());
        $str = 'education.navyid,person.name,person.sname,u4.unitname as refnum4,positiontab.postname,education.item,statustab.title,u3.unitname as refnum3';
        $this->db->select($str)->from('education')
                ->join('person', 'person.navyid = education.navyid', 'LEFT')
                ->join('unittab as u4', 'u4.refnum = education.unit4', 'LEFT')
                ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                ->join('positiontab', 'education.postcode = positiontab.postcode', 'LEFT')
                ->join('statustab', 'statustab.statuscode = person.statuscode', 'LEFT')
                ->where($this->input->get('sql'))
                ->order_by('education.postcode asc,education.item asc');
        $query = $this->db->get()->result();
        return $query;
    }

    public function education_person() {
        $this->load->database($this->selectdb());
        $this->db->select('CONCAT (person.name ," " ,person.sname ," " ,concat(company,"/",batt)," " , person.id8 ," - ",person.navyid) AS name')
                ->from('person')->limit('4')
                ->like("person.name", $this->input->get('name'), 'after')
                ->like("person.sname", $this->input->get('sname'), 'after')
                ->like("person.id8", $this->input->get('id8'), 'after');
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }

    public function unittab() {
        $this->load->database($this->selectdb());
        $query_new['unit'] = $this->db->select("refnum,unitname")->from('unittab')
                        ->where('movestat', '1')
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

    public function unit3_update() {
        $this->load->database($this->selectdb());
        $this->db->set('unit3', $this->input->get('unit3'), FALSE)
                ->where('navyid', $this->input->get('navyid'))
                ->update('person');
        $this->db->close();
    }

    public function unit34_update() {
        $this->load->database($this->selectdb());
        $this->db->set('unit3', "unit4", FALSE)
                ->where('unit4', "19")
                ->where('postcode', "D")
                ->where('unit3', '0')
                ->where('statuscode', 'AA')
                ->order_by(item, "asc")
                ->limit(87)
                ->update('person');
        $this->db->close();
    }

    public function detail() {
        $select = 'person.name,person.is_request,person.sname,person.yearin,person.height,person.batt,'
                . 'person.company,person.platoon,person.pseq,person.id8,person.oldyearin,'
                . 'statustab.statuscode,statustab.title,statustab.stitle,eductab.educname,'
                . 'skilltab.skill,person.percent,u3.unitname as refnum3,education.unit4,'
                . 'education.postcode,education.item,u1.unitname as refnum1,u2.'
                . 'unitname as refnum2,s.title as patient_status,religion.religion,'
                . 'u4.unitname as refnum4,positiontab.postname';
        $this->load->database($this->selectdb());
        $query_new = $this->db->select($select)->from('person')->where('person.navyid', $this->input->get('navyid'))
                        ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
                        ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
                        ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
                        ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                        ->join('education', 'education.navyid = person.navyid', 'LEFT')
                        ->join('positiontab', 'education.postcode = positiontab.postcode', 'LEFT')
                        ->join('unittab as u4', 'u4.refnum = education.unit4', 'LEFT')
                        ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
                        ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
                        ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
                        ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

    public function education_unittab() {
        $this->load->database($this->selectdb());
        $query_new = $this->db->select('concat(refnum," ",unitname)as name')->from('unittab')
                        ->where('movestat', '1')
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

    public function education_position() {
        $this->load->database($this->selectdb());
        $query_new = $this->db->select('concat(postname," ",postcode)as name,postcode as code')->from('positiontab')
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

    public function education_num() {
        $this->load->database($this->selectdb());
        $select = 'IFNULL(MAX(item),0) as maxNUM';
        $query_new['num'] = $this->db->select($select)->from('education')
                        ->where('unit4', $this->input->get('unit4'))
                        ->where('postcode', $this->input->get('postcode'))
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

    public function education_viewtable() {
        $this->load->database($this->selectdb());
        $this->db->select('hisrequire.*,unittab.unitname,eductab.educname,,skilltab.skill')
                ->from('hisrequire')
                ->like('hisrequire.yearin', $this->input->get('yearin'), 'after');
        if ($this->input->get('unit3') != 0) {
            $this->db->where('hisrequire.unit', $this->input->get('unit3'));
        }
        $query = $this->db->join('unittab', 'unittab.refnum = hisrequire.unit', 'left')
                ->join('skilltab', 'skilltab.skillcode = hisrequire.skillcode', 'left')
                ->join('eductab', 'eductab.ecode1 = hisrequire.ecode1 and eductab.ecode2 = hisrequire.ecode2', 'left')
                ->get()->result();
        $this->db->close();
        return $query;
    }

    public function remove() {
        $this->load->database($this->selectdb());
        $this->db->where('navyid', $this->input->get('navyid'));
        $this->db->delete('education');
        $this->db->close();
    }

    public function insert() {
        $this->load->database($this->selectdb());
        $query = $this->db->select('navyid')->from('education')
                        ->where('navyid', $this->input->get('navyid'))
                        ->get()->result_array();
        $data = array('navyid' => $this->input->get('navyid'),
            'yearin' => $this->input->get('yearin')
            ,
            'unit4' => $this->input->get('unit4'),
            'postcode' => $this->input->get('postcode'),
            'item' => $this->input->get('item'),
            'flag' => 'F');

        if (sizeof($query) == 0) {
            $this->db->insert('education', $data);
        } else {
            $this->db->where('navyid', $this->input->get('navyid'))
                    ->update('education', array('yearin' => $this->input->get('yearin'),
                        'unit4' => $this->input->get('unit4'),
                        'postcode' => $this->input->get('postcode'),
                        'item' => $this->input->get('item'),
                        'flag' => 'F'));
        }
        $this->db->close();
    }

    public function count() {
        $this->load->database($this->selectdb());
        $this->db->select('unittab.unitname as unit,count(education.unit4) as count,amount.count as all,count(person.unit3) as count2')
                ->from('education')->where('unittab.movestat', 1)
                ->join('unittab', 'unittab.refnum = education.unit4', 'RIGHT')
                ->join('amount', 'amount.unit = unittab.refnum', 'LEFT')
                ->join('person', 'education.navyid = person.navyid  and education.unit4 = person.unit3', 'LEFT')
                ->group_by('unittab.refnum')
                ->order_by('unittab.refnum', 'asc');
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }

    public function countall() {
        $this->load->database($this->selectdb());
        $this->db->select('unittab.unitname as unit,count(person.unit3) as count,amount.count as all')
                ->from('person')->where('unittab.movestat', 1)
                ->join('unittab', 'unittab.refnum = person.unit3', 'RIGHT')
                ->join('amount', 'amount.unit = unittab.refnum', 'LEFT')
                ->group_by('unittab.refnum')
                ->order_by('unittab.refnum', 'asc');
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }

    public function amount() {
        $this->load->database($this->selectdb());
        $query_new = $this->db->select('unittab.unitname,unittab.refnum,amount.unit,amount.count,amount.yearin')->from('amount')
                        ->join('unittab', 'unittab.refnum = amount.unit')
                        ->get()->result();
        return $query_new;
    }

    public function saveamount() {
        $array = $this->input->get();
        $this->load->database($this->selectdb());
        for ($i = 1; $i <= 30; $i++) {
            $this->db->set('count', $array[$i])->where('unit', $i)->update('amount');
        }
        $this->db->close();
    }

}
