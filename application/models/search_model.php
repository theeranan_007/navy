<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class search_model extends CI_Model {

    private $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }
   public function selectdb() {
         if($this->ci->session->userdata('database')!=null){
             return $this->ci->session->userdata('database');
         }
         else{
             return 'navdb';
         }
    }
    public function search_query() {
        $query = array();
        $like = array();

        //print_r($like);

        $this->load->database($this->selectdb());
        $this->db->select('navyid,name,sname,batt,company,id8,oldyearin,yearin')
                ->from('person');
        if ($this->input->get('towncode') != "")
            $this->db->like('towncode', $this->input->get('towncode'), "after");
        if ($this->input->get('runcode') != "")
            $this->db->where('runcode', $this->input->get('runcode'));
        if ($this->input->get('id13') != "")
            $this->db->where('id13', $this->input->get('id13'));
        if ($this->input->get('name') != "")
            $this->db->like('name', $this->input->get('name'), "after");
        if ($this->input->get('sname') != "")
            $this->db->like('sname', $this->input->get('sname'), "after");
        if ($this->input->get('id8') != "")
            $this->db->like('id8', $this->input->get('id8'), "after");
        if ($this->input->get('yearin') != "")
            $this->db->like('yearin', $this->input->get('yearin'), "after");

        $query = $this->db->get()->result();
        $this->db->close();
        $this->load->database('navdb_all');
        $this->db->select('navyid,name,sname,batt,company,id8,oldyearin,yearin')
                ->from('person');
        if ($this->input->get('towncode') != "")
            $this->db->like('towncode', $this->input->get('towncode'), "after");
        if ($this->input->get('runcode') != "")
            $this->db->where('runcode', $this->input->get('runcode'));
        if ($this->input->get('id13') != "")
            $this->db->where('id13', $this->input->get('id13'));
        if ($this->input->get('name') != "")
            $this->db->like('name', $this->input->get('name'), "after");
        if ($this->input->get('sname') != "")
            $this->db->like('sname', $this->input->get('sname'), "after");
        if ($this->input->get('id8') != "")
            $this->db->like('id8', $this->input->get('id8'), "after");
        if ($this->input->get('yearin') != "")
            $this->db->like('yearin', $this->input->get('yearin'), "after");
        $query += $this->db->get()->result();
        $this->db->close();
        return $query;
    }

    public function detail() {
        $select = 'person.yearin,person.swimcode,person.id,person.id13,person.birthdate,person.name,person.is_request,person.sname,person.repdate,person.regdate,person.movedate,person.runcode,person.percent,person.yearin,person.height,person.batt,'
                . 'person.company,person.platoon,person.pseq,person.id8,person.oldyearin,'
                . 'statustab.statuscode,statustab.title,statustab.stitle,eductab.educname,'
                . 'skilltab.skill,person.percent,u3.unitname as refnum3,selectexam.unit4,'
                . 'selectexam.postcode,selectexam.item,u1.unitname as refnum1,u2.'
                . 'unitname as refnum2,s.title as patient_status,religion.religion,'
                . 'person.father,person.fsname,person.mother,person.msname,'
                . 'person.address,person.address_mu,person.address_soil,person.address_road,'
                . 'person.mark,person.blood,person.towncode,person.skillcode,person.occcode,t1.townname as '
                . 'town1,t2.townname as town2,t3.townname as town3,t3.zip as zipcode,AddressMove,occtab.occname,schooltab.schoolname,'
                . 'mc.namecode mc,mcp.NAMECODE mcp,mc5.NAMECODE mc5,mc4.NAMECODE mc4,g.groupname,m.start';
        $this->load->database('navdb');
        $query_new = $this->db->select($select)->from('person')
                        ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
                        ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
                        ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
                        ->join('occtab', 'occtab.occcode = person.occcode ', 'LEFT')
                        ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                        ->join('selectexam', 'selectexam.unit4 = person.unit4', 'LEFT')
                        ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
                        ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
                        ->join('schooltab', 'schooltab.campuscode = substring(person.schoolcode,7,8) and schooltab.ctown =  substring(person.schoolcode,1,6)', 'LEFT')
                        ->join('townname as t1', 't1.towncode = CONCAT(substring(person.towncode,1,2),"0000")', 'LEFT')
                        ->join('townname as t2', 't2.towncode = CONCAT(substring(person.towncode,1,4),"00")', 'LEFT')
                        ->join('townname as t3', 't3.towncode = person.towncode', 'LEFT')
                        ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
                        ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
                        ->join('navdb_all.member m', 'm.id = person.navyid', 'LEFT')
                        ->join('navdb_all.membercode mc', 'mc.membercode = m.membercode', 'LEFT')
                        ->join('navdb_all.membercode mcp', 'mc.membercode_parentid = mcp.membercode', 'LEFT')
                        ->join('navdb_all.membercode mc5', 'm.membercode5 = mc5.membercode', 'LEFT')
                        ->join('navdb_all.membercode mc4', 'm.membercode4 = mc4.membercode', 'LEFT')
                        ->join('navdb_all.grouptab g', 'g.groupID = m.groupID', 'LEFT')
                        ->where('person.navyid', $this->input->get('navyid'))
                        ->get()->result();
        $this->db->close();
        $this->load->database('navdb_all');
        $query_new += $this->db->select($select)->from('person')
                        ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
                        ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
                        ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
                        ->join('occtab', 'occtab.occcode = person.occcode ', 'LEFT')
                        ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                        ->join('selectexam', 'selectexam.unit4 = person.unit4', 'LEFT')
                        ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
                        ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
                        ->join('schooltab', 'schooltab.campuscode = substring(person.schoolcode,7,8) and schooltab.ctown =  substring(person.schoolcode,1,6)', 'LEFT')
                        ->join('townname as t1', 't1.towncode = CONCAT(substring(person.towncode,1,2),"0000")', 'LEFT')
                        ->join('townname as t2', 't2.towncode = CONCAT(substring(person.towncode,1,4),"00")', 'LEFT')
                        ->join('townname as t3', 't3.towncode = person.towncode', 'LEFT')
                        ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
                        ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
                        ->join('member m', 'm.id = person.navyid', 'LEFT')
                        ->join('membercode mc', 'mc.membercode = m.membercode', 'LEFT')
                        ->join('membercode mcp', 'mc.membercode_parentid = mcp.membercode', 'LEFT')
                        ->join('membercode mc4', 'm.membercode4 = mc4.membercode', 'LEFT')
                        ->join('membercode mc5', 'm.membercode5 = mc5.membercode', 'LEFT')
                        ->join('grouptab g', 'g.groupID = m.groupID', 'LEFT')
                        ->where('person.navyid', $this->input->get('navyid'))
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

    public function towncode() {
        $this->load->database($this->selectdb());
        $query_new = $this->db->select("towncode,townname")->from('townname')
                        ->where('clevel', '1')
                        ->get()->result();
        return $query_new;
    }

}
