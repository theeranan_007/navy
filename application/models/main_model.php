<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class main_model extends CI_Model {

    private $ci;

    ///////////////////////////////// switch database //////////////////////////
    public function selectdb() {
         if($this->ci->session->userdata('database')!=null){
             return $this->ci->session->userdata('database');
         }
         else{
             return 'navdb';
         }
    }

    ////////////////////////////////ends switch database ///////////////////////////////

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }
  
       public function detail() {
        $select = 'person.name,person.is_request,person.sname,person.yearin,person.height,person.batt,'
                . 'person.company,person.platoon,person.pseq,person.id8,person.oldyearin,'
                . 'statustab.statuscode,statustab.title,statustab.stitle,eductab.educname,'
                . 'skilltab.skill,person.percent,u3.unitname as refnum3,person.unit3,selectexam.unit4,'
                . 'selectexam.postcode,selectexam.item,u1.unitname as refnum1,u2.'
                . 'unitname as refnum2,s.title as patient_status,religion.religion,'
                . 'u4.unitname as refnum4,positiontab.postname';
        $this->load->database($this->selectdb());
        $query_new = $this->db->select($select)->from('person')->where('person.navyid', $this->input->get('navyid'))
                        ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
                        ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
                        ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
                        ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                        ->join('selectexam', 'selectexam.navyid = person.navyid', 'LEFT')
                        ->join('positiontab', 'selectexam.postcode = positiontab.postcode', 'LEFT')
                        ->join('unittab as u4', 'u4.refnum = selectexam.unit4', 'LEFT')
                        ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
                        ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
                        ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
                        ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }
 public function save(){
        $this->load->database('test');

        $array = array(
            'yearin' => $this->input->get('yearin'),
            'name' => $this->input->get('name'),
            'sname' => $this->input->get('sname'),
            'address' => $this->input->get('address'),
            'id8' => $this->input->get('id8'),
            'unit3' => $this->input->get('unit3'),
            );
        if($this->input->get('navyid')==''){
            $this->db->set($array)
            ->insert('person');
        }else{
        $this->db->set($array)
                ->where('navyid', $this->input->get('navyid'))
            ->update('person');
        }
        $this->db->close();
    }


    public function amount() {
        $this->load->database($this->selectdb());
        $query_new = $this->db->select('unittab.unitname,unittab.refnum,amount.unit,amount.count,amount.yearin')->from('amount')
                        ->join('unittab', 'unittab.refnum = amount.unit')
                        ->get()->result();
        return $query_new;
    }


    public function counttotal() {
        $this->load->database($this->selectdb());
        $this->db->select('unittab.refnum,unittab.unitname as unit,count(person.unit3) as count,amount.count as all,amount.count-count(person.unit3) as total')
                ->from('person')->where('unittab.movestat', 1)
                ->join('unittab', 'unittab.refnum = person.unit3', 'RIGHT')
                ->join('amount', 'amount.unit = unittab.refnum', 'LEFT')
                ->group_by('unittab.refnum')
                ->order_by('unittab.refnum', 'asc');
        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }

    public function saveamount() {
        $array = $this->input->get();
        $this->load->database($this->selectdb());
        for ($i = 1; $i <= 30; $i++) {
            $this->db->set('count', $array[$i])->where('unit', $i)->update('amount');
        }
        $this->db->close();
    }

    public function process() {
        $this->load->database($this->selectdb());
        $query = $this->db->select('person.navyid')
                        ->from('person')->where($this->input->get('unit'))->where('(person.unit3 = 0 or person.unit3 = "")')
                        ->order_by('person.percent DESC')
                        ->limit($this->input->get('limit'))
                        ->get()->result_array();
        if (sizeof($query) != 0) {

            foreach ($query as $key => $result) {
                $this->db->set('person.unit3', $this->input->get('unit3'))
                        ->where('person.navyid', $result['navyid'])
                        ->update('person');
            }
        }
        $this->db->close();
    }
/////////////////////////////////////// query unit2 //////////////////////////////
    public function armtown() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('*')->from('armtown')
                    ->where('armtown.legion', $this->input->get('legion'))
                    ->order_by('armtown.armname asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    public function person_armtown() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('person.navyid,person.name,'
                    . 'person.sname,person.id8,religion.religion,'
                    . 'armtown.armname,u1.unitname as u1,u2.unitname as u2,u3.unitname as u3,person.percent,statustab.title')->from('person')
                    ->join('armtown','armtown.armid = person.armid','left')
                    ->join('unittab as u1','u1.refnum = person.unit1','left')
                    ->join('unittab as u2','u2.refnum = person.unit2','left')
                    ->join('unittab as u3','u3.refnum = person.unit3','left')
                    ->join('statustab','statustab.statuscode = person.statuscode','left')
                    ->join('religion','religion.regcode = person.regcode','left');
                        if($this->input->get('armid')!='')
                    $this->db->where('armtown.armid', $this->input->get('armid'));

                    $this->db->where('armtown.legion',4)->order_by('armtown.armname asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query unittab //////////////////////////////
    public function unittab() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('unitname,refnum')->from('unittab')->where('movestat',1);
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query all //////////////////////////////
    public function countall() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('count,unit')->from('amount');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query unit1 //////////////////////////////
    public function countunit1() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('unit1 as unit,count(unit1) as count')->from('person')->where('unit3 = 0')
                    ->group_by('unit1')->order_by('unit1 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query unit2 //////////////////////////////
    public function countunit2() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('unit2 as unit,count(unit2) as count')->from('person')->where('unit3 = 0')
                    ->group_by('unit2')->order_by('unit2 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query unit3 //////////////////////////////
    public function countunit3() {
        $respond['query']='';
        $respond['error']='';
        try {
            $this->load->database($this->selectdb());
            $this->db->select('unit3,count(unit3) as count')->from('person')
                    ->group_by('unit3')->order_by('unit3 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    /////////////////////////////////////// query filter_unit3 //////////////////////////////
    public function filter_unit() {
        $respond['query']='';
        $respond['error']='';
        $str = 'person.navyid,person.name,person.sname,person.percent,u1.unitname as unit1,u2.unitname as unit2,u3.unitname as unit3,statustab.title';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('person')
                    ->join('unittab as u1','u1.refnum = person.unit1','LEFT')
                    ->join('unittab as u2','u2.refnum = person.unit2','LEFT')
                    ->join('unittab as u3','u3.refnum = person.unit3','LEFT')
                    ->join('statustab','statustab.statuscode = person.statuscode','LEFT')
                    ->where('unit3',$this->input->get('unit3'))
                    ->order_by('person.percent desc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error']=$error;
        }
        return $respond;
    }
    public function unit3_update(){
        $this->load->database($this->selectdb());
        $this->db->set('unit3',$this->input->get('unit3'))
                ->where('navyid',$this->input->get('navyid'))
                ->update('person');
        $this->db->close();
    }
    public function percentage(){
      $respond['query']='';
      $respond['error']='';
      try {
    	$this->load->database($this->selectdb());
    	$this->db->select('unittab.refnum,unittab.unitname as unit,count(person.unit3) as count,amount.count as all')
    	->from('person')->where('unittab.movestat',1)
    	->join('unittab','unittab.refnum = person.unit3','RIGHT')
    	->join('amount','amount.unit = unittab.refnum','LEFT')
    	->group_by('unittab.refnum')
    	->order_by('unittab.refnum','asc');
    	$respond['query'] = $this->db->get()->result_array();
    	$this->db->close();
    } catch (Exception $error) {
        $respond['error']=$error;
    }
    return $respond;
    }
    public function percentage2(){
      $respond['query']='';
      $respond['error']='';
      try {
    	$this->load->database($this->selectdb());
    	$this->db->select('unittab.refnum,unittab.unitname as unit,count(person.unit3) as count,amount.count as all')
    	->from('person')->where('unittab.movestat',1)
    	->join('unittab','unittab.refnum = person.unit3','RIGHT')
    	->join('amount','amount.unit = unittab.refnum','LEFT')
    	->group_by('unittab.refnum')
    	->order_by('unittab.refnum','asc');
    	$respond['query'] = $this->db->get()->result();
    	$this->db->close();
    } catch (Exception $error) {
        $respond['error']=$error;
    }
    return $respond;
    }
}
