<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class cansusRegistration_model extends CI_Model {

    private $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }
   public function selectdb() {
         if($this->ci->session->userdata('database')!=null){
             return $this->ci->session->userdata('database');
         }
         else{
             return 'navdb';
         }
    }
    public function search() {
        $str = 'person.navyid,person.name,person.sname,person.batt,
        person.company,person.id13,person.id8,person.oldyearin,person.
        yearin,unittab.unitname unit3,if(status_move.status_name is null,"ไม่ย้ายเข้า","status_move.status_name") status,
        p.book_number,p.rank,p.report_number,t1.townname town1,t2.townname town2,t3.townname town3,
        if(p.navyid is null,person.address,if(p.status=2,p.address_out,p.address_in)) address,
        if(p.navyid is null,person.address_mu,if(p.status=2,p.address_mu_out,p.address_mu_in))  address_mu,
        if(p.navyid is null,person.address_soil,if(p.status=2,p.address_soid_out,p.address_soid_in)) address_soil,
        if(p.navyid is null,person.address_road,if(p.status=2,p.address_road_out,p.address_road_in)) address_road';
        $this->load->database($this->selectdb());
        $this->db->select($str)
                ->from('person');
        if ($this->input->get('limit') == 1)
            $this->db->limit(10);
        if ($this->input->get('id13') != "")
            $this->db->where('person.id13', $this->input->get('id13'));
        if ($this->input->get('name') != "")
            $this->db->like('person.name', $this->input->get('name'), "after");
        if ($this->input->get('sname') != "")
            $this->db->like('person.sname', $this->input->get('sname'), "after");
        if ($this->input->get('id8') != "")
            $this->db->like('person.id8', $this->input->get('id8'), "after");
        if ($this->input->get('yearin') != "")
            $this->db->where('person.yearin', $this->input->get('yearin'));
        if ($this->input->get('book_number') != "")
            $this->db->where('people.book_number', $this->input->get('book_number'));
        if ($this->input->get('rank') != "")
            $this->db->where('people.rank', $this->input->get('rank'));
        if ($this->input->get('unit3') != "")
            $this->db->where('unittab.unitname', $this->input->get('unit3'));
        if($this->input->get('icheck')==0){
            $this->db->join('navdb_all.people p','p.navyid = person.navyid','right');
        }
        else{
            $this->db->join('navdb_all.people p','p.navyid = person.navyid','LEFT');
        }
        $this->db->join('townname t1','t1.towncode = concat(substring(if(p.navyid is null,person.towncode,if(p.status = 2,towncode_out,towncode_in)),1,2),"0000")','left');
        $this->db->join('townname t2','t2.towncode = concat(substring(if(p.navyid is null,person.towncode,if(p.status = 2,towncode_out,towncode_in)),1,4),"00")','left');
        $this->db->join('townname t3','t3.towncode = if(p.navyid is null,person.towncode,if(p.status = 2,towncode_out,towncode_in))','left');
        $this->db->join('unittab','unittab.refnum = person.unit3','LEFT');
        $this->db->join('navdb_all.status_move','status_move.status_id = p.status','LEFT');
        $query = $this->db->get()->result();
        $this->db->close();
        $this->load->database('navdb_all');
        $this->db->select($str)
                ->from('person');
        if ($this->input->get('limit') == 1)
            $this->db->limit(10);
        if ($this->input->get('id13') != "")
            $this->db->where('person.id13', $this->input->get('id13'));
        if ($this->input->get('name') != "")
            $this->db->like('person.name', $this->input->get('name'), "after");
        if ($this->input->get('sname') != "")
            $this->db->like('person.sname', $this->input->get('sname'), "after");
        if ($this->input->get('id8') != "")
            $this->db->like('person.id8', $this->input->get('id8'), "after");
        if ($this->input->get('yearin') != "")
            $this->db->where('person.yearin', $this->input->get('yearin'));
        if ($this->input->get('book_number') != "")
            $this->db->where('people.book_number', $this->input->get('book_number'));
        if ($this->input->get('rank') != "")
            $this->db->where('people.rank', $this->input->get('rank'));
        if ($this->input->get('unit3') != "")
            $this->db->where('unittab.unitname', $this->input->get('unit3'));
        if($this->input->get('icheck')==0){
            $this->db->join('people p','p.navyid = person.navyid','right');
        }
        else{
            $this->db->join('people p','p.navyid = person.navyid','LEFT');
        }
        $this->db->join('townname t1','t1.towncode = concat(substring(if(p.navyid is null,person.towncode,if(p.status = 2,towncode_out,towncode_in)),1,2),"0000")','left');
        $this->db->join('townname t2','t2.towncode = concat(substring(if(p.navyid is null,person.towncode,if(p.status = 2,towncode_out,towncode_in)),1,4),"00")','left');
        $this->db->join('townname t3','t3.towncode = if(p.navyid is null,person.towncode,if(p.status = 2,towncode_out,towncode_in))','left');
        $this->db->join('unittab','unittab.refnum = person.unit3','LEFT');
        $this->db->join('statustab','statustab.statuscode = person.statuscode','LEFT');
        $this->db->join('navdb_all.status_move','status_move.status_id = p.status','LEFT');
        $query += $this->db->get()->result();
        $this->db->close();
        return $query;
    }
    public function detail() {
        $select = 'person.yearin,person.swimcode,person.id,person.id13,person.birthdate,person.name,person.is_request,person.sname,person.repdate,person.regdate,person.movedate,person.runcode,person.percent,person.yearin,person.height,person.batt,'
                . 'person.company,person.platoon,person.pseq,person.id8,person.oldyearin,'
                . 'statustab.statuscode,statustab.title,statustab.stitle,eductab.educname,'
                . 'skilltab.skill,person.percent,u3.unitname as refnum3,selectexam.unit4,'
                . 'selectexam.postcode,selectexam.item,u1.unitname as refnum1,u2.'
                . 'unitname as refnum2,s.title as patient_status,religion.religion,'
                . 'person.father,person.fsname,person.mother,person.msname,'
                . 'person.address,person.address_mu,person.address_soil,person.address_road,'
                . 'person.mark,person.blood,person.towncode,person.skillcode,person.occcode,t1.townname as '
                . 'town1,t2.townname as town2,t3.townname as town3,t3.zip as zipcode,AddressMove,occtab.occname,schooltab.schoolname';
        $this->load->database('navdb');
        $query_new = $this->db->select($select)->from('person')
                        ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
                        ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
                        ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
                        ->join('occtab', 'occtab.occcode = person.occcode ', 'LEFT')
                        ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                        ->join('selectexam', 'selectexam.unit4 = person.unit4', 'LEFT')
                        ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
                        ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
                        ->join('schooltab', 'schooltab.campuscode = substring(person.schoolcode,7,8) and schooltab.ctown =  substring(person.schoolcode,1,6)', 'LEFT')
                        ->join('townname as t1', 't1.towncode = CONCAT(substring(person.towncode,1,2),"0000")', 'LEFT')
                        ->join('townname as t2', 't2.towncode = CONCAT(substring(person.towncode,1,4),"00")', 'LEFT')
                        ->join('townname as t3', 't3.towncode = person.towncode', 'LEFT')
                        ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
                        ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
                        ->where('person.navyid', $this->input->get('navyid'))
                        ->get()->result();
        $this->db->close();
        $this->load->database('navdb_all');
        $query_new += $this->db->select($select)->from('person')
                        ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
                        ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
                        ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
                        ->join('occtab', 'occtab.occcode = person.occcode ', 'LEFT')
                        ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                        ->join('selectexam', 'selectexam.unit4 = person.unit4', 'LEFT')
                        ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
                        ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
                        ->join('schooltab', 'schooltab.campuscode = substring(person.schoolcode,7,8) and schooltab.ctown =  substring(person.schoolcode,1,6)', 'LEFT')
                        ->join('townname as t1', 't1.towncode = CONCAT(substring(person.towncode,1,2),"0000")', 'LEFT')
                        ->join('townname as t2', 't2.towncode = CONCAT(substring(person.towncode,1,4),"00")', 'LEFT')
                        ->join('townname as t3', 't3.towncode = person.towncode', 'LEFT')
                        ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
                        ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
                        ->where('person.navyid', $this->input->get('navyid'))
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }
    public function unittab() {
        $this->load->database($this->selectdb());
        $query_new = $this->db->select("unitname,refnum")->from('unittab')
                        ->where('movestat', '1')
                        ->get()->result();
        return $query_new;
        $this->db->close();
    }

}
