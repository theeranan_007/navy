<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class search_education_model extends CI_Model {

    private $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }

     public function selectdb() {
         if($this->ci->session->userdata('database')!=null){
             return $this->ci->session->userdata('database');
         }
         else{
             return 'navdb';
         }
    }
    public function detail() {
        $select = 'person.yearin,person.swimcode,person.id,person.id13,person.birthdate,person.name,person.is_request,person.sname,person.repdate,person.regdate,person.movedate,person.runcode,person.percent,person.yearin,person.height,person.batt,'
                . 'person.company,person.platoon,person.pseq,person.id8,person.oldyearin,'
                . 'statustab.statuscode,statustab.title,statustab.stitle,eductab.educname,'
                . 'skilltab.skill,person.percent,u3.unitname as refnum3,selectexam.unit4,'
                . 'selectexam.postcode,selectexam.item,u1.unitname as refnum1,u2.'
                . 'unitname as refnum2,s.title as patient_status,religion.religion,'
                . 'person.father,person.fsname,person.mother,person.msname,'
                . 'person.address,person.address_mu,person.address_soil,person.address_road,'
                . 'person.mark,person.blood,person.towncode,person.skillcode,person.occcode,t1.townname as '
                . 'town1,t2.townname as town2,t3.townname as town3,t3.zip as zipcode,AddressMove,occtab.occname,schooltab.schoolname,'
                . 'mc.namecode mc,mcp.NAMECODE mcp,mc5.NAMECODE mc5,mc4.NAMECODE mc4,g.groupname,m.start';
        $this->load->database('navdb');
        $query_new = $this->db->select($select)->from('person')
                        ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
                        ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
                        ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
                        ->join('occtab', 'occtab.occcode = person.occcode ', 'LEFT')
                        ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                        ->join('selectexam', 'selectexam.unit4 = person.unit4', 'LEFT')
                        ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
                        ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
                        ->join('schooltab', 'schooltab.campuscode = substring(person.schoolcode,7,8) and schooltab.ctown =  substring(person.schoolcode,1,6)', 'LEFT')
                        ->join('townname as t1', 't1.towncode = CONCAT(substring(person.towncode,1,2),"0000")', 'LEFT')
                        ->join('townname as t2', 't2.towncode = CONCAT(substring(person.towncode,1,4),"00")', 'LEFT')
                        ->join('townname as t3', 't3.towncode = person.towncode', 'LEFT')
                        ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
                        ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
                        ->join('navdb_all.member m', 'm.id = person.navyid', 'LEFT')
                        ->join('navdb_all.membercode mc', 'mc.membercode = m.membercode', 'LEFT')
                        ->join('navdb_all.membercode mcp', 'mc.membercode_parentid = mcp.membercode', 'LEFT')
                        ->join('navdb_all.membercode mc5', 'm.membercode5 = mc5.membercode', 'LEFT')
                        ->join('navdb_all.membercode mc4', 'm.membercode4 = mc4.membercode', 'LEFT')
                        ->join('navdb_all.grouptab g', 'g.groupID = m.groupID', 'LEFT')
                        ->where('person.navyid', $this->input->get('navyid'))
                        ->get()->result();
        $this->db->close();
        $this->load->database('navdb_all');
        $query_new += $this->db->select($select)->from('person')
                        ->join('statustab', 'person.statuscode = statustab.statuscode', 'LEFT')
                        ->join('eductab', 'person.educode1 = eductab.ecode1 and person.educode2 = eductab.ecode2', 'LEFT')
                        ->join('skilltab', 'person.skillcode = skilltab.skillcode', 'LEFT')
                        ->join('occtab', 'occtab.occcode = person.occcode ', 'LEFT')
                        ->join('unittab as u3', 'u3.refnum = person.unit3', 'LEFT')
                        ->join('selectexam', 'selectexam.unit4 = person.unit4', 'LEFT')
                        ->join('unittab as u1', 'u1.refnum = person.unit1', 'LEFT')
                        ->join('unittab as u2', 'u2.refnum = person.unit2', 'LEFT')
                        ->join('schooltab', 'schooltab.campuscode = substring(person.schoolcode,7,8) and schooltab.ctown =  substring(person.schoolcode,1,6)', 'LEFT')
                        ->join('townname as t1', 't1.towncode = CONCAT(substring(person.towncode,1,2),"0000")', 'LEFT')
                        ->join('townname as t2', 't2.towncode = CONCAT(substring(person.towncode,1,4),"00")', 'LEFT')
                        ->join('townname as t3', 't3.towncode = person.towncode', 'LEFT')
                        ->join('statustab as s', 's.statuscode = person.patient_status', 'LEFT')
                        ->join('religion', 'religion.regcode = person.regcode', 'LEFT')
                        ->join('member m', 'm.id = person.navyid', 'LEFT')
                        ->join('membercode mc', 'mc.membercode = m.membercode', 'LEFT')
                        ->join('membercode mcp', 'mc.membercode_parentid = mcp.membercode', 'LEFT')
                        ->join('membercode mc4', 'm.membercode4 = mc4.membercode', 'LEFT')
                        ->join('membercode mc5', 'm.membercode5 = mc5.membercode', 'LEFT')
                        ->join('grouptab g', 'g.groupID = m.groupID', 'LEFT')
                        ->where('person.navyid', $this->input->get('navyid'))
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }
    public function query() {
        $select = "person.navyid,person.name,person.sname,CONCAT(statustab.title,'(',person.statuscode,')') as status,CONCAT_WS('/',person.company,person.batt) as belong,unittab.refnum,unittab.unitname,person.id8,person.oldyearin";
        $this->load->database($this->selectdb());
        $query_new['count'] = $this->db->select("COUNT(navyid) AS count")->from("person")->get()->result(); //
        $query_new['select'] = $this->db->select($select)->from('person')->limit($this->input->get('limit'), $this->input->get('row'))
                        ->join('statustab', 'statustab.statuscode = person.statuscode', 'LEFT')
                        ->join('unittab', 'unittab.refnum = person.unit3', 'LEFT')
                        ->where("person.statuscode !=", "AA")
                        ->order_by('person.statuscode', 'asc')
                        ->order_by('person.batt', 'asc')
                        ->order_by('person.company', 'asc')
                        ->order_by('person.id8', 'asc')
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

    public function eductab() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('eductab')
                    ->where('eductab.ecode2', '001')
                    ->order_by('eductab.ecode1 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
    public function save() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = array(
            'yearin'=>$this->input->get('yearin'),
            'unit'=> $this->input->get('unit'),
            'general'=> $this->input->get('general'),
            'ecode1'=> $this->input->get('ecode1'),
            'ecode2'=> $this->input->get('ecode2'),
            'skillcode'=> $this->input->get('skill'),
            'total' => $this->input->get('total'));
        try {
            $this->load->database($this->selectdb());
            $this->db->set($str);
            if($this->input->get('seq')=='')   {
                $this->db->insert('hisrequire');
            }else{
                $this->db->where('seq',$this->input->get('seq'))->update('hisrequire');
            }
            $respond['query'] = '';
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
     public function viewtable() {
        $respond['query'] = '';
        $respond['error'] = '';

          $str = array(
              'person.yearin'=>$this->input->get('yearin'),
              'person.educode1'=>$this->input->get('ecode1'),
              'person.educode2'=> $this->input->get('ecode2'));
        try {
            $this->load->database($this->selectdb());
            $this->db->select('person.navyid,person.yearin,person.id8,person.name,person.sname,person.batt,person.company,schooltab.schoolname')->from('person')
                    ->join('eductab','eductab.ecode1 = person.educode1 and eductab.ecode2 = person.educode2','left')
                    ->join('schooltab', 'schooltab.campuscode = substring(person.schoolcode,7,8) and schooltab.ctown =  substring(person.schoolcode,1,6)', 'LEFT')
                    ->where($str);
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        $this->db->close();
        try {
            $this->load->database('navdb_all');
            $this->db->select('person.navyid,person.yearin,person.id8,person.name,person.sname,person.batt,person.company,schooltab.schoolname')->from('person')
                    ->join('eductab','eductab.ecode1 = person.educode1 and eductab.ecode2 = person.educode2','left')
                    ->join('schooltab', 'schooltab.campuscode = substring(person.schoolcode,7,8) and schooltab.ctown =  substring(person.schoolcode,1,6)', 'LEFT')
                    ->where($str);
            $respond['query'] += $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] += $error;
        }
        $this->db->close();
        return $respond;
    }
    public function eductab2() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('eductab')
                    ->where('eductab.ecode2!=001 and eductab.ecode1=' . $this->input->get('ecode1'))
                    ->order_by('eductab.ecode2 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
public function eductab_p2() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = '*';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('eductab')
                    ->where('eductab.ecode2', '001')
                    ->order_by('eductab.ecode1 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        return $respond;
    }
    public function eductab2_p2() {
        $respond['query'] = '';
        $respond['error'] = '';
        $str = 'concat(e.educname,"(",count(p.navyid),")") as educname,e.ecode2';
        try {
            $this->load->database($this->selectdb());
            $this->db->select($str)->from('eductab e')
                    ->join('person p','p.educode1=e.ecode1 and p.educode2=e.ecode2','left')
                    ->where('e.ecode1=' . $this->input->get('ecode1').' and p.yearin="'.$this->input->get('yearin').'"')
                    ->group_by('e.educname')
                    ->order_by('e.ecode2 asc');
            $respond['query'] = $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] = $error;
        }
        $this->db->close();
        try {
            $this->load->database("navdb_all");
            $this->db->select($str)->from('eductab e')
                    ->join('person p','p.educode1=e.ecode1 and p.educode2=e.ecode2','left')
                    ->where('e.ecode1=' . $this->input->get('ecode1').' and p.yearin="'.$this->input->get('yearin').'"')
                    ->group_by('e.educname')
                    ->order_by('e.ecode2 asc');
            $respond['query'] += $this->db->get()->result();
        } catch (Exception $error) {
            $respond['error'] += $error;
        }
        $this->db->close();
        return $respond;
    }
    public function search_education_unittab() {
        $this->load->database($this->selectdb());
        $query_new = $this->db->select('concat(refnum," ",unitname)as name')->from('unittab')
                        ->where('movestat', '1')
                        ->get()->result();
        $this->db->close();
        return $query_new;
    }

}
