<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class search_person_model extends CI_Model {
    private $ci;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->ci = & get_instance();
    }
    public function search_query() {
        $query = array();
        $like = array();

        //print_r($like);

        $this->load->database("navdb");
        $this->db->select('navyid,name,sname,concat(company,"/",batt) as belong,id8,oldyearin')
        ->from('person');
        if ($this->input->get('name') != "")
        $this->db->like('name', $this->input->get('name'), "both");
        if ($this->input->get('sname') != "")
        $this->db->like('sname', $this->input->get('sname'), "both");
        if ($this->input->get('id8') != "")
        $this->db->like('id8', $this->input->get('id8'), "after");

        $query = $this->db->get()->result();
        $this->db->close();
        return $query;
    }


}
