
$(document).ready(function() {
    $('#firstnamem').keypress(function (e) {
  if (e.which == 13) {
    //result();
    return false;    //<---- Add this line
  }
});
    $('#lastnamem').keypress(function (e) {
  if (e.which == 13) {
    //result();
    return false;    //<---- Add this line
  }
});
   $('#btn_search').click(function(){
    result();
   });
});
    function resultm(){
    $('#result tbody').empty();
    $('#num_row').empty();
    $('#notification').empty();
    
        $.ajax({
                url:"http://192.168.0.1/navy/search/query?name="+$("#firstname").val()+"&sname="+$("#lastname").val()+"&RegisProv="+$("#RegisProv").val(),
                cache: false,
                dataType:'JSON',
                success:function(res)
                {
                    var count = Object.keys(res).length;
                    var danger_notification = "";
    danger_notification += '<div class="alert alert-danger alert-dismissible">';
    danger_notification += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    danger_notification += '<h4><i class="icon fa fa-check"></i>ไม่พบรายการ</h4>';
    danger_notification += '</div>'
    var success_notification = "";
    success_notification += '<div class="alert alert-success alert-dismissible">';
    success_notification += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    success_notification += '<h4><i class="icon fa fa-check"></i>พบพลทหารจำนวน '+count+' นาย</h4>';
    success_notification += '</div>'
                    if (count==0){
                        $("#result_box").removeAttr('class');
                        $("#result_box").attr('class', 'box box-danger');
                        $("#search_box").removeAttr('class');
                        $("#search_box").attr('class', 'box box-danger');
                        $(danger_notification).appendTo("#notification");
                        $('#num_row').html("ไม่พบรายการ");
                    }
                    else{
                        $("#result_box").removeAttr('class');
                        $("#result_box").attr('class', 'box box-success');
                        $("#search_box").removeAttr('class');
                        $("#search_box").attr('class', 'box box-success');
                        $(success_notification).appendTo("#notification");
                        $('#num_row').html("พบพลทหารจำนวน "+count+" นาย");
                    }
                    
                    var txthtml= "";
                    $.each(res, function(key, value){
                        txthtml += '<tr><td class="text-center">'+value['name'];
                        txthtml +='</td><td class="text-center">'+value['sname'];
                        txthtml +='</td><td class="text-center">'+value['yearin'];
                        txthtml +='</td><td class="text-center">'+value['batt'];
                        txthtml +='</td><td class="text-center">'+value['company'];
                        txthtml +='</td><td class="text-center">'+value['id8'];
                        if(value['oldyearin']==null)
                        txthtml +='</td><td class="text-center">';
                        else
                        txthtml +='</td><td class="text-center">'+value['oldyearin'];

                        txthtml +='</td><td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'+value['navyid']+'">';
                        txthtml += "Launch demo modal";
                        txthtml +="</button>";
                        txthtml +="<div class='modal fade' id='"+value['navyid']+"' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>";
                        txthtml +='<div class="modal-dialog modal-dialog modal-lg" role="document">';
                        txthtml +='<div class="modal-content">';
                         txthtml +=  '   <div class="modal-header">';
                        txthtml +=        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                         txthtml +=       '<h4 class="modal-title" id="myModalLabel"> '+value['navyid']+'</h4>';
                         txthtml +=     '</div>';
      txthtml +='<div class="modal-body">';
      txthtml +='</div>';
                         txthtml +=    ' <div class="modal-footer">';
                         txthtml +=       '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
                         txthtml +=       '<button type="button" class="btn btn-primary">Save changes</button>';
                         txthtml +=     '</div>';
                         txthtml +=  ' </div>';
                         txthtml += '</div>'
                        txthtml +='</div></td></tr>';                
                    });
                    $(txthtml).appendTo("#result_separation_unit tbody");
                },
                error:function(err)
                {
                    alert("error");
                }
        }); 
}
